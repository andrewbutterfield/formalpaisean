You want the LaTeX kernel function \@ifclassloaded:

\@ifclassloaded{<someclass>}
  {<true code>}
  {<false coode>}%

for this to work, you have to enclose it in \makeatletter and \makeatother.
