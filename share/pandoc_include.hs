#!/usr/bin/env runhaskell
-- pandoc_include.hs: insert contents of a specified file into a
-- pandoc code block:
-- ~~~~ {include="foo.pml"}
-- ~~~~
-- would insert the contents of foo.pml into the body
-- of the code block.
-- run as: pandoc -t json doc.md | pandoc_include.hs | pandoc -t json -t desired format
-- Original by Anthony McCormick, as posted to Stack Overflow (http://stackoverflow.com/questions/21584396/pandoc-include-files-filter-in-haskell)

import Text.Pandoc.JSON

doInclude :: Block -> IO Block
doInclude cb@(CodeBlock (id, classes, namevals) contents) =
   case lookup "include" namevals of
       Just f     -> return . (CodeBlock (id, classes, namevals)) =<< readFile f
       Nothing    -> return cb
doInclude x = return x

main :: IO ()
main = toJSONFilter doInclude