---
fontfamily: times
fontsize: 12
author: John Noll
title: PML Quick Start
date: 4 August, 2015
documentclass: hitec
...

# First steps

A _process_ is a sequence of tasks, where a task is either a primitive
_action_ or a composition of _actions_.  Thus the simplest process
contains one _action_:


~~~~~ {.pml}
process philosophy {
    action think {}
}
~~~~~

An _action_, is specified with the `action` keyword, followed by an
identifier that names the action, then the action body surrounded by
curly braces.

Thinking is hard work, though, so we might want to eat first:

~~~~~ {.pml}
process philosophy {
    action eat {}
    action think {}
}
~~~~~

This process has two actions, nominally to be performed in order:
`eat`, then `think`.

Thinking can take a long time, especially when philosphy is involved;
so, we might want to eat again, then think some more: 

~~~~~ {.pml}
process philosophy {
    iteration {
        action eat {}
        action think {}
    }
}
~~~~~

An _iteration_ construct says that the actions in the iteration body
should be repeated, in order, an unspecified number of times.  It is
up to the agent that is enacting the process to decide when the
iteration is over.

As a practical matter, the processes we want to model have some
tangible output.  Philosophy is no exception:

~~~~~ {.pml}
process philosophy {
    iteration {
        action eat {}
        action think {}
    }
    action write {}
}
~~~~~

But what, exactly, is the tangible output?  And what is the input?
These are modeled by _resources_:
    
~~~~~ {.pml}
process philosophy {
    iteration {
        action eat {
            requires { food }
            provides { sustenance }
        }
        action think { 
            requires { sustenance }
            provides { idea }
        }
    }
    action write {
        requires { idea }
        provides { essay }
    }
}
~~~~~

_Resources_ name things that are required to perform an action
(`requires`), and things that are provided when an action is completed
(`provides`).  Resources can have attributes:

~~~~~ {.pml}
process philosophy {
    iteration {
        action eat {
            requires { food }
            provides { sustenance }
        }
        action think { 
            requires { sustenance }
            provides { idea }
        }
    }
    action write {
        requires { idea }
        provides { essay.draft == "initial" }
    }
}
~~~~~

Attributes specify the state of a resource, as a predicate comparing
the resource attribute to a value (a number or string, usually).  

The same resource can serve as both the input and output of an action;
attributes can be used to specify how the action changes the resource:

~~~~~ {.pml}
process philosophy {
    iteration {
        action eat {
            requires { food }
            provides { sustenance }
        }
        action think { 
            requires { sustenance }
            provides { idea }
        }
    }
    action write {
        requires { idea }
        provides { essay.draft == "initial" }
    }
    action edit { 
        requires { essay.draft == "initial" }
        provides { essay.draft == "edited" }
    }
    action proofread { 
        requires { essay.draft == "edited" }
        provides { essay.draft == "final" }
    }
}
~~~~~

# Branching

Sometimes there is a choice of what to do next.  For example, writing
an essay doesn't always proceed directly from a single idea; there
might be several ideas that need to be integrated.  So, sometimes our
philosopher might want to think, and other times write:

~~~~~ {.pml}
process philosophy {
    iteration {
        selection {
            action eat {
                requires { food }
                provides { sustenance }
            }
            action think { 
                requires { sustenance }
                provides { idea }
            }
            action write {
                requires { idea }
                provides { essay.draft == "initial" }
            }
        }
    }
    action edit { 
        requires { essay.draft == "initial" }
        provides { essay.draft == "edited" }
    }
    action proofread { 
        requires { essay.draft == "edited" }
        provides { essay.draft == "final" }
    }
}
~~~~~

A _selection_ is a choice construct: it means, "do _exactly one_ of
these tasks."  So, at any time our philosopher can either eat, think, or
write.  Then, when the essay is deemed ready for editing, the philosopher
stops iterating over eat, think, or write, and proceeds to editing.

Suppose our philosopher can eat and think at the same time.  We can
capture this with a _branch_, which means: "do _all_ of these tasks
concurrently":

~~~~~ {.pml}
process philosophy {
    iteration {
        selection {
            branch {
                action eat {
                    requires { food }
                    provides { sustenance }
                }
                action think { 
                    requires { sustenance }
                    provides { idea }
                }
            }
            action write {
                requires { idea }
                provides { essay.draft == "initial" }
            }
        }
    }
    action edit { 
        requires { essay.draft == "initial" }
        provides { essay.draft == "edited" }
    }
    action proofread { 
        requires { essay.draft == "edited" }
        provides { essay.draft == "final" }
    }
}
~~~~~

# Scripts

How is one supposed to perform an action?  The _script_ field can be
used for instructions to the agent, in natural language if the agent
is human, or some executable code if the agent is a computer:

~~~~~ {.pml}
process philosophy {
    iteration {
        selection {
            branch {
                action eat {
                    requires { food }
                    provides { sustenance }
                    script { "Pick up a fork in each hand, and eat the
                    spaghetti." }
                }
                action think { 
                    requires { sustenance }
                    provides { idea }
                    script { "Focus on something deep."
                }
            }
            action write {
                requires { idea }
                provides { essay.draft == "initial" }
                script { "Write down your idea in philosophical
                terms.  Try to be as abstruse as possible." 
            }
        }
    }
    action edit { 
        requires { essay.draft == "initial" }
        provides { essay.draft == "edited" }
        script { "Read and refine your initial draft, replacing clear,
        logical presentation with convoluted arguments." }
    }
    action proofread { 
        requires { essay.draft == "edited" }
        provides { essay.draft == "final" }
        script { "Read the draft to be sure spelling and punctuation
        are correct." }
    }
}
~~~~~

# Agents

Some processes involve multiple actors.  The `agent` keyword specifies
what kind of actor should perform an action.  Suppose our philosopher
has a student edit the draft, and an assistant do the final proofreading:

~~~~~ {.pml}
process philosophy {
    iteration {
        selection {
            branch {
                action eat {
                    agent { philosopher }
                    requires { food }
                    provides { sustenance }
                    script { "Pick up a fork in each hand, and eat the
                    spaghetti." }
                }
                action think { 
                    agent { philosopher }
                    requires { sustenance }
                    provides { idea }
                    script { "Focus on something deep." }
                }
            }
            action write {
                agent { philosopher }
                requires { idea }
                provides { essay.draft == "initial" }
                script { "Write down your idea in philosophical
                terms.  Try to be as abstruse as possible." }
            }
        }
    }
    action edit { 
        agent { student }
        requires { essay.draft == "initial" }
        provides { essay.draft == "edited" }
        script { "Read and refine your initial draft, replacing clear,
        logical presentation with convoluted arguments." }
    }
    action proofread { 
        agent { assistant }
        requires { essay.draft == "edited" }
        provides { essay.draft == "final" }
        script { "Read the draft to be sure spelling and punctuation
        are correct." }
    }
}
~~~~~

