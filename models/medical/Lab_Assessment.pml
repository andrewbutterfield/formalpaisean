process Lab_Assessment{
	action PresentToSpecialistLab {
		requires { symptoms }
		provides { testPlan }
	}
	action PrepareTests {
		requires { testPlan }
		provides { testSuite }
	}
	action RunTests {
		requires { test_suite && examinationArtefacts 
			&& diagnosis }
		provides { diagnosis.status == "tested" }
	}
}
