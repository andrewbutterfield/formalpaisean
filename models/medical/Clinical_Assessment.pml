process Clinical_Assessment {
  iteration {
    iteration {
      action PresentToSpecialistClinician {
	requires { reportedSymptoms }
	provides { scheduledExamination }
      }
      action Examine {
	requires { scheduledExamination }
	provides { examinationArtefacts }
      }
    }
    action Diagnose {
      requires { examinationArtefacts }
      provides { diagnosis }
    }
    action Treat {
      requires { diagnosis }
      provides { treatment }
    }

  }
}
