This directory contains some PML models of clinical pathways:

Models from FHEIS '13 paper:

+ Clinical_Assessment.pml 
+ Dementia_Management.pml 
+ Dementia_Management-revised.pml
+ Lab_Assessment.pml

Models from ICHI '14 demo:

+ diabetes_assessment.pml - In my opinion this starts at the wrong
  place; I think it would be more accurate if it started with a
  "suspect diabetes" diagnosis.
+ patient_assessment.pml - This simple process should feed into
  diabetes_assessment.pml (and any other pathways that start with a
  "suspect" diagnosis).
