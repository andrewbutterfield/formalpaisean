process DementiaManagement {
  action Step_4_Identify_causal_or_exacerbating_factors { /* XXX identifiers must start with letter */
    requires { Guidelines_For_Treatment_Of_Patients }
    /*  provides { } XXX provides/requires can't be empty  */
  }
  action Step_5_Provide_patient_carer_with_information {/* XXX identifiers must start with letter */
    agent {GP && patient && carer}
    requires {patient_record.Confirmed_Dementia  }
    requires {patient_record.requests_privacy == "false" }
    provides { information_to_carer }
  }	
  action treatment_or_care_plan {
    agent {memory_assessment_service} /* XXX does this comprise the next line? */
    agent {GP && clinical_psychologiest && nurses && occupational_therapists && phsiotherapists && speech_and_language_therapists && social_workers && voluntary_organisation}
    requires { patient_history }
    provides { care_plan }
  }
  /* XXX The following two iterations over actions were written as just iterations with action bodies, but no actions.  Iterations can only have other control structures or actions in the body, so I changed each to an iteration over one action.  But is that the real meaning?  It seems like the first action is a one-off approval,  and so is just the next in the sequence after "treatmen_or_care_plan"; the seems to be the same. */
  iteration {
    action formal_review_of_care_plan {
      agent {person && carer}
      requires {care_plan}
      provides {care_plan.review} /* XXX Maybe, care_plan.reviewed == "true" */
    }
  }
  iteration {
    action assess_carer_needs { 
      agent { carer}
      /*    requires { } XXX provides/requires can't be empty */
      provides {care_plan.respite_care}
    }
  }
  branch {/* XXX is there an error here?  If you look at the graph, you will see two branches nested in this branch, both with the same actions.  The difference is that the right sub-branch of the left branch doesn't have "behavioral_disturbances".  Also, the far right path seems to be something that should follow these two branches. */
    branch {
      action non_medication_interventions {
/*	agent {} XXX agent can't be empty */
/*	requires {}  XXX provides/requires can't be empty */
	provides {support_for_carer}
	provides {info_about_servicesAndInterventions}
	provides {(optional) cognitive_simulation}
      }
      action medication_interventions {
	agent {specialist}
	agent {carer}
	requires {(intangible)carer_view_on_pat_condition }
	provides {prescription}
      }
    } /* end of inside branch */
    branch {
      action non_medication_interventions { 
	agent {carer && patient}
	requires {(nontangible)non_cognitive_symptoms || (nontangible) challenging_behaviour}
	provides {early_assessment}
      }
      sequence {
	action medication_interventions {
	  requires {(intangible) risk_of_harm_or_distress}
	  provides {medication}
	}
	action behavioural_disturbances {
	  agent {patient}
	  requires {(intangible) risk_of_behavioural_disturbance}
	  provides {care_plan.appropriate_setting}
	}
      } /* XXX end inside ~~branch~~ sequence */
    } /* end outside branch */
    sequence carers_view {
      action carers_view {
	agent {carer}
	provides {(intangible) view_on_condition}
      }
      action doc_symptoms {
	agent {patient}
	provides {patient_record.symptoms}
      }
      iteration{
	action record_changes {
	  agent {patient}
	  provides {patient_record.symptoms}
	  provides {(optional) medication}
	}
      }
      iteration {
	action monitor_patients_on_medication{
	  agent {patient }
	  provides {(optional)care_plan.medication}
	}
	action intensive_monitoring {
	  agent {patient}
	  requires {(intangible) psychiatric_symptoms}
	  provides {(optional)care_plan.medication}
	}
	action alternative_assessments {
	  requires {patient_record.disability || patient_record.sensory_impairment || patient_record.lingustic_problems || patient_record.speech_problems} 
	  provides {care_plan.alternative_assessment_method}
	}
      }
      action manage_comorbidity {
	/*requires { }*/
	provides {comorbidity.depression}
	provides {comorbidity.psychosis}
	provides {comorbidity.delirium}
	provides {comorbidity.parkinsons_disease}
	provides {comorbidity.stroke}
      }
      action psychosocial_interventions {
	requires {comorbidity.depression || comorbidity.anxiety}
	agent {carer}
      }
    }
  }
}
