\HDRa{Operational Semantics for PML}

\section{Introduction}

We present a first cut at a structured operational semantics for PML.



\subsection{PML State}

The state of a running PML process evolves over time keeping
track of resources, and task state.
We do not give a precise definition here,
except to say that we can always ask the state does a resource exists,
and we can update it to include a resource.

For now our notion of resource is very simple - just a name or identifier.
In practise a resource itself may have attributes and may be transformed.
Ultimately we shall view the contents of a required or provides construct
as being a predicate that describes the whole state of a resource.
\begin{eqnarray*}
   s \in State && \mbox{PML System State}
\end{eqnarray*}
Given a set (list) $R$ of resources, we adopt the following suggestive
notation:
\begin{eqnarray*}
   R \subseteq s && \mbox{All of $R$ are in $s$}
\\ && \mbox{All properties specified in $R$ are true of $s$}
\\ R \cup s && \mbox{All of $R$ are put into $s$}
\\ && \mbox{$s$ is modified so that all of $R$ become true}
\end{eqnarray*}
In each case the first statement corresponds to the simple view,
whilst the second refers to the notion of a resource state property

\subsection{PML Operational Semantics}

Our basic judgement is written:
\[
  (s,w) \fun (s',w')
\]
This states that executing PML process $w$ in state $s$
results in a modified state $s'$ and remaining PML process $w'$.

\subsubsection{Operation of \texttt{action}.}

\[
\inferrule
 {qs \subseteq s}
 {(s,N:\;?qs\;!ps) \fun (ps \cup s,\done)}
\]

\subsubsection{Operation of \texttt{sequence}.}

\[
\inferrule
 {(s,w_1) \fun (s',\done)}
 {(s,w_1;w_2) \fun (s',w_2)}
\]

\[
\inferrule
 {(s,w_1) \fun (s',w'_1) \\ w'_1 \neq \done}
 {(s,w_1;w_2) \fun (s',w'_1;w_2)}
\]


\subsubsection{Operation of \texttt{branch}.}

\[
\inferrule
 {(s,w_1) \fun (s',w'_1)}
 {(s,w_1 \pll w_2) \fun (s',w'_1 \pll w_2)}
\]

\[
\inferrule
 {(s,w_2) \fun (s',w'_2)}
 {(s,w_1 \pll w_2) \fun (s',w_1 \pll w'_2)}
\]

\[
\inferrule
 {}
 {(s,\done \pll \done) \fun (s,\done)}
\]



\subsubsection{Operation of \texttt{selection}.}

Here selection is just non-deterministic choice
\[
\inferrule
 {(s,w_1) \fun (s',w'_1)}
 {(s,w_1 \sel w_2) \fun (s',w'_1)}
\]

\[
\inferrule
 {(s,w_2) \fun (s',w'_2)}
 {(s,w_1 \sel w_2) \fun (s',w'_2)}
\]


\subsubsection{Operation of \texttt{iteration}.}

These rules are very simple: an iteration will perform zero of more
repetitions, the number performed being chosen non-deterministically.
\[
\inferrule
 {}
 {(s,w^*) \fun (s,\done)}
\]

\[
\inferrule
 {}
 {(s,w^*) \fun (s,w;w^*)}
\]

\subsection{On Non-Determinism}

The semantics presented above simply views any choice as non-deterministic.
In particular, the state of the system has no influence.
In practise, PML will favour choices that ``make sense'', choosing the
branch that makes most sense in the current context.
For selection, this means picking a process whose initial basic actions
are currently enabled, i.e. have their required resources.
For iteration, it means repeating until the required resources
have come available for the following process.

These properties are context-sensitive,
and so cannot strictly be given a structured operational semantics (SOS)
as shown here. In effect we need extra conditions that talk about
concepts like the set of initial actions of a process,
or the process that is the successor of the current one.
This kind of information can be captured in the state by
giving every process at every label a unique name,
and maintaining tables that map those names to the properties just described.
