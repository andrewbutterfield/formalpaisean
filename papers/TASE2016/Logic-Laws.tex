\HDRa{Logic Laws}

A listing of common and useful logic laws and definitions.

\section{Syntax}

\subsection{Types}

We make use of a rich variety of given types and type constructors:
\RLEQNS{
   S,T \in Type
   &::=& \One  & \say{unit type}
\\&\Alt& \Bool & \say{boolean values}
\\&\Alt& \Char & \say{(ASCII) characters}
\\&\Alt& \Nat \Alt \Int \Alt \Rat \Alt \Real \Alt \Cmpx & \say{numbers}
\\&\Alt&\Set T & \say{set of }T
\\&\Alt& T_1 \times \cdots \times T_n & \say{cross-product}
\\&\Alt& \Seq T & \say{sequence of }T
\\&\Alt& S \fun T & \say{(total) function from }S~\say{ to }T
\\&\Alt& S \pfun T & \say{(partial) function from }S~\say{ to }T
}

\subsection{Expressions}

 We build basic expressions
 out of constants (\m{k \in Const}),
 variables (\m{v \in Var}), tuples,
 functions and operators:
\RLEQNS{
     k &\in& Const & \say{constants of all types}
\\   v,x &\in& Var & \say{variables (plain, dashed, subscripted)}
\\   E \in Expr
     &::=& k  & \say{constants}
\\ &\Alt& v & \say{variables}
\\ &\Alt& E_1\;E_2 &\say{function application}
\\ &\Alt& \lambda v @ E &\say{lambda abstraction}
\\ &\Alt& E_1 \oplus E_2 & \say{infix operator application}
\\ &\Alt& (E_1\ldots,E_n) & \say{tuples}
\\ &\Alt& \setof{E_1\ldots,E_n} & \say{sets}
\\ &\Alt& \seqof{E_1\ldots,E_n} & \say{sequences}
\\ &\Alt& (E:T)  & \say{type annotation}
\\ &\Alt& \theta v @ P
\\ \oplus &::=&
\multicolumn{2}{l}{
     * \Alt / \Alt \cap \Alt + \Alt - \Alt \cup \Alt \le \Alt <  \Alt > \Alt \geq
\Alt \subset \Alt \subseteq \Alt \supset \Alt \supseteq \Alt \in \Alt = \Alt \neq
}
\\ e \in Expr & \multicolumn{3}{l}{\say{expressions with no dashed variables}}
}
Note that the last expression construct uses a predicate,
so $Expr$ and $Pred$ syntaxes are mutually recursive.

\subsection{Predicates}

\RLEQNS{
   A,B,C \in AtmPred &::=& E:\Bool & \say{atomic predicates}
\\
\\ P \in Pred &::=& \true & \say{always true}
\\ &|& \false & \say{always false}
\\ &|& A & \say{atomic predicates}
\\ &|& \lnot P & \say{logical negation}
\\ &|& P_1 \land P_2 & \say{conjunction (logic-and)}
\\ &|& P_1 \lor P_2 & \say{disjunction (logic-or)}
\\ &|& P_1 \implies P_2 & \say{implication}
\\ &|& P_1 \equiv P_2 & \say{equivalence}
\\ &|& \forall v:T @ P & \say{universal quantification (for-all)}
\\ &|& \exists v:T @ P & \say{existential quantification (there-exists)}
\\ &|& \exists_1 v:T @ P & \say{unique existence quantification}
\\ &|& [ P ] & \say{universal closure}
\\
\\ c \in Pred & \multicolumn{3}{l}{\say{predicates with no quantifiers or dashed variables}}
}

\subsection{Definitions}

\RLEQNS{
   (P = Q) &\defs& [P\equiv Q]             & \elabel{def:pred:eq}
}

\subsection{Infix Operator Precedence}

Here we list negation and the
binary connectives in order of decreasing precedence:
$$\begin{array}{|c|l|}
  \hline
  \say{op. symbols} & \say {description}
\\\hline
   *,/ & \say{multiplicative}
\\
   \cap & \say{intersection}
\\\hline
   +,- & \say{additive}
\\
   \cup & \say{union}
\\\hline
   \le,<,>,\geq  & \say{comparisons}
\\
   \subset,\subseteq,\supset,\supseteq & \say{subset}
\\\hline
   \in & \say{membership}
\\\hline
   =,\neq & \say{\emph{expression} equality}
\\\hline
   \lnot & \say{negation}
\\\hline
   \land & \say{logical-and}
\\\hline
   \lor & \say{logical-or}
\\\hline
   ; & \say{sequential composition}
\\\hline
   \cond c & \say{conditional}
\\\hline
   \implies, \impliedby & \say{implication}
\\\hline
   \refines, \refinedby & \say{refinement}
\\\hline
   \equiv,\iff,\not\equiv  & \say{equivalence}
\\\hline
   =,\neq & \say{\emph{predicate} equality}
\\\hline
\end{array}$$


\section{Metatheorems}

If $P$ is a theorem, then so is $[P]$ and $\forall x,y,\ldots @ P$,
for arbitrary variables $x,y,\ldots$.

In effect a proof that $P$ is a theorem is a proof of $[P]$,
i.e., that $[P]$ is true.

As a consequence of this, we generally write theorems of the form $P \equiv Q$
as $P = Q$ instead.

Any proposition tautology of the form $P = Q$
can be re-written as $[P] = [Q]$.
Proof:
\begin{eqnarray*}
\START{[\OLD{P}] = [Q]}
\EQ{by tautology $P=Q$}
\THIS{\OLD{[\NEW{Q}] = [Q]}}
\EQ{same predicate each side}
\THIS{\NEW{\True}}
\end{eqnarray*}



\newpage
\section{Propositional Logic}

\subsection{Axioms}

\subsection{Laws}



\subsubsection{Equivalence and \true}
\RLEQNS{
      \true && & \elabel{true}
\\    P &=& P & \elabel{refl:eqv}
\\    \true &=& Q \equiv Q & \elabel{refl:eqv:alt}
\\    P\equiv Q &=& Q \equiv P &  \elabel{comm:eqv}
\\    ((P\equiv Q)\equiv R) &=& (P\equiv (Q\equiv R)) & \elabel{assoc:eqv}
}

\subsubsection{Disjunction}
\RLEQNS{
      P\lor Q &=& Q \lor P                            & \elabel{comm:or}
\\    (P\lor Q) \lor R &=& P \lor (Q \lor R)          & \elabel{assoc:or}
\\    P \lor P &=& P                                  & \elabel{idem:or}
\\    P \lor (Q \equiv R) &=& P\lor Q \equiv P \lor R & \elabel{distr:or:eqv}
\\    P &\lor& \lnot P                                & \elabel{exc:middle}
\\    P \lor \true &=& \true                          & \elabel{zero:or}
\\    P \lor \false &=& P                             & \elabel{unit:or}
\\    P \lor (Q\lor R) &=& (P\lor Q) \lor (P \lor R)  & \elabel{distr:or:or}
}

\subsubsection{Conjunction}
\RLEQNS{
      P \land Q &\defs& P \equiv Q \equiv P \lor Q          & \elabel{def:conj}
\\    P \land Q &=& Q \land P                               & \elabel{comm:and}
\\    (P\land Q) \land R &=& P \land (Q \land R)            & \elabel{assoc:and}
\\    P \land P &=& P                                       & \elabel{idem:and}
\\    P \land \true &=& P                                   & \elabel{unit:and}
\\    P \land \false &=& \false                             & \elabel{zero:and}
\\    P \land (Q \land R) &=& (P \land Q) \land (P \land R) & \elabel{distr:and:and}
\\    P \land \lnot P &=& \false                            & \elabel{contradiction}
}


\subsubsection{Disjunction and Conjunction}
\RLEQNS{
      P \land (P\lor Q) &=& P                               & \elabel{abs:and:or}
\\    P \lor (P \land Q) &=& P                             & \elabel{abs:or:and}
\\    P \land (\lnot P \lor Q) &=& P \land Q                & \elabel{abs:and:nor}
\\    P \lor (\lnot P \land Q) &=& P \lor Q                & \elabel{abs:or:nand}
\\    P \lor (Q \land R) &=& (P \lor Q) \land (P \lor R)   & \elabel{distr:or:and}
\\    P \land (Q \lor R) &=& (P \land Q) \lor (P \land R)  & \elabel{distr:and:or}
\\    \lnot (P \land Q) &=& \lnot P \lor \lnot Q            & \elabel{demorgan:nand}
\\    \lnot (P \lor Q) &=& \lnot P \land \lnot Q            & \elabel{demorgan:nor}
\\    (P \equiv Q) \land (R \equiv P) &=& (P \equiv Q) \land (R \land Q)
                                                           & \elabel{distr:and2:eqv}
\\    P \equiv Q &=& (P\land Q) \lor (\lnot P \land \lnot Q) & \elabel{alt:eqv}
\\    P \not\equiv Q &=& (\lnot P \land Q) \lor (P \land \lnot Q) & \elabel{alt:neqv}
}

\subsubsection{Negation, Inequivalence and \false}
\RLEQNS{
      \false &\defs& \lnot \true & \elabel{def:false}
\\    \lnot(P \equiv Q) &=& (\lnot P) \equiv Q & \elabel{distr:not:eqv}
\\    (P \not\equiv Q) &\defs& \lnot(P\equiv Q) & \elabel{def:neqv}
\\    \lnot P \equiv Q &=& P \equiv \lnot Q & \elabel{contra:neqv}
\\    \lnot \lnot P &=& P & \elabel{invol:not}
\\    \lnot \false &=& \true & \elabel{not:false}
\\    \lnot P \equiv P &=& \false & \elabel{alt:false}
\\    (P \not\equiv Q) &=& (Q\not\equiv P) & \elabel{comm:neqv}
\\    ((P \not\equiv Q) \not\equiv R) &=& (P\not\equiv (Q \not\equiv R)) & \elabel{assoc:neqv}
\\    ((P \not\equiv Q) \equiv R) &=& (P \not\equiv (Q\equiv R)) & \elabel{swap:neqv:eqv}
}


\subsubsection{Implication}
\RLEQNS{
      P \implies Q &\defs& \lnot P\lor Q & \elabel{def:impl}
\\    P \impliedby Q &\defs& Q \implies P & \elabel{def:impby}
\\    P \implies Q &=& P \land Q \equiv P & \elabel{meet:ante}
\\    P \implies Q &=& P \lor Q \equiv Q & \elabel{join:cnsq}
\\    P \implies Q \lor R    &=& (P \implies Q) \lor (P \implies R) & \elabel{cnsq:or}
\\    P \implies Q \land R   &=& (P \implies Q) \land (P \implies R) & \elabel{cnsq:and}
\\    (P \lor Q) \implies R  &=& (P \implies R) \land (Q \implies R) & \elabel{ante:or}
\\    (P \land Q) \implies R &=& (P \implies R) \lor (Q \implies R) & \elabel{ante:and}
\\    P \implies Q &=& \lnot Q \implies \lnot P & \elabel{contra}
\\    P \implies (Q\equiv R) &=& (P \implies Q) \equiv (P \implies R) & \elabel{distr:impl:eqv}
\\    P \land Q \implies R &=& P \implies (Q \implies R) & \elabel{distr:and:impl}
\\    P \land (P \implies Q) &=& P\land Q & \elabel{abs:and:ante}
\\    P \land (Q \implies P) &=& P & \elabel{abs:and:cnsq}
\\    P \lor (P \implies Q) &=& \true & \elabel{abs:or:ante}
\\    P \lor (Q \implies P) &=& Q\implies P & \elabel{abs:or:cnsq}
\\    P \lor Q \implies P \land Q &=& P \equiv Q & \elabel{alt:eqv}
\\    P \implies P &=& \true & \elabel{triv:impl}
\\    P \implies \true &=& \true & \elabel{rzero:impl}
\\    \true \implies P &=& P & \elabel{lunit:impl}
\\    P \implies \false &=& \lnot P & \elabel{runit:inv:impl}
\\    \false \implies P &=& \true & \elabel{lzero:inv:impl}
\\    (P \implies Q) \land (Q \implies R) &\implies& (P \implies R) & \elabel{trans:impl}
\\    (P \equiv Q) \land (Q \implies R) &\implies& (P \implies R) & \elabel{trans:eqv:impl}
\\    (P \implies Q) \land (Q \equiv R) &\implies& (P \implies R) & \elabel{trans:impl:eqv}
}



\newpage
\section{Free/Bound Variables}

\subsubsection{Expression Free Variables}


\RLEQNS{
   \fv &:& Expr \fun \Set Var &\elabel{sig:fv:expr}
\\ \fv k &\defs& \emptyset &\elabel{def:fv:const}
\\ \fv v &\defs& \setof v &\elabel{def:fv:var}
\\ \fv (E_1\;E_2) &\defs& \fv E_1 \cup \fv E_2 &\elabel{def:fv:app}
\\ \fv (\lambda v @ E) &\defs& \fv E \setminus \setof v &\elabel{def:fv:lam}
\\ \fv (E_1 \oplus E_2)  &\defs& \fv E_1 \cup \fv E_2 &\elabel{def:fv:op}
\\ \fv (E_1\ldots,E_n) &\defs& \bigcup\setof{\fv E_1,\ldots,\fv E_n} &\elabel{def:fv:tuple}
\\ \fv \setof{E_1\ldots,E_n} &\defs& \bigcup\setof{\fv E_1,\ldots,\fv E_n} &\elabel{def:fv:set}
\\ \fv \seqof{E_1\ldots,E_n} &\defs& \bigcup\setof{\fv E_1,\ldots,\fv E_n} &\elabel{def:fv:seq}
\\ \fv (E:T) &\defs& \fv E &\elabel{def:fv:typ}
\\ \fv (\theta v @ P) &\defs& \fv P \setminus \setof v &\elabel{def:fv:the}
}

\subsubsection{Predicate Free Variables}

\RLEQNS{
   \fv &:& Pred \fun \Set Var  &\elabel{sig:fv:pred}
\\ \fv \true &\defs& \emptyset &\elabel{def:fv:true}
\\ \fv \false &\defs& \emptyset &\elabel{def:fv:false}
\\ \fv A &\defs& \fv A \mbox{ (as Expression)} &\elabel{def:fv:atom}
\\ \fv(\lnot P) &\defs& \fv P &\elabel{def:fv:not}
\\ \fv( P_1 \land P_2) &\defs& \fv P_1 \cup \fv P_2 &\elabel{def:fv:and}
\\ \fv( P_1 \lor P_2) &\defs& \fv P_1 \cup \fv P_2 &\elabel{def:fv:or}
\\ \fv( P_1 \implies P_2) &\defs& \fv P_1 \cup \fv P_2 &\elabel{def:fv:impl}
\\ \fv( P_1 \equiv P_2) &\defs& \fv P_1 \cup \fv P_2 &\elabel{def:fv:eqv}
\\ \fv( \forall v:T @ P) &\defs& (\fv P) \setminus \setof v &\elabel{def:fv:all}
\\ \fv( \exists v:T @ P) &\defs& (\fv P) \setminus \setof v &\elabel{def:fv:any}
\\ \fv( \exists_1 v:T @ P) &\defs& (\fv P) \setminus \setof v &\elabel{def:fv:one}
\\ \fv [ P ] &\defs& \emptyset &\elabel{def:fv:univ}
}

\newpage
\section{Substitution}

\subsection{Axioms}

\subsubsection{Expression Substitution}



\RLEQNS{
   \_[\_,\_] &:& (Expr \times (Expr\times Var)) \fun Expr & \elabel{sig:subs:expr}
\\ k[e/x] &\defs& k & \elabel{def:sub:const}
\\ v[e/x] &\defs& v, \quad v \neq x & \elabel{def:sub:var:othr}
\\ x[e/x] &\defs& e & \elabel{def:sub:var:self}
\\ (E_1\;E_2)[e/x] &\defs& E_1[e/x] \; E_2[e/x] & \elabel{def:sub:app}
\\ (\lambda v @ E)[e/v] &\defs& \lambda v @ E & \elabel{def:sub:lam:blkd}
\\ (\lambda v @ E)[e/x] &\defs& \lambda v @ E[e/x], \quad v \notin e & \elabel{def:sub:lam:free}
\\ (\lambda v @ E)[e/x] &\defs& \lambda w @ E[w/v][e/x],
   \quad v \in e, w \notin E,e,x  & \elabel{def:sub:lam:trap}
\\  (E_1 \oplus E_2)[e/x] &\defs& E_1[e/x] \oplus E_2[e/x] & \elabel{def:sub:op}
\\  (E_1\ldots,E_n)[e/x] &\defs& (E_1[e/x],\ldots,E_n[e/x]) & \elabel{def:sub:tuple}
\\  \setof{E_1\ldots,E_n}[e/x] &\defs& \setof{E_1[e/x],\ldots,E_n[e/x]} & \elabel{def:sub:set}
\\  \seqof{E_1\ldots,E_n}[e/x] &\defs& \seqof{E_1[e/x],\ldots,E_n[e/x]} & \elabel{def:sub:seq}
\\  (E:T)[e/x] &\defs& (E[e/x]:T) & \elabel{def:sub:typ}
\\ (\theta v @ P)[e/v] &\defs& \theta v @ P & \elabel{def:sub:the:blkd}
\\ (\theta v @ P)[e/x] &\defs& \theta v @ P[e/x], \quad v \notin e & \elabel{def:sub:the:free}
\\ (\theta v @ P)[e/x] &\defs& \theta w @ P[w/v][e/x],
   \quad v \in e, w \notin E,e,x & \elabel{def:sub:the:trap}
}

\subsubsection{Predicate Substitution}

\RLEQNS{
   \_[\_,\_] &:& (Pred \times (Expr\times Var)) \fun Pred & \elabel{sig:subs:pred}
\\ \true[e/x] &\defs& \true & \elabel{def:sub:true}
\\  \false[e/x] &\defs& \false & \elabel{def:sub:false}
\\  A[e/x] &\defs&  A[e/x] \say{ (as Expression)} & \elabel{def:sub:atom}
\\ (\lnot P)[e/x] &\defs& \lnot (P[e/x]) & \elabel{def:sub:not}
\\ ( P_1 \land P_2)[e/x] &\defs& ( P_1[e/x] \land P_2[e/x]) & \elabel{def:sub:and}
\\ ( P_1 \lor P_2)[e/x] &\defs& ( P_1[e/x] \lor P_2[e/x]) & \elabel{def:sub:or}
\\ ( P_1 \implies P_2)[e/x] &\defs& ( P_1[e/x] \implies P_2[e/x]) & \elabel{def:sub:implies}
\\ ( P_1 \equiv P_2)[e/x] &\defs& ( P_1[e/x] \equiv P_2[e/x]) & \elabel{def:sub:equiv}
\\ ( \forall v:T @ P)[e/v] &\defs& \forall v:T @ P & \elabel{def:sub:all:blkd}
\\ ( \forall v:T @ P)[e/x] &\defs& (\forall v:T @ P[e/x]), \quad v \notin e & \elabel{def:sub:all:free}
\\ ( \forall v:T @ P)[e/x] &\defs& (\forall w:T @ P[w/v][e/x]),
   \quad  v \in e, w \notin P,e,x & \elabel{def:sub:all:trap}
\\ ( \exists v:T @ P)[e/v] &\defs& \exists v:T @ P  & \elabel{def:sub:any:blkd}
\\ ( \exists v:T @ P)[e/x] &\defs& (\exists v:T @ P[e/x]), \quad v \notin e  & \elabel{def:sub:any:free}
\\ ( \exists v:T @ P)[e/x] &\defs& (\exists w:T @ P[w/v][e/x]),
   \quad  v \in e, w \notin P,e,x  & \elabel{def:sub:any:trap}
\\ ( \exists_1 v:T @ P)[e/v] &\defs& \exists_1 v:T @ P   & \elabel{def:sub:one:blkd}
\\ ( \exists_1 v:T @ P)[e/x] &\defs& (\exists_1 v:T @ P[e/x]), \quad v \notin e & \elabel{def:sub:one:free}
\\ ( \exists_1 v:T @ P)[e/x] &\defs& (\exists_1 w:T @ P[w/v][e/x]),
   \quad  v \in e, w \notin P,e,x & \elabel{def:sub:one:trap}
\\ {}[ P ][e/x] &\defs& [ P ]  & \elabel{def:sub:univ}
}


\subsection{Laws}

\RLEQNS{
   P[x/y][y/x]  &=&  P, \quad x \notin P & \elabel{inv:subs}
\\ P[e/x][f/x]  &=&  P[e[f/x]/x] & \elabel{comp:subs}
\\ P[e/x][f/y]  &=&  P[f/y][e/x], \quad x \notin f, y \notin e & \elabel{pcomm:subs}
\\ P[e,f/x,y]  &=&  P[f,e/y,x] & \elabel{comm:subs}
\\ P[e/x][f/y]  &=&  P[e,f/x,y], \qquad y \notin e & \elabel{mrg:subs}
\\ \lefteqn{P[e_1,\ldots,e_n/x_1,\ldots,x_n][f/y]}
\\ &=&
   P[e_1,\ldots,e_n,f/x_1,\ldots,x_n/y],
   \quad y \notin e_1,\ldots,e_n & \elabel{mrg:subs}
\\ x = e \land P
   &=&
   x = e \land P[e/x] & \elabel{eq:and:subs}
\\ (x = e \land P \implies Q)
   &=&
  (x = e \land P \implies Q[e/x]) & \elabel{eq:impl:subs}
}


\newpage
\section{Quantifiers}

\subsection{Axioms}

\subsection{Laws}


\subsubsection{Universal Quantification}

\RLEQNS{
   (\forall x @ P)  &=&  (\forall y @ P[y/x]),
   \quad y \notin P & \elabel{alfa:all}
\\ (\forall x | x = E \implies P)  &=&  P[E/x]),
   \quad x \notin E & \elabel{1pt:all}
\\ (\forall x @ P) \land (\forall x @ Q )
        &=&
       (\forall x @ P \land Q) & \elabel{distr:all:and}
\\ (\forall x @ (\forall y @ P))
        &=&
       (\forall y @ (\forall x @ P)) & \elabel{comm:all}
\\ (\forall x,y @ P)
        &=&
       (\forall x @ (\forall y @ P)) & \elabel{nest:all}
\\ P \lor (\forall x @ Q)  &=&  (\forall x @ P \lor Q),
  \quad {x \notin P} & \elabel{distr:all:or}
\\
  (\forall x | R @ P) &\defs& (\forall x @ R \implies P ) & \elabel{def:rng:all}
}

\subsubsection{Universal Closure}

\RLEQNS{
   ~[ P ]  &=&  \forall x_1,\ldots,x_n @ P,
    \quad \fv P \subseteq \setof{x_1,\ldots,x_n} & \elabel{def:univ}
\\ ~[ \true ]  &=&  \true & \elabel{univ:true}
\\ ~[ \false ]  &=&  \false & \elabel{univ:false}
\\ ~[ P \land Q ]  &=&  [P] \land [Q] & \elabel{distr:and:univ}
\\ ~[ [ P ] ]  &=&  [ P ] & \elabel{nest:univ}
\\ ~[ \forall x_1,\ldots,x_n @ P ]  &=&  [ P ] & \elabel{intro:univ:all}
\\ ~[ x=e \implies P ]  &=&  [ P[e/x] ], \quad x\notin e & \elabel{1pt:univ}
\\ ~[ P ]  &=&  [ P ] \land P[\vec e/\vec x],
   \quad \alpha P = \setof{\vec x} & \elabel{inst:univ}
}



\subsubsection{Existential Quantification}

\RLEQNS{
   (\exists x @ P) &\defs& \lnot(\forall x @ \lnot P) & \elabel{def:any}
\\ (\exists x @ P)  &=&  (\exists y @ P[y/x] ),
   \quad y \notin  P & \elabel{alfa:any}
\\ (\exists x @ x = E \land P)  &=&  P[E/x],
   \quad x \notin E & \elabel{1pt:any}
\\ (\exists x @ P) \lor (\exists x @ Q )
        &=&
       (\exists x @ P \lor Q) & \elabel{distr:any:or}
\\ (\exists x @ (\exists y @ P))
        &=&
       (\exists y @ (\exists x @ P)) & \elabel{comm:any}
\\ (\exists x,y @ P)
        &=&
       (\exists x @ (\exists y @ P)) & \elabel{nest:any}
\\ P \land (\exists x @ Q)  &=&  (\exists x @ P \land Q ),
  \quad {x \notin P} & \elabel{distr:any:and}
\\ \exists x | R @ P &\defs& \exists x @ R \land P & \elabel{def:rng:any}
\\ \exists_1 x @ P
   &\defs&
   (\exists x @ P)
\\ && {} \land (\forall y,z @ P[y/x] \land P[z/x] \implies y=z)
   & \elabel{def:one}
}


\section{More Implication}

Logical implication ($\implies$) plays a key role in reasoning about semantics,
and so here we collect some more complex laws and principles that help
in reasoning.

\subsection{``Consequences'' of Implication}

$$
\begin{array}{ccccc}
   & A \implies B & = & \lnot A \lor B &
\\ (A = A \land B) &=& A \implies B &=& (B = B \lor A)
\\ A \implies (B \implies P) &=& (A \land B) \implies P &=& B \implies (A \implies P)
\end{array}
$$
\begin{eqnarray*}
   A \implies (P \land Q) &=& (A \implies P) \land (P \implies Q)
\\ A \implies (P \lor Q)  &=& (A \implies P) \lor  (P \implies Q)
\\ (A \land B) \implies P &=& (A \implies P) \lor  (B \implies Q)
\\ (A \lor  B) \implies P &=& (A \implies P) \land (B \implies Q)
\\ (A \implies B) \implies P &=& (\lnot A \implies P) \land (B \implies P)
\\ (A \implies P) \implies (B \implies Q)
   &=&
   (B \implies A \lor Q) \land (P \land B \implies Q)
\end{eqnarray*}
The laws suffice to break any nesting of implications
down an equivalent form with no nested implications%
\footnote{
There may be a normal form here, but we don't plan to investigate further at present.
}
.

\subsection{Conjunctive Healthiness and Implication}

A conjunctive healthiness condition \H\ is one expressed in the form
\[
  \H(P)  \defs  P \land H
\]
so that $P = P \land H$.
If we have a conditional execution of $P$,
expressed as $C \implies P$ (say),
then we would like that to be healthy too, but
\begin{eqnarray*}
  & & (C \implies P) \land H
\\&=& (C \lor \lnot H) \implies P \land H
\\&=& (C \lor \lnot H) \implies P
\\&\neq& C  \implies P
\end{eqnarray*}
However if we have a collection of such conditional executions,
such that the conditions are exhaustive.

Given conditions $A$, $B$ and $C$ such that
\begin{eqnarray*}
  && A \lor B \lor C
\end{eqnarray*}
and programs $P$, $Q$ and $R$ that are conjunctively healthy:
\begin{eqnarray*}
  P \land H &=& P \\
  Q \land H &=& Q \\
  R \land H &=& R
\end{eqnarray*}
then we can prove
\begin{eqnarray*}
   \left(
     \begin{array}{ccc}
       (A & \implies & P) \land {}\\
       (B & \implies & Q) \land {} \\
       (C & \implies & R)
     \end{array}
   \right)
   \land H
   &=&
   \left(
     \begin{array}{ccc}
       (A & \implies & P) \land {}\\
       (B & \implies & Q) \land {} \\
       (C & \implies & R)
     \end{array}
    \right)
\end{eqnarray*}
We shall first simplify the rhs using all our assumptions,
and then show how this is invariant when made healthy.
\RLEQNS{
\lefteqn{
   \left(
     \begin{array}{ccc}
       (A & \implies & P) \land {}\\
       (B & \implies & Q) \land {} \\
       (C & \implies & R)
     \end{array}
   \right)
}
\EQ{Defn. of $\implies$}
\\&& \left(
     \begin{array}{ccc}
       (\dnot{A} & \lor & P) \land {}\\
       (\dnot{B} & \lor & Q) \land {} \\
       (\dnot{C} & \lor & R)
     \end{array}
     \right)
\EQ{$\lor$--$\land$ distributivity}
\\&&
  \left(\begin{array}{l@{~+~}l}
    \dnot{A} \cdot \dnot{B} \cdot \dnot{C}
    &
    P \cdot \dnot{B} \cdot \dnot{C}
  \\
          \dnot{A}
    \cdot \dnot{B}
    \cdot R
    &
          P
    \cdot \dnot{B}
    \cdot R
  \\
          \dnot{A}
    \cdot Q
    \cdot \dnot{C}
    &
          P
    \cdot Q
    \cdot \dnot{C}
  \\
          \dnot{A}
    \cdot Q
    \cdot R
    &
          P
    \cdot Q
    \cdot R
  \end{array}\right)
\EQ{De-morgan}
\\&&
  \left(\begin{array}{l@{~+~}l}
          \dnot{A + B + C}
    &
          P
    \cdot \dnot{B}
    \cdot \dnot{C}
  \\
          \dnot{A}
    \cdot \dnot{B}
    \cdot R
    &
          P
    \cdot \dnot{B}
    \cdot R
  \\
          \dnot{A}
    \cdot Q
    \cdot \dnot{C}
    &
          P
    \cdot Q
    \cdot \dnot{C}
  \\
          \dnot{A}
    \cdot Q
    \cdot R
    &
          P
    \cdot Q
    \cdot R
  \end{array}\right)
\EQ{Assumption: $A+B+C$, so first entry is false}
\\&&
  \left(\begin{array}{l@{~+~}l}
          \false
    &
          P
    \cdot \dnot{B}
    \cdot \dnot{C}
  \\
          \dnot{A}
    \cdot \dnot{B}
    \cdot R
    &
          P
    \cdot \dnot{B}
    \cdot R
  \\
          \dnot{A}
    \cdot Q
    \cdot \dnot{C}
    &
          P
    \cdot Q
    \cdot \dnot{C}
  \\
          \dnot{A}
    \cdot Q
    \cdot R
    &
          P
    \cdot Q
    \cdot R
  \end{array}\right)
\EQ{Simplify}
\\&& P \cdot \dnot{B} \cdot \dnot{C}
  +
          \dnot{A}
    \cdot \dnot{B}
    \cdot R
    +
          P
    \cdot \dnot{B}
    \cdot R
  +
          \dnot{A}
    \cdot Q
    \cdot \dnot{C}
    +
          P
    \cdot Q
    \cdot \dnot{C}
  +
          \dnot{A}
    \cdot Q
    \cdot R
    +
          P
    \cdot Q
    \cdot R
\EQ{Abbreviate by $RHS$}
\\&& RHS
}
We now take the lhs, which is $RHS \land H$:
\RLEQNS{
\lefteqn{RHS \land H}
\EQ{$\land$--$\lor$ distributivity}
\\&& P \cdot \dnot{B} \cdot \dnot{C} \cdot H
  + \ldots
\EQ{$P \cdot H = P$, by assumption}
\\&& P \cdot \dnot{B} \cdot \dnot{C} + \ldots
\EQ{Every term in rhs contains at least one of $P$, $Q$, $R$}
\MORE{and so all absorb ${} \cdot H$}
\\&& RHS
}
Proof complete.


\section{Expression Definitions and Laws}

Note, the laws from the Z Mathematical Toolkit apply here.

\subsection{Arithmetic}

\subsection{Sets}x


\subsection{Sequences}

\subsection{Maps}

