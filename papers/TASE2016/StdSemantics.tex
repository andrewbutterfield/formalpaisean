\subsection{Standard PML Framework}

\subsection{Introduction}

We are going to define a standard PML semantics framework in which
we can investigate and relate at least three interpretations of
a PML description:
\begin{description}
  \item[Strict]
    the behaviour follows strictly according to the control-flow structure,
    becoming deadlocked if control requires execution of actions
    whose required resources are not available.
  \item[Flexible]
    the behaviour is guided by the control flow,
    but actions can run out of sequence if their resources become available prematurely.
  \item[Weak]
    control flow is completely ignored,
    and execution simply iterates the non-deterministic choice of actions
    whose resources are available (also know as the ``dataflow interpretation'').
\end{description}
Here we define a common semantic core for all of the above interpretations,
that includes:
\begin{itemize}
  \item a common set of observation variables
  \item a common set of healthiness conditions
  \item a common semantics for general notions of execution.
  \item a common semantics for resource expressions
  \item a common semantics for the execution of a basic action.
\end{itemize}

\subsection{Observation Variables}


We introduce three observation variables:
\begin{description}
  \item[$ok : \Bool$]
    ---termination; and
  \item[$r : Res$]
    ---the  resource-state; and
  \item[$h : Name^*$]
    ---completed action history.
\end{description}
We construct predicates describing a homogeneous relation over
$(ok,r,h)$ as a UTP theory of Designs.

\subsection{Healthiness Conditions}

We are giving a Design semantics to PML,
so the usual healthiness conditions ($\HH{} = \HH1\circ\HH2$) hold.

However, we have a history observable that is like a reactive system
trace, and we need to have similar healthiness conditions,
here specialised for  PML, and called \PML.


Like Reactive theory in UTP, \PML\ has three parts,
labelled \PP{1,2,3},
which capture healthiness characteristics similar
to \RR{1,2,3}, but where histories ($h$)
replace traces ($tr$),
and noting that our PML model has no counterparts
for either $ref$ or $wait$.

\subsubsection{No Time Travel}

\RLEQNS{
   \PP1(P) &\defs& P \land h \leq h' & \elabel{def:mk:P1}
}

\subsubsection{No Direct Historical Memory}

\RLEQNS{
   \PP2(P) &\defs& \exists h_0 @ P[h_0,h_0\cat(h'-h)/h,h']
   & \elabel{def:mk:P2}
}
A simpler (equivalent) form of \isPP2 is
\RLEQNS{
   \isPP2(P) &=& (P = P[\nil,h'-h/h,h'])
   &\elabel{alt:is:P2}
}

\subsubsection{No Change before Start}

\RLEQNS{
   \PP3(P) &\defs& P \cond{ok} (r'=r \land h'=h)
   & \elabel{def:mk:P3}
}


\subsubsection{Healthiness Properties}

All three healthifiers are idempotent and monotonic:
\RLEQNS{
   \PP1 &=& \PP1 \circ \PP1                             & \elabel{idem:P1}
\\ \PP2 &=& \PP2 \circ \PP2                             & \elabel{idem:P2}
\\ \PP3 &=& \PP3 \circ \PP3                             & \elabel{idem:P3}
\\ P \refinedby Q &\implies& \PP1(P) \refinedby \PP1(Q) & \elabel{mono:P1}
\\ P \refinedby Q &\implies& \PP2(P) \refinedby \PP2(Q) & \elabel{mono:P2}
\\ P \refinedby Q &\implies& \PP3(P) \refinedby \PP3(Q) & \elabel{mono:P3}
}
The are also all mutually commutative:
\RLEQNS{
\\ \PP1 \circ \PP2 &=& \PP2 \circ \PP1 & \elabel{comm:P1:P2}
\\ \PP1 \circ \PP3 &=& \PP3 \circ \PP1 & \elabel{comm:P1:P3}
\\ \PP3 \circ \PP2 &=& \PP2 \circ \PP3 & \elabel{comm:P3:P2}
}
A consequence of all of this is we can define full WP-healthiness
by composing the healthifiers in any order, so we choose the following
as being most likely to be the simplest:
\RLEQNS{
   \PML &\defs& \PP3 \circ \PP1 \circ \PP2 & \elabel{def:P}
\\ \PML(P) &=& (\exists h_0 @ P[h_0,h_0\cat(h'-h)/h,h']) \land h \leq h'
\\        && \cond{ok}
\\        && r'=r \land h'=h
          & \elabel{expand:P}
}

\subsection{General Execution}

\subsubsection{Assignment}

We define assignment as a shorthand notation,
noting that it by itself does not guarantee healthiness.
\RLEQNS{
   h := e  &\defs&  \true \design r'=r \land h' = e & \elabel{def:h:asg}
\\ r := e  &\defs&  \true \design r'=e \land h' = h & \elabel{def:r:asg}
}

We shall now define the semantics of both tasks and PML descriptions using Designs over the alphabet described above.

\subsubsection{Skip ($\Skip$)}


In order to capture those cases when a task does nothing,
we will use the notion of a weak PML version of Design $Skip$
which we call $\Skip_{WP}$ (or $\Skip$ when clear from context).
\RLEQNS{
	\Skip_{WP}
	&\defs&
	\PP3( \true
          \design
          r' = r \land h' = h )
    & \elabel{def:skip}
}
We see
that $\Skip$ leaves the resources and the history of a system unchanged.

We have a proof obligations,
to show that $\Skip$ obeys \PML\
(\HH1,\HH2 and \PP3 are automatic because we use $\design$ and \PP3).

\RLEQNS{
   \PML(\Skip) &=& \Skip & \elabel{skip:is:P}
}

\subsection{Basic Actions}


\subsubsection{Task Body $\TSKB{rr}{pr}$}

We say a task is:
\begin{description}
  \item[$blocked$]
    when not all of its required resources are present;
  \item[$ready$] when all its required resources are present; and
  \item[$done$]
   when all of the resources it provides are present.
\end{description}

At any point in time, a $ready$ task can execute,
in which case all its provided resources become present,
and it enters the $done$ state.
As a result of the resources being updated,
previously $blocked$ tasks may become ready,
or even done.
If no task is in the $ready$ state,
and all tasks are $done$, we have successful termination
of the PML process.
If none are $ready$, but some are $blocked$ then we have a \emph{deadlock}.

We need to be a little more precise:
in order to make the action system behave sensibly,
we need to define exclusive situations that cover all possibilities.
Let $R$ be true when all required resources are present,
and $D$ be true when all provided resources are present.
Then we can envisage the following four cases:
\[
\begin{array}{|c|c|c|}
\hline
  \lnot R & \lnot D & blocked
\\\hline
  R & \lnot D & ready
\\\hline
  R & D & done
\\\hline
  \lnot R & D & redundant
\\\hline
\end{array}
\]
The last case can arise if provided resources become present
through the action of some other task, for example.

\[
  TASK\_STATE
  ::=
%  none
%  \alt
  blocked
  \alt
  ready
  \alt
%  active
%  \alt
%  suspended
%  \alt
  done
  \alt
  redundant
\]

We are interested in deriving all the possible histories of a system,
where a history is a sequence of task names, showing one possible execution
ordering.


We can define predicates that, given a $TBody$, and resource set,
test for a specific task state,
except that for now we conflate $blocked$ and $redundant$,
as in neither case can the task do anything, as it lacks required resources.
\RLEQNS{
   isBlk, isRdy, isDone &:& Res \fun TBody \fun \Bool
\\ isBlk_{r}(\TSKB{rr}{pr}) &\defs& \lnot rr[r/\rho]
\\ isRdy_{r}(\TSKB{rr}{pr})
   &\defs&
   rr[r/\rho] \land \lnot pr[r/\rho]
\\ isDone_{r}(\TSKB{rr}{pr})
   &\defs&
   rr[r/\rho] \land pr[r/\rho]
}


A task does nothing ($\Skip$) if $blocked$ or $redundant$,
performs its action ($\Delta$) if $ready$
and makes a non-deterministic choice between nothing
or redoing its action if $done$.
\RLEQNS{
   \TSKN N {tbody}
   &\defs& & \elabel{def:task}
\\ && (isBlk_{r}(tbody) \implies \Skip)
   & \elabel{def:task:blk}
\\ &\land&  (isRdy_{r}(tbody) \implies \Delta\TSKN N {tbody})
   & \elabel{def:task:rdy}
\\ &\land&  (isDone_{r}(tbody) \implies (\Skip \sqcap \Delta\TSKN N {tbody}))
   & \elabel{def:task:done}
\\ \Delta\TSK N {rr} {pr}
   &\defs&
   \PP3(~\true \design (\TSKB{rr}{pr})[r,r'/\rho,\rho'] \land h' = h\cat\seqof N~)
   & \elabel{def:do:task}
}
Note that action $\Delta(N,ps)$ is not idempotent
as it extends $h$ every time.

As before, the \HH1 and \HH2 conditions are fairly immediate
as both $\Skip$ and $\Delta$ use $\design$,
and $\demc$ preserves these.

We still need to demonstrate \PML:

\RLEQNS{
   \PML(\TSKN N {tbody}) &=& \TSKN N {tbody} & \elabel{task:is:P}
}


\subsection{Laws of Common Core PML}

\subsubsection{Skip as Assignment}

\RLEQNS{
	\Skip &=& r,h := r, h & \elabel{skip:is:id-asg}
}
