\HDRa{Introduction}\label{ha:intro}

We define a UTP semantics for  shared global state concurrency,
along with code to perform calculations with those semantics.
The need for such code will become rapidly apparent.

\HDRb{Haskell Literacy}

This entire document is largely a Haskell Literate Script (LHS),
in which Haskell code is embedded in \LaTeX\ sources,
inside \verb"\begin{code}...\end{code}" environments.
Often, however, such documents are very hard to read,
forcing the reader, Haskell-literate or otherwise,
to plough through reams of boilerplate code to get to the interesting stuff.
We address this by relegating almost all of the boilerplate material
to appendices.
The Haskell code associated with the ``interesting'' material
does not really require the reader to have read the boilerplate,
as the nature of the code makes it largely self-explanatory in the
context in which it appears, with perhaps an occasional foray
to the definition of the main datatypes (\ref{hc:ExprData},\ref{hc:PredData}).

Given the presentation order as described above,
it is worth giving a brief overview of the key elements in the program:
\begin{itemize}
  \item
    The key datatypes are \texttt{Expr} (\secref{hc:ExprData}
    and \texttt{Pred} (\secref{hc:PredData})
      denoting expressions and predicates respectively.
      These are parametric in an un-interpreted state type \texttt{s}.
  \item
    A dictionary of type \texttt{Dict} (\secref{hb:DataDict}),
     also parametric in \texttt{s},
    is constructed to support various calculation activities.
    It maps a \texttt{String} to an \texttt{Entry} that currently has 3 variants:
    \begin{description}
      \item[Function Defn.]~\\
        Maps a function name to its definition,
        as well as an direct hard-coded evaluator, and a pretty-printer.
        The latter is ueful in implementing convenient shorthand notations
        (e.g. Set Membership (\secref{hd:membership}, \figref{fig:short:membership}).
      \item[Alphabet]~\\
        Maps various observable category names to current alphabet variables
      \item[Predicate Var.]~\\
        Associates specific alphabets with specific predicate variables.
        Currently used to establish $A$, $B$ and $C$ as atomic actions
        that only affect program variable state.
    \end{description}
\end{itemize}


\HDRc{Installing and Running}

The sources for this document,
plus a lot of other material to do with formal modelling of a language
called Process Modelling Language (PML)\cite{DBLP:journals/infsof/AtkinsonWN07},
can be found at

\begin{center}\small
\url{https://andrewbutterfield@bitbucket.org/andrewbutterfield/formalpaisean.git}
\end{center}
The sources for this calculator can be found in \texttt{semantics/src}.
Alternatively you may have obtained a zip archive of the \texttt{semantics} directory alone.
Running the software requires a reasonably recent version of GHC.
In any case:
\begin{itemize}
  \item
    Open a Terminal/MS-DOS/xterm window and \texttt{cd} into \texttt{src}.
  \item
    Enter at the command prompt:  \texttt{ghci SemanticCalc.lhs}
  \item
    Once GHCi has loaded, to see pre-built examples, enter \texttt{catalog}
  \item
    The calculator is invoked by \texttt{calculemus}
    with an argument of type \texttt{Pred s}
  \item
    Once the calculator has exited (command 'x'),
    then a transcript of the calculation can be obtained
    \emph{immediately afterwards} as follows:
    \begin{description}
      \item[Displayed in Terminal]~\\ \texttt{putStrLn \$ calcPrint it}
      \item[Written to File]~\\ \texttt{writeFile "transcript.txt" \$ calcPrint it}
    \end{description}
    Function \texttt{calcPrint} converts the result returned by \texttt{calculemus}
    into a Haskell \texttt{String}.
\end{itemize}

\HDRb{Background}

Our semantics is inspired closely by that of
UTPP \cite{DBLP:conf/icfem/WoodcockH02},
which translates a parallel program into an action system, whose semantics is
given using UTP. An action is viewed as a behaviour coupled with a guard that
establishes the conditions on observable state when that action may
begin to execute. An action system is a collection of such guarded actions,
that continually runs, with a non-deterministic choice being made whenever
multiple guards are enabled at the same time.
In terms of UTP's lattice-theoretic approach,
the system is a distributed join (non-deterministic choice) of all its basic actions,
where a disabled (false) guard behaves the like the join unit value (top/miracle).

\HDRb{Approach}

Our semantics steps back a slight bit from the action-system formulation,
in that guards are de-emphasised, at least syntactically,
but we still have the distributed join, with disabled actions as unit.
We did this to reduce some of the mechanism associated with Designs,
so as to make the nature of the parallel semantics easier to see.
However, we expect our semantics can easily be ``poured'' back into the
full action system framework without any difficulty.

Our key motivation in stepping away from action systems,
and the UTPP formulation was to explore if we could eliminate one of the
sources of non-compositionality in that semantics,
namely that, while each basic statement had a starting label,
its semantics was defined in terms of that label,
and the labels of \emph{whatever statements came after it}.
We wanted to get true compositionality, so that the semantics of any construct
was independent of its context,
and the semantics of any composite could be constructed from
the semantics of its components.
In the case of UTPP, it is not clear how much we have gained in terms
of calculational ease, but the UTP paradigm we have developed in order to
achieve this looks like it might be a framework for addressing a wider range
of semantics where compositionality is difficult.

One key issue we face in giving a semantics to the
Process Modelling Language (PML) \cite{DBLP:journals/infsof/AtkinsonWN07}
is that the iteration construct has no explicit halting condition
associated with it syntactically.
The expectation is that an iterated is repeated until it brings
about a state of affairs that enables \emph{what comes after it}.
This is semantically quite different
from the notion of labels present in UTPP,
and is definitely not compositional.
We believe that the UTP approach described here will allow us
to produce a semantics that is compositional:
the iteration simply repeats until it gets a stop signal;
sequential composition itself is then responsible
for sending that stop signal,
should it detect that what \emph{it sees as coming afterwards}
has become enabled in some way.

The key idea is that we use some observations to,
in effect, act as abstractions of the surrounding (local) context.
As actual context is built up by the use of language composites,
those observations are then instantiated in such a way
that either hides them as internal,
or lets them play an appropriate role at the higher level.

We shall start with an overview of the key intuitions behind our theory,
and then focus on the kinds of expressions we need to be able to handle
in order to define our semantics.
These expressions cover the notions of label generators,
and manipulating sets of labels or places.
We then present the formal UTP semantics of each language construct.

\HDRb{Document Structure}

The document is presented in an order that makes most sense
for a reader interested in the development of the semantic
theory.
This means that the Haskell code is presented in an ad-hoc order.

First,
the UTCP semantics is described and implemented
as module \texttt{UTCPSemantics} (\secref{ha:UTCP-semantics}),
and then we present the UTCP laws most useful for calculation,
in module \texttt{UTCPLaws} (\secref{ha:UTCP-laws}),
as well as those that relying on complex conditions
that we ask the user to assess (\texttt{UTCPCReduce}, \secref{ha:UTCP-cond:reduce}).
Future plans are described in the TO-DO section (\secref{ha:TO-DO}).
The rest of the document is made up of appendices,
starting with transcripts of calculations done using this
calculator (or earlier versions) for interesting cases (\secref{ha:sem-calcs}),
followed by the rest of the Haskell code,
plus a final section (\secref{ha:UTCP-examples})
that shows hand-crafted calculations
done using \LaTeX\ before the calculator was developed.
These went through \emph{four} iterations before the need to find a better way
sunk in properly---they are the key motivator driving the development of this
software.


\HDRc{Haskell Order (define before use)}
If you want to read the Haskell code in define-before-use order
then proceed as follows:
\begin{enumerate}
  \item PrettyPrint \secref{ha:pretty-printer}.
  \item CalcPredicates \secref{ha:calc-preds}
  \item CalcSteps \secref{ha:calc-steps}.
  \item CalcRun \secref{ha:calc-run}.
  \item UTCPSemantics \secref{ha:UTCP-semantics}.
  \item UTCPLaws \secref{ha:UTCP-laws}.
  \item UTCPCReduce \secref{ha:UTCP-cond:reduce}.
  \item SemanticCalc \secref{ha:sem-with-calc}.
\end{enumerate}

\HDRc{What's in a name?}

You may want to skip this section---it's a slightly rambling,
heavily quote-laden discussion of why we have renamed the theory
from UTPP to UTCP.

A key (mildly controversial?) notion is the distinction between ``parallel''
and ``concurrent'' programming%
\cite{%
HC:Marlow:08%
,HW:ParVsConc:14%
,GHCM:Marlow:09%
,wiki:conc-comp%
,wiki:par-comp%
,blog:Harper:11%
}.

A key portion of \cite{GHCM:Marlow:09} states:
\begin{quote}``
A concurrent program is one with multiple threads of control.
Each thread of control has effects on the world,
and those threads are interleaved in some arbitrary way by the scheduler.
We say that a concurrent programming language is non-deterministic,
because the total effect of the program may depend
on the particular interleaving at runtime.
The programmer has the tricky task of controlling this non-determinism
using synchronisation,
to make sure that the program ends up doing what it was supposed to do
regardless of the scheduling order.
And that’s no mean feat,
because there’s no reasonable way to test that you have covered all the cases.
This is regardless of what synchronisation technology you’re using:
yes, STM is better than locks,
and message passing has its advantages,
but all of these are just ways to communicate between threads
 in a non-deterministic language.
\par
A parallel program, on the other hand,
is one that merely runs on multiple processors,
with the goal of hopefully running faster than it would on a single CPU.
\par
So where did this dangerous assumption that Parallelism == Concurrency come from?
It’s a natural consequence of languages with side-effects:
when your language has side-effects everywhere,
then any time you try to do more than one thing at a time
you essentially have non-determinism
caused by the interleaving of the effects from each operation.
So in side-effecty \emph{(sic)} languages,
the only way to get parallelism is concurrency;
it’s therefore not surprising that we often see the two conflated.
''\end{quote}

From Wikipedia\cite{wiki:conc-comp}:
\begin{quote}``
Concurrent computing is a form of computing
in which several computations are executing
during overlapping time periods
--—concurrently---
instead of sequentially (one completing before the next starts).
This is a property of a system
--—this may be an individual program,
a computer,
or a network--—
and there is a separate execution point or "thread of control"
for each computation (``process'').
A concurrent system is one where a computation can make progress
without waiting for all other computations to complete
--—where more than one computation can make progress at ``the same time''.
''\end{quote}

From Wikipedia\cite{wiki:par-comp}:
\begin{quote}``
Parallel computing is a type of computation
in which many calculations are carried out simultaneously,
operating on the principle that large problems can often
be divided into smaller ones,
which are then solved at the same time.
There are several different forms of parallel computing:
bit-level, instruction-level, data, and task parallelism.
Parallelism has been employed for many years,
mainly in high-performance computing,
but interest in it has grown lately
due to the physical constraints preventing frequency scaling.
As power consumption (and consequently heat generation) by computers
has become a concern in recent years,
parallel computing has become the dominant paradigm in computer architecture,
mainly in the form of multi-core processors.
''\end{quote}

From Wikipedia\cite{wiki:par-comp}:
\begin{quote}``
Parallel computing is closely related to concurrent computing
--—they are frequently used together,
and often conflated,
though the two are distinct:
it is possible to have parallelism without concurrency
(such as bit-level parallelism),
and concurrency without parallelism
(such as multitasking by time-sharing on a single-core CPU).
In parallel computing,
a computational task is typically broken down in several,
often many,
very similar subtasks that can be processed independently
and whose results are combined afterwards,
upon completion.
In contrast,
in concurrent computing,
the various processes often do not address related tasks;
when they do, as is typical in distributed computing,
the separate tasks may have a varied nature
and often require some inter-process communication during execution.
''\end{quote}

From Robert Harper's blog\cite{blog:Harper:11}:
\begin{quote}``
The first thing to understand is
parallelism has nothing to do with concurrency.
Concurrency is concerned with nondeterministic composition of programs
(or their components).
Parallelism is concerned with asymptotic efficiency of programs
with deterministic behavior.
''\end{quote}

Aside (off-topic): He also says, in a reply to a comment made on the above:
\begin{quote}``
Incidentally,
another consequence of non-determinism making it hard to get provable efficiency
bounds on programs is that Haskell sucks.
It is nearly impossible to make accurate time or,
especially, space predictions about Haskell programs.
That’s why there’s so much emphasis on space profiling,
as opposed to space analysis.
Why is Haskell non-deterministic?
Because it is lazy, and,
as Gilles Kahn, the father of laziness,
said many decades ago:
laziness is all about concurrency.
''\end{quote}

We feel that the theory presented here,
and in the original UTPP work\cite{DBLP:conf/icfem/WoodcockH02},
is in fact setting up a framework for reasoning about concurrency.
This is precisely because it has threads
(any use of the parallel construct $\parallel$)
and it exposes the shared state-changes interactions/interference
in a very comprehensive and brutal way:
an iteration whose body is
one massive non-deterministic choice between guarded atomic actions,
with some (attempt at?) orchestration by language control-flow constructs.


The upshot of all of this is that we shall call our theory a
``Unifying Theory of Concurrent Programming'' (UTCP).

Now, back to the matter in hand \ldots.
