\subsection{Resource Semantics}

\subsubsection{Resource Syntax}

We reprise part of the resource/expression syntax here
\begin{eqnarray*}
   v \in Var &::=& i \alt (i) \alt (i)v
\\ a \in Attr &::=& v.i
\\ u \in Val && s
           \alt n
           \alt a
\\ e \in Expr &::=& s
               \alt v
               \alt a
               \alt u \bowtie u
               \alt v = v
\end{eqnarray*}
We have omitted the boolean operators, whose semantics is obvious
and just consider variable equality here, whose semantics is precisely
full structural equality of the two instances of $v$.

\subsubsection{Resource States}

We interpret a resource as a predicate over a resource-state structure,
which is a partial mapping from a resource-name to a resource-state.
A resource-state at least records the existence of the corresponding resource, as well as any resource-attributes that may be associated with same, and a list of zero of more resource-qualifiers.
A resource-attribute consists of a attribute-name,
and optionally a corresponding attribute value.
So we can consider a resource-state as a possibly empty partial map
from attribute-names to attribute values.
If a resource does not exist,
then it is not present in the resource-state structure.
\begin{eqnarray*}
   \rho \in Res &=& ResName \pfun ResState
\\ (q,\varrho),\varsigma \in ResState &=& AttrQual^* \times (AttrName \pfun AttrVal)
\\ \nu \in AttrVal &=& Self \alt Num \alt Str \alt Id
\\ r \in ResName &=& Id
\\ a \in AttrName &=& Id
\end{eqnarray*}

\subsubsection{Semantic Definitions}

The meanings are as follows, where $\rho$ denotes the current resource-state.
\RLEQNS{
   i &\defs& i \in \dom\rho & \elabel{def:res:i}
\\ (i_1)(i_2)\cdots(i_n)i
   &\defs&
   i \in \dom\rho \land \pi_1(\rho i) = \seqof{i_1,i_2,\ldots,i_n}
   & \elabel{def:res:q:i}
\\ i_1 . i_2 &\defs& i_1 \in \dom\rho \land i_2 \in \dom\pi_2(\rho i_1)
   & \elabel{def:res:i.i}
\\ (i_1)(i_2)\cdots(i_n)i . i_a
   &\defs& (i_1)(i_2)\cdots(i_n)i \land i.i_a
   & \elabel{def:res:q:i.i}
\\ v.i \bowtie s
   &\defs&
   v.i \land (\pi_2(\rho v))(i) \bowtie s
   & \elabel{def:res:v.i:rel:s}
\\ s \bowtie v.i
   &\defs&
   v.i \land s \bowtie (\pi_2(\rho v))(i)
   & \elabel{def:res:s:rel:v.i}
\\ v.i \bowtie n
   &\defs&
   v.i \land (\pi_2(\rho v))(i) \bowtie n
   & \elabel{def:res:v.i:rel:n}
\\ n \bowtie v.i
   &\defs&
   v.i \land n \bowtie (\pi_2(\rho v))(i)
   & \elabel{def:res:n:rel:v.i}
\\ v_1.i_1 \bowtie v_2.i_2
   &\defs&
   v_1.i_1 \land v_2.i_2 \land
   (\pi_2(\rho v_1))(i_1) \bowtie (\pi_2(\rho v_2))(i_2)
   & \elabel{def:res:v.i:rel:v.i}
\\ v_1 = v_2
   &\defs&
   v_1 \land (v_1=v_2)
   & \elabel{def:res:v:eq:v}
}
We overload $Res$ lookup over $Var$ by taking $\rho((i_1)(i_2)\cdots(i_n)i)$
to be $\rho i$.
Also we treat values and variables as predicates
when they occur in a predicate context, and as values when in some comparison
relation.

This list is partial, as for now we don't have sensible resource meanings for the
following expressions:
\[
 s \quad (i) \quad  s \bowtie n \quad n \bowtie n \quad n \bowtie s \quad (i).i \bowtie u
\]

\subsubsection{Task Semantics}

Now, consider a task body as follows
\[
 \TSKB{rr}{pr}
 \qquad
 \mbox{--- \textbf{r}equired and \textbf{p}rovided \textbf{r}esources}
\]
where both $rr$ and $pr$ are a single expression
interpreted as a predicate as outlined above.
The standard notation with lists of resources:
\[
  \TSKB{rs}{ps}
\]
can be viewed as shorthand for the following use of conjunction:
\[
  \TSKB{\bigwedge rs}{\bigwedge ps}
\]
The semantics of such a task-body is
\RLEQNS{
   \TSKB{rr}{pr}
   &\defs&
   resof(pr) :\![ rr, pr ]
   & \elabel{def:task:frame-spec}
\\ resof &:& Expr \fun \Set ResName & \elabel{sig:resof}
\\ resof(\lnot e) &\defs& resof(e) & \elabel{def:resof:not}
\\ resof(e_1 \land e_2) &\defs& resof(e_1)\cup resof(e_2)
   & \elabel{def:resof:and}
\\ resof(e_1 \lor e_2) &\defs& resof(e_1)\cup resof(e_2)
   & \elabel{def:resof:or}
\\ resof((i)^*i) &\defs& \setof i & \elabel{def:resof:rname}
\\ resof((i)^*i.i_a) &\defs& \setof i & \elabel{def:resof:attr}
\\ resof(u_1 \bowtie u_2) &\defs& resof(u_1) \cup resof(u_2) & \elabel{def:resof:rel}
\\ resof(v_1 = v_2) &\defs& \setof v & \elabel{def:resof:eq}
\\ resof(e) &\defs& \nul & \elabel{def:resof:default}
}
Here $(i)^*i$ is shorthand for a pattern of the form
$(i_1)...(i_n)i$, where $n \geq 0$.
We can elaborate a resource frame specification in this context as
something that ensured that $pr$ is true at the end (a.k.a. $pr'$)
while also asserting that only resources mentioned in the resource-set
$rs$ can be altered. We ensure above that $rs = resof(pr)$.
\RLEQNS{
   rs:\![rr,pr]
   &\defs& rr \design pr' \land \rho'\setminus rs = \rho\setminus rs
   & \elabel{def:pml:res-frame}
}
\subsubsection{Simplified Resource Semantics}

Finally we can abstract from details so that $\rho$ is flattened
to a set of resource names and a resource is interpreted as simply
being a membership test. Attributes and qualifiers are simply ignored.

Here the semantics of a task body is very simple:
\RLEQNS{
   \TSKB{rs}{ps}
   &\defs&
   rs \in \rho \design \rho'=\rho \cup ps
   & \elabel{def:task:simple}
}
Note: This approach is not currently in use.
