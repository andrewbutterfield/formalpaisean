\HDRa{Syntax of PML}

Here we present the various forms of PML\cite{DBLP:journals/infsof/AtkinsonWN07}
syntax we use for semantics work.

\section{The `Standard' Syntax}

The concrete syntax is defined using BNFC,
as described in \S\ref{BNFC:Syntax}.

\subsection{PML Statement Syntax}

The concrete syntax is a bit verbose for formal work,
so we define a more concise shorthand as per the following table:

\begin{center}
\begin{tabular}{|l|p{2in}|c|}
  \hline
  Concept & Concrete & Abstract
\\\hline
  Basic Action
& \texttt{
    action N\{
    \newline \phantom{mmmm}requires\{r\}
    \newline \phantom{mmmm}provides\{p\} \}
  }
& $\TSK N r p$
\\\hline
  Sequential Composition
& \texttt{
    sequence \{
    \newline \phantom{mmmm}w1
    \newline \phantom{mmmm}w2 \}
  }
& $ w_1 ; w_2 $
\\\hline
  Parallel Composition
& \texttt{
    branch \{
    \newline \phantom{mmmm}w1
    \newline \phantom{mmmm}w2 \}
  }
& $ w_1 \pll w_2 $
\\\hline
  Choice, Alternative
& \texttt{
    selection \{
    \newline \phantom{mmmm}w1
    \newline \phantom{mmmm}w2 \}
  }
& $ w_1 \sel w_2 $
\\\hline
  Repetition (single)
& \texttt{
    iteration \{
    \newline \phantom{mmmm}w \}
  }
& $ w^* $
\\\hline
  Repetition (many)
& \texttt{
    iteration \{
    \newline \phantom{mmmm}w1
    \newline \phantom{mmmm}w2 \}
  }
& $(w_1 ; w_2)^*$
\\\hline
\end{tabular}
\end{center}

Or, more formally:
\begin{eqnarray*}
   N \in Name && \mbox{Process/Action Names}
\\ p,q,r \in Res && \mbox{Resources}
\\ u,v,w \in Work &::=& \TSK N {r^{*}} {r^{*}}
   \alt \; w;w \;
   \alt \;w \pll w \;
   \alt \; w\sel w \;
   \alt \; w^{*} \;
   \alt \; \done
\end{eqnarray*}
Here we add a construct $\done$ that we use to denote
a task/process that is ``done''. This makes the semantics a little simpler.

\subsection{PML Resource Syntax}

As for statements, the resource syntax is verbose,
so we define a more concise form.
The key idea here is that we interpret a resource syntax as
defining a predicate that needs to hold of the current state of system resources.

\begin{center}
\begin{tabular}{|l|p{2in}|c|}
  \hline
  Concept & Concrete & Abstract
\\\hline
  Logical-Or
& \texttt{
    e1 || e2
  }
& $e_1 \lor e_2$
\\\hline
  Logical-And
& \texttt{
    e1 \&\& e2
  }
& $e_1 \land e_2$
\\\hline
  Logical-Not
& \texttt{
    !e
  }
& $\lnot e_1$
\\\hline
  Relations
& \texttt{
    v1 <= v2
  }
& $v_1 \leq v_1$
\\\hline
  Qualified Identifies
& \texttt{
   ( q ) v
  }
& $(q)v$
\\\hline
  Attributes
& \texttt{
   v . a
  }
& $v.a$
\\\hline
\end{tabular}
\end{center}

Formally:
\begin{eqnarray*}
   s \in Str &::=& \mbox{``\ldots''}
\\ i \in Id && \mbox{Identifiers}
\\ n \in Num && \mbox{Numbers}
\\ v \in Var &::=& i \alt (i) \alt (i)v
\\ a \in Attr &::=& v.i
\\ u \in Val && s
           \alt n
           \alt a
\\ e \in Expr &::=& s
               \alt v
               \alt a
               \alt \lnot e
               \alt e \land e
               \alt e \lor e
               \alt u \bowtie u
               \alt v \sim v
\\ \bowtie &::=n& = \alt \neq \alt < \alt \leq \alt > \alt \geq
\\ \sim &::=& = \alt \neq
\end{eqnarray*}

\section{The `Weak' Syntax}

This is the simplified syntax used for the baseline weak semantics,
which simply views PML as a set of atomic tasks,
each specifying their required resources,
and what resources they provide once complete.
The resources are viewed as uninterpreted entities,
with resource specifications considered as predicates
identifying a particular subset of all resources.

We first posit the notion of names,
and resource specifiers:
\begin{eqnarray*}
   \NmDecl && \NmDefn
\\ \ResDecl && \ResDefn
\end{eqnarray*}
Then the notion of a task, with a tag (name),
and a body consisting of
lists of required and provided resources.
\begin{eqnarray*}
   \TaskDecl &::=& \TaskDefn
\\ \TbdyDecl &::=& \TbdyDefn
\end{eqnarray*}
We consider a task
$\TSK N {rqd_1,\ldots,rqd_m} {prv_1,\ldots,prv_n}$
well-formed if the resource sets $\{rqd_1,\ldots,rqd_m\}$
and $\{prv_1,\ldots,prv_n\}$ are disjoint.

We model a (weak) PML description as a map from names to task bodies
which automatically enforces that task names are unique:
\begin{eqnarray*}
  \WPMLDecl & ::=& \WPMLDefn
\end{eqnarray*}
Notationally we write a weak PML description as an enumeration
of uniquely named tasks:
\[
  \TSK{N_1}{q_1}{p_1},\;
  \TSK{N_2}{q_2}{p_2},\;
  \ldots,\;
  \TSK{N_n}{q_n}{p_n}
\]
but semantically these should be treated as a finite partial function:
\[
\mapof{
 N_1 \mapsto (\TSKB{q_1}{p_1}),
 N_2 \mapsto (\TSKB{q_2}{p_2}),
 \ldots,
 N_n \mapsto (\TSKB{q_n}{p_n})
}
\]
