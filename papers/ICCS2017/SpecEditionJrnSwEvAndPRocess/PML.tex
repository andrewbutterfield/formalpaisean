\section{PML and Process Flexibility}


The Process Modelling Language (PML)
\cite{DBLP:journals/infsof/AtkinsonWN07}
is a shared-state concurrent imperative language
that has been designed to model organizational/business processes.
PML models a process as a collection of atomic tasks, each of which
\texttt{requires} resources to start, and \texttt{provides} resources
when it completes.  PML uses a four constructs
to model process flow: \texttt{sequence}, \texttt{iteration}, \texttt{selection} and \texttt{branch}:
\begin{enumerate}
\item
Sequence: Models a series of tasks to be performed in the specified order:
\begin{lstlisting}
sequence {
  action_A {}
  action_B {}
  action_C {}
}
\end{lstlisting}
\item
Iteration: Models a series of tasks to be performed repeatedly:
\begin{lstlisting}
iteration {
  action_A {}
  action_B {}
  action_C {}
}
\end{lstlisting}
\item
Selection: Models a set of tasks from which only one can be chosen to be performed:
\begin{lstlisting}
selection {
  choice_A { }
  choice_B { }
  choice_C { }
}
\end{lstlisting}
\item
Branch: Models a set of concurrent tasks:
\begin{lstlisting}
branch {
  path_A { }
  path_B { }
  path_C { }
}
\end{lstlisting}
\end{enumerate}

The \texttt{iteration} and \texttt{branch} constructs in PML
are underspecified by design and
behave somewhat unusually.
For instance, the \texttt{iteration} construct has no explicit
termination condition.
PML acknowledges the flexible nature of processes
and leaves the decision to the \texttt{agent} responsible for enacting the process
to decide when the loop should terminate.

This means that a given \texttt{iteration} construct
can be interpreted in at least two different  ways:
\begin{itemize}
\item
The \texttt{agents} (people) enacting the business process
use their judgement to determine when
the actions in the body of the \texttt{iteration} have been repeated enough.
\item
The availability of resources in the system
serves as loop control.
The \texttt{iteration} stops when the
\texttt{required} resources of the action following the iteration are
available, and the \texttt{required} resources of the first action in
the body of the iteration are \emph{not} available.
\end{itemize}


Similarly flexible is the behaviour of the \texttt{branch} construct.
The decision on when to proceed beyond the branch join point
is left unspecified,  and thus is
left to the judgement of
the \texttt{agent} enacting the process model.


In essence, depending on the interpretation, the trace (enactment history) of
a specific process model can consist of:
\begin{inparaenum}
\item  an iteration of the non-deterministic choice of actions
whose resources are available;
\item a sequence of actions that is governed solely by
when the required resources become available; or
\item a precise pre-defined sequence of actions which
deadlock if the required resources are not available.
\end{inparaenum}

