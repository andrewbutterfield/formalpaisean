\section{Introduction}

Software development is a complex activity that requires frequent
adaptation on the part of developers to cope with external
and internal forces such as changing requirements, evolving design
knowledge, and failures.

Software development in regulated domains such as medical devices has
additional complexity due to the restrictions on the software process
imposed by regulations: depending on the potential impact of device
failure, regulations stipulate that certain processes must be followed
to 
manage risks and improve safety.   Organizations must
document how their processes comply with these regulations.

In this context, it becomes important to be able to document an
organization's development process in sufficient detail to satisfy
regulatory constraints, while still allowing developers as much
flexibility as possible within those constraints.  Ideally the same
notation should be used both to satisfy regulators and to inform
developers as to what tasks are required by regulations; this ensures
that changes to the process are reflected in both presentations.

To satisfy these requirements, we started with a process modeling
notation called ``PML'' that was specifically designed to be
lightweight and allow flexibility.  We then developed 
formal semantics for three levels of interpretation for PML:

\begin{enumerate}
\item Strict - the specified control flow and pre- and post-conditions dictate
  the exact order and conditions for enactment of the process.
\item Flexible - the specified control flow must be enacted as
  written, except that sequential steps can be re-ordered as long as
  pre- and post-conditions are met, and concurrent activities need not
  all complete before subsequent activities can start.
\item Weak - the specified control flow can be ignored: steps can be
  performed in any order as long as their pre-conditions are met.
  Steps can also be skipped if their post-conditions are met.
\end{enumerate}

This defines a hierarchy of flexibility from none to maximum.  The
implication is that a process specification could be written that has
a strict interpretation that satisfies regulatory constraints.  If the
weak interpretation can be shown to be equivalent, through comparison of the two
interpretations' semantics, then the regulators' need for compliance,
and developers' need for flexibility, are satisfied with a single
process and process description.

The remainder of this paper is organized as follows.  In the next
section, we introduce PML and provide an example that will serve to
illustrate the need for flexibility and, later, how formal semantics
can be used to analyze equivalence of strict, flexible, and weak
interpretations.  Following that we describe the formal semantics and
how they are derived using UTP.  Finally, we discuss related work,
then conclude with implications and future directions.



