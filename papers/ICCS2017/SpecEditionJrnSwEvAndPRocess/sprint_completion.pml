process sprint_completion {
  iteration finalize_issue {
    branch {
      action report_to_customer {
        script { "Notify customer that fix will be in next release." }
        requires { issue.status == "resolved" }
        provides { issue.cust_notified }
        agent { CustomerServiceRep }
      }
      selection {
        sequence branch_exists {
          action copy_to_branch { 
            script { "If fix should be included in branch, copy code to unit on branch." }
            requires { issue.status == "resolved" && unit.on_branch && !rel_branch.blocked}
            provides { issue.status == "resolved" && unit.changed_on_branch }
            agent { Developer }
          }
          action test_change_on_branch { 
            script { "QA tests change on branch" }
            requires { unit.changed_on_branch }
            provides { unit.tested_on_branch }
            agent { ExternalQA }
          }
        }
        action mark_for_future {
          script { "If branch is protected mark issue as needing to be copied to branch." }
          requires { issue.status == "resolved" && unit.on_branch && rel_branch.blocked }
          provides { issue.status == "resolved" && unit.on_branch && issue.needs_copy_to_branch }
          agent { Developer }
        }
      }
    }
  }
  sequence sprint_retrospective {
    iteration {
      action select_issue_for_demo {
        script { "Select fix for demonstration." }
        requires { issue.status == "resolved" }
        provides { issue.to_demo }
        agent { ProductOwner }
      }
    }
    iteration {
      action demo_issue {
        script { "Demonstrate fix." }
        requires { issue.to_demo }
        provides { issue.demoed }
        agent { Developer && ProductOwner && ScrumMaster}
      }
    }
  }
}
