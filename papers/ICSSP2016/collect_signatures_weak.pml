process collect_signatures {
  branch {
    action obtain_PM_sig { }
    action obtain_dept_head_sig { }
    action obtain_dir_sig { }
    action obtain_VP_sig { }
  }

  action distribute_document { }
}
