process collect_signatures {
  action obtain_PM_sig {
    requires { document_approval_form }
    provides { document_approval_form.pm_signed }
    agent { ProjectManager }
  }
  action obtain_dept_head_sig {
    requires { document_approval_form }
    provides { document_approval_form.dept_head_signed }
    agent { DepartmentHead }
  }
  action obtain_dir_sig {
    requires { document_approval_form }
    provides { document_approval_form.director_signed }
    agent { DivisionDirector }
  }
  action obtain_VP_sig {
    requires { document_approval_form }
    provides { document_approval_form.VP_eng_signed }
    agent { VPEngineering }
  }
  action distribute_document {
    requires { document && document_approval_form.pm_signed && document_approval_form.dept_head_signed && document_approval_form.director_signed && document_approval_form.VP_eng_signed }
    provides { document.distributed }
    agent { Author }
  }
}
