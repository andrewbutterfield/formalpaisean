\section{PML and Process Flexibility}


The Process Modelling Language (PML)
\cite{DBLP:journals/infsof/AtkinsonWN07}
is a shared-state concurrent imperative language
that has been designed to model organizational/business processes.
PML models a process as a collection of atomic tasks, each of which
\texttt{requires} resources to start, and \texttt{provides} resources
when it completes.  PML uses a four constructs
to model process flow: \texttt{sequence}, \texttt{iteration}, \texttt{selection} and \texttt{branch}:
\begin{enumerate}
\item
Sequence: Models a series of tasks to be performed in the specified order:
\begin{lstlisting}
sequence {
  action_A {}
  action_B {}
  action_C {}
}
\end{lstlisting}
\item
Iteration: Models a series of tasks to be performed repeatedly:
\begin{lstlisting}
iteration {
  action_A {}
  action_B {}
  action_C {}
}
\end{lstlisting}
\item
Selection: Models a set of tasks from which only one can be chosen to be performed:
\begin{lstlisting}
selection {
  choice_A { }
  choice_B { }
  choice_C { }
}
\end{lstlisting}
\item
Branch: Models a set of concurrent tasks:
\begin{lstlisting}
branch {
  path_A { }
  path_B { }
  path_C { }
}
\end{lstlisting}
\end{enumerate}

The \texttt{iteration} and \texttt{branch} constructs in PML
are underspecified by design and
behave somewhat unusually.
For instance, the \texttt{iteration} construct has no explicit
termination condition.
PML acknowledges the flexible nature of processes
and leaves the decision to the \texttt{agent} responsible for enacting the process
to decide when the loop should terminate.

This means that a given \texttt{iteration} construct
can be interpreted in at least two different  ways:
\begin{itemize}
\item
The \texttt{agents} (people) enacting the business process
use their judgement to determine when
the actions in the body of the \texttt{iteration} have been repeated enough.
\item
The availability of resources in the system
serves as loop control.
The \texttt{iteration} stops when the
\texttt{required} resources of the action following the iteration are
available, and the \texttt{required} resources of the first action in
the body of the iteration are \emph{not} available.
\end{itemize}


Similarly flexible is the behaviour of the \texttt{branch} construct.
The decision on when to proceed beyond the branch join point
is left unspecified,  and thus is
left to the judgement of
the \texttt{agent} enacting the process model.


In essence, depending on the interpretation, the trace (enactment history) of
a specific process model can consist of:
\begin{inparaenum}
\item  an iteration of the non-deterministic choice of actions
whose resources are available;
\item a sequence of actions that is governed solely by
when the required resources become available; or
\item a precise pre-defined sequence of actions which
deadlock if the required resources are not available.
\end{inparaenum}

\subsection{Initial Semantic Considerations}
\begin{figure}[t]
  \centering
  \lstinputlisting{collect_signatures.pml}
  \caption{PML specification of Collect Signatures process.}
  \label{fig:collect-signatures-listing}
\end{figure}

The flexibility of different semantic interpretations of
process terms allows PML to model a process at
different levels of granularity
and at each of those levels
to cater for context-sensitive
interpretations.

To illustrate the attractiveness of the idea,
let us consider a $Collect Signatures$ workflow
that is required in a document change approval process. An
organization must put such a process
in place in order to comply with US FDA regulation Title 21 Subchapter
H Part 820 ``Medical Devices Quality Management Regulation,''~\cite{FDA_2016_Quality} which states
in sub-part 820.20:
\begin{quote}
Each manufacturer shall designate an individual(s) to review for
adequacy and approve prior to issuance all documents established to
meet the requirements of this part. The approval, including the date
and signature of the individual(s) approving the document, shall be
documented.
\end{quote}

In our example, suppose the document must be approved by the Project
Manager, Department Head, Division Director, and Vice President of
Engineering.  This would be documented on a document change approval
form that collects signatures from these individuals.  A PML
specification for this process is shown in
\cref{fig:collect-signatures-listing}.

\begin{figure}[t]
  \centering
  \includegraphics[width=.5\textwidth]{collect_signatures.eps}
  \caption{Strict interpretation of Collect Signatures process.}
  \label{fig:collect-signatures-graph}
\end{figure}


A strict interpretation of this workflow requires that the signatures
be obtained in order: Project Manager (PM) first, then Department Head, then Division
Director, and finally Vice President of Engineering (see \cref{fig:collect-signatures-graph}).

A flexible interpretation, on the other hand, recognizes that the
document must have all the signatures before it can be distributed, but
the order is not really important.  As such, signatures could be
obtained in any order, when the individuals are available.  This
flexible interpretation is
depicted by the specification in \cref{fig:collect-signatures-flexible-graph}.  In this
interpretation, the process iterates over signature collection,
obtaining signatures one at a time, when the person is available (the
diamonds indicate selection: ``choose one of the following'').

\begin{figure}[t]
  \centering
  \includegraphics[width=.55\textwidth]{collect_signatures_flexible.eps}
  \caption{Flexible interpretation of Collect Signatures process.}
  \label{fig:collect-signatures-flexible-graph}
\end{figure}


A final interpretation would be that each person could sign a copy of
the signature page, and so the signatures could be collected in
parallel.  Once all signatures are obtained, the document can be
submitted.  This is shown in
\cref{fig:collect-signatures-weak-graph} (the circles indidate a
concurrent branch among all paths).  Note that in this
interpretation, the \texttt{requires} predicate for the
\texttt{distribute\_document} action enforces barrier synchronization: the
\texttt{distribute\_document} action cannot be performed until all of the
signatures have been collected, which means all paths of the
\texttt{branch} construct must complete.

\begin{figure}[t]
  \centering
  \includegraphics[width=.55\textwidth]{collect_signatures_weak.eps}
%  \lstinputlisting{collect_signatures_weak.pml}
  \caption{Weak interpretation of Collect Signatures process.}
  \label{fig:collect-signatures-weak-graph}
\end{figure}

Why not just use a specification that matches the weak
(\cref{fig:collect-signatures-weak-graph}) interpretation?  In some cases, this might
be the best approach.  However, there are some considerations that
make the initial specification (\cref{fig:collect-signatures-graph})
more appropriate.  This specification captures the \emph{intent} of
the process: that the signatures act as a series of gateways to ensure
that the Principle Investigator accepts responsibility for the
document, and that the document meets with departmental, institute, and executive
approval.  A hierarchical approval sequence ensures that executives
are not bothered with documents that don't meet departmental
standards.  At the same time, there are sometimes good reasons for
circumventing this hierarchical sequence: for example, one or more
individuals might be unavailable at the time their signature would be
needed in the sequence, but could convey their intent to sign upon
return; waiting for the strict sequence might result in missing the
submission deadline.  Also, our experience indicates that people tend
to describe processes as sequential even when the sequence is not
strictly needed; consequently, a sequential specification is initially
easier to validate.  Later, the specification might be evolved into a
concurrent model as part of a process improvement exercise.


Our motivation for analysing context-sensitive flexibility
comes from its relevance in modelling processes in regulated domains.
The typical quality assurance standards that regulate
safety critical domains, such as the ones that
regulate medical devices,
automotive software, avionics software  etc,
have both requirements that need to be followed strictly,
and parts which allow and in many occasions call for
a context-sensitive flexibility of interpretation.

For illustration purposes, let us consider the
Quality System (QS) Regulation -- Medical Device Good Manufacturing Practices
\cite{FDA15}
from the U.S. Food and Drug Administration (FDA).
Due to the fact that the QS regulation applies to a broad number of devices and processes,
it follows a flexible approach which prescribes the essential elements
to be incorporated in a manufacturer's quality process
without prescribing specifically how to enact these elements.
Furthermore, it is left to the manufacturer to determine which
specific quality assurance procedures to implement
according to their specific process or device.

This flexibility of enactment
gives rise to a number of interesting questions such as:
\begin{enumerate}
\item
A control flow perspective of process analysis:
which enactment paths
satisfy the regulatory requirements  and which
are rogue paths which could compromise
compliance to the regulatory standards?

\item
A data flow perspective of process analysis:
can we highlight instances of
resource black holes
(where a resource is produced but
not used by anyone/any part of the process)
and resource miracles
(where resources appear to materialize
out of nowhere)?
\end{enumerate}
