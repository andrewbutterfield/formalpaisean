\section{PML Formal Semantics}\label{sec:pml-formal}

We have defined not one, but three,
distinct but related formal semantics for PML.
This was done using a semantic framework known
as the Unifying Theories of Programming (UTP)~\cite{Hoare-He98}
for two main reasons:
first, the UTP framework makes it easy to formally relate the three semantic models.
Also, it facilitates linking these semantic models to other
application or domain specific resource semantics models.
For example, in the clinical pathway domain,
we could link our semantics to one obtained
from data and knowledge regarding drug interactions.

\subsection{UTP}

UTP
uses predicate calculus to define before-after relationships
over appropriate collections of free observation variables.
The before-variables are undashed,
while after-variables have dashes.
Some observations correspond to the values of program variables ($v,v'$),
while others deal with  important observations one might make of
a running program,
such as start ($ok$) and termination ($ok'$).
The set of observation variables associated with a theory
or predicate is known as its \emph{alphabet}.
For example,
the meaning of an assignment statement might be given as follows:
\begin{equation*}
  x := e  \qquad\defs\qquad  ok \implies ok' \land x' = e \land \nu'=\nu
\end{equation*}
The definition says that once started, the assignment terminates,
with the final value of variable $x$ set equal
to the value of expression $e$ in the before-state,
while the other variables, denoted collectively by $\nu$, remain unchanged.
UTP supports specification languages as well as programming
ones, and a general notion of refinement as universally-closed
reverse implication:
\begin{equation*}
  S \refinedby P \defs [ P \implies S ]
\end{equation*}
Typically the predicates used in a UTP theory form
a complete lattice under the refinement ordering,
with the most liberal specification ($Chaos$) as its bottom element,
and the infeasible program that satisfies
any specification ($Miracle$) as its top.
Iteration is then viewed as the usual least fixed point on this lattice.

\subsection{Semantics by three}

A PML description can be viewed as a named collection of basic actions,
defined in terms or the resources they need in order to start,
and the new resources they produce once they have completed.
These are all explicitly connected together using the control-flow
constructs:
\texttt{sequence}, \texttt{selection}, \texttt{iteration}, \texttt{branch}.
However,
this is also an implied flow-of-control ordering
induced by the required and provides clauses of each basic action.
Simply put, an action that requires resource $r$ can't run until
at least one other action runs that itself provides $r$.
It is this tension between the explicit and implied control-flow
that provides the basis for our three semantics for a PML description:
\begin{itemize}
	\item \textbf{Weak:}
	control flow is completely ignored,
	and execution simply iterates the non-deterministic choice of actions
	whose resources are available (also know as the ``dataflow interpretation'').
	\item \textbf{Flexible:}
	the behaviour is guided by the control flow,
	but actions can run out of sequence if their required resources become available
	before the control flow has determined that they should start.
	\item \textbf{Strict:}
	the behaviour follows strictly according to the control-flow structure,
	becoming deadlocked if control requires execution of actions
	whose required resources are not available.
\end{itemize}
We can demonstrate that these three semanitcs
%
(\cref{fig:pml-3-semantics})%
\begin{figure*}
   \includegraphics[width=\textwidth]{PML-3-Semantics.eps}
  \caption{The three semantics for PML}\label{fig:pml-3-semantics}
\end{figure*}
%
form a refinement-chain,
in the sense that any process enactment that satisfies
the strict semantics also satisfies the flexible
and weak semantics:
\begin{equation*}
\textrm{Weak} \refinedby \textrm{Flexible} \refinedby \textrm{Strict}
\end{equation*}
However satisfying the weak semantics does not guarantee satisfying
the flexible, or strict ones.

\subsection{Concurrency, Global State}

Regardless of which of the three semantics we consider,
a key common feature is that we are dealing with what is in effect
a concurrent programming language with global shared state.
Formal semantics for such languages are well established,
in connected denotational and operation form
e.g. \cite{DBLP:journals/iandc/Brookes96}
and \cite{DBLP:conf/concur/BoerKPR91},
and more recently in UTP \cite{DBLP:conf/icfem/WoodcockH02}.
What all of these have in common, along with our three versions\cite{BMN:TASE2016},
is that basically all such programs semantically reduce to
a top-level iteration
over a non-deterministic choice of all the currently enabled atomic
state-change actions.
In the case of PML, we consider the basic actions as atomic for now,
and an action is enabled if its \texttt{required} resources are present,
and if applicable control-flow also permits the action to proceed.
For the strict semantics, all the control-flow constructs are
applicable, whereas for the weak view, none of them have any sway.
In the flexible semantics, control flow constraints can be overridden
by `local' knowledge of actions not strictly scheduled, but actually
able to start in terms of resource provision.
By `local' we usually mean actions contained in the \texttt{sequence} construct
containing the actions in question.

In this concurrent semantics setting, 
if no action is enabled, 
but the top-level termination condition is not met, 
then we have deadlock.
In the PML context,
the termination condition is that every resource
mentioned in a \texttt{provides} clause
has actually been provided.
So a deadlock will arise if at any point in process enactment
that there are no tasks
that provide resources not currently present,
for which all their own required resources are present.

\subsection{Doing it in threes}

This is all very well,
but how does having these three semantic models assist with the
analysis of the deployment of processes defined in PML,
and their flexible enactment?
The most obvious approach is simply to take an enactment history
(which can be expressed as a \texttt{sequence} of basic actions)
and check it for compliance with the three semantics (Fig. \ref{fig:pml-3-semantics}),
or alternatively have a workflow tool that can enforce a designated
level of compliance.
But we can go further than this
-- one key win that we get from having these semantically interoperable
level of interpretation is that we can determine those situations
were the interpretation level does not matter.
Surprisingly this gives us useful information about the nature
of the process being modelled, and often guidance as to when flexibility
is feasible or not.

The first most obvious case is when the structure of the PML description
is simply a large \texttt{branch} (parallel) over basic actions.
In this case there are no control-flow constraints,
and we simply end up with a scheduling of basic actions limited
only by resource constraints.
A more interesting case is when we have a `perfect' \texttt{sequence},
namely one in which every basic action \texttt{requires} the resources
that are \texttt{provided} by its immediate predecessor in the sequence.
In this case, in the absence of any concurrent interfering process in
a different \texttt{branch},
we have that all three levels of semantics also coincide
-- the actions must occur in the order as written.
What this does, in terms of process enactment, is to act as a signal
that there is no flexibility in such `perfect' sequences,
and that the process needs to followed strictly.

\subsection{Three Semantics Overview}

The weak semantics is easiest:
the alphabet consists of $rs$ and $rs'$ where $rs$ is the set of resources
available before an action or process runs,
while $rs'$  denotes the resources available once it has completed.
A basic action checks $rs$ to see if its required resources
are available.
If not it is not enabled, which is modelled as a false predicate,
false being the unit for non-deterministic choice (logical disjunction).
If it is enabled, then its behaviour is to add is provided respources
into $rs'$.
So the meaning of
\begin{verbatim}
action A {
  requires { r1 }
  provides { r2 }
}
\end{verbatim}
is the predicate
\begin{equation*}
r_1 \in rs \land rs' = rs \cup \{ r_2 \}
\end{equation*}
When $r_1 \notin rs$ then it is $false$,
otherwise $rs'$ is $rs$ with $r_2$ added.

We simply collect up all the basic actions,
discarding any information about the enclosing constructs,
and then creates a non-deterministic choice
over all of them.
This is enclosed in a loop that repeats until there is no change
in the resources available.

For the strict semantics, we need to add in extra alphabet variables
in order to capture control-flow behaviour.
This involves the generation of unique labels for constructs
and adding some atomic control-flow actions that manipulate label
sets.
A basic action is now enabled when both its required resources are present,
and its control label is also in a designated label set ($ls$, $ls'$).
We do not give details here---these are to appear elsewhere (\cite{BMN:TASE2016}?).
The flexible semantics requires a few more alphabet variables to 
propagate action enabling information to surrounding flow of control constructs.

\subsubsection{Example}

We can now consider how our three semantics deal with the Collect Signatures example
(\cref{fig:collect-signatures-listing%
,fig:collect-signatures-graph%
,fig:collect-signatures-flexible-graph%
,fig:collect-signatures-weak-graph}).
A feature of all concurrent, shared-state semantics
is that they can be used to compute,
in principle at least,
all the possible execution orders that can arise,
given some starting state.
With the strict semantics, applied to this example,
we get only one sequence: \texttt{PM ; dept\_head ; dir ; VP ; distr\_doc}
(here we shorten action name \texttt{obtain\_XX\_sig} to \texttt{XX}),
which is precisely that of \cref{fig:collect-signatures-graph}.
With the weak semantics, the first four actions can occur in any order,
while the fifth must wait until all the previous four are done.
This gives 24 different interleavings of the four parallel actions
in \cref{fig:collect-signatures-weak-graph}.
In this case, because each arm of the selection in the flexible semantics
has only one action (\cref{fig:collect-signatures-flexible-graph}),
we get the same 24 interleavings as for the weak case.

\subsection{Current State}

The current state of development of these semantic models
is as follows
---
the extremal ones, weak and strict, are complete,
while the flexible semantics has thrown up a number of interesting
choices---there is a spectrum of possibilities here, depending
on how `local' the flexibility is.
The current plan is to formalise the degree of flexibility
that corresponds to the PML analysis and simulation tool described in
\cite{DBLP:journals/infsof/AtkinsonWN07}.
Regardless of the precise details of the flexible semantics,
or the number of variations of flexibility that may emerge,
they all will have the property of being refinements of the strict
semantics, and refining the weak one.