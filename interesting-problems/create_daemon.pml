process create_daemon {
  iteration {
    action initiate {
      requires { r }
      provides { blood_sample || urine_sample }
    }
    branch {
      action create_glucose_test {
        requires { 
          blood_sample 
        }
        provides { 
          (assert) new_proc.model == "glucose_test" && 
                   new_proc.blood_sample == blood_sample
        }
      }
      action create_urinalysis {
        requires { 
          urine_sample
        }
        provides { 
          (assert) new_proc.model == "urinalysis" && 
                   new_proc.urine_sample == urine_sample
        }
      }
    }
  }
  action end {
    requires { something_never_provided }
  }
}
