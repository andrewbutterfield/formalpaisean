% Finding Prerequisites for Process Creation
% John Noll
% _DATE_

# Introduction

Sometimes it makes sense to automatically create an executing instance
of a PML program.  For example, when a blood sample arrives at a
laboratory, a process should be started to analyze the sample, with
the sample bound to the process's input resource, and a particular
technician bound to the process's agent:

~~~~~ {.tcl include="../glucose_test.pml"}
~~~~~

This process can't begin until a blood sample is available, but should
be created immediately when one arrives.

# Problem

The enactment environment needs to know what resources a process
requires before it can start, and what state these resources should be
in.  PML does not explicitly state a process's required resources;
rather, the resources required by a *process* are the aggregate set of
resources required by it's initial action(s).

In the example above, the first action is `retrieve_record`; this
action will be blocked until a specific blood sample is bound to its
`blood_sample` resource.  Subsequent actions depend on the resources
provided by this action, so the set of required resources for the
process as a whole just contains `blood_sample`.

# Solution

A daemon process could monitor the resource repository and create
new processes when resources change.  A PML program to achieve this
might look like[^equality_syntax]:

~~~~~ {.tcl include="../create_daemon.pml"}
~~~~~

This program loops forever (assuming the `something_never_provided`
resource is, indeed, never created); each cycle through the loop will
look for matching required resources, which when found will assert the
provided resources.  The `assert` qualifier means the enactment engine
should ensure that the predicate is true if it's not already true[^assert].

Note: at present the enactment engine cannot do the computation required to
unify `process_instance` in the *requires*  predicates.

# Issues

One issue with this approach is how to bind new (or changed) resource
instances to resources in the *create_daemon* process.  For example,
when a new blood sample arrives at the lab, how does the enactment
environment know to bind this instance to `blood_sample` rather than
`urine_sample`?  

We can imagine that physical objects like these test samples would
have labels identifying their source and type.  So a clerk at the lab
could create a database record with these data for each incoming
sample.  The type could be used to bind the sample to the correct
resource name.

However, PML does not have a notion of resource type;  while names
like "blood_sample" convey the type to a human reader, the PML
compiler and enactment engine treats this as an untyped name that gets
bound to a value by some external mechanism.

In the current implementation, this is achieved with a combination of
two API procedures:

1. *peos_set_resource_binding(pid, resource_name, value)*
   Each process has it's own context (namespace) of resource bindings.
   This procedure binds *resource_name* to the specified *value*
   within the context of the process specified by *pid*.  
   
   
2. *peos_bind_resource_file(pid, resource_file)*
   This procedure reads resource bindings of the form "resourceName:
   value" from the specified file and
   adds them to the context specified by *pid*.

The values bound to resource names can be literals or TCL[^TCL]
expressions; this allows resource values to be derived from other
resource bindings.  For example, suppose the test results resource in
our *glucose_test* example is a file with the same name as the test
record, but with a ".results" extension.  Then, the value of the
`test_results`resource can be automatically derived by binding
`test_results` as follows:

    peos_set_resource_binding(pid, "test_results", "[file rootname ${test_record}].results")

These procedures can be combined to provide a facility for binding
resources for the *create_process* daemon, with the following components: 

+ A resource file that specifies
how resources in the process are bound, based on the type of the
resource instance:

    ~~~~~ 
        blood_sample: [isa $r "BloodSample"] ? $r : {}
        urine_sample: [isa $r "UrineSample"] ? $r : {}
    ~~~~~ 

+ A TCL procedure *isa* that compares a resource to a type:

    ~~~~~ {.tcl}
        proc isa r t {
          return [string equal [typeof $r] $t]
        }
    ~~~~~    

+  A TCL procedure *typeof* that examines the resource instance
and returns its type.  

    ~~~~~ {.tcl}
        set isa_predicates {}
        proc typeof r {
          global isa_predicates
          foreach p $isa_predicates {
            set t [$p $r]
            if {[string length $t] > 0} {return $t}
          }
          return {}
        }
        
        proc isa_BloodSample r {
          # code to determine whether r is a BloodSample
        }
        lappend isa_predicates isa_BloodSample
        
        proc isa_UrineSample r {
          # code to determine whether r is a BloodSample
        }
        lappend isa_predicates isa_UrineSample
    ~~~~~    

+ A wrapper that reacts to a resource event by binding `r` in
*create_daemon* to the new or changed resource, loads the resource
binding file, then executes *create_daemon*.

# Tool Requirements


What this solution needs is a tool to analyze a set of PML models
and generate three files: 

1. *create_daemon.pml* containing a PML program like *create_daemon*
above to create instances of processes from the set.

2. *create_daemon.res* containing resource bindings for the required
resources of *create_daemon.pml*.

3. *create_daemon.tcl* containing `isa_ResourceType` predicates for
each resource type.  This would need to be edited to implement the
predicate bodies.

The tool would need to:

1. Find the initial action(s) of each model.

2. Extract the required resource(s).

3. Create a new action with the required resource predicates from step
(2) as its required resources, and a *provides* predicate that
instantiates a new instance of the model.

4. Assemble the set of new actions into a PML specification with an
iteration as in *create_daemon* above.

5. Create a resource binding file for the required resource, with each
binding of the form:

    required_resource: : [isa $r "RequiredResource"] ? $r : {}

6. Create a  TCL file containing skeleton `proc` definitions of the
form:

~~~~~ {.tcl}
proc isa_RequiredResource r {
  # code to determine whether r is a RequiredResource
}
lappend isa_predicates isa_RequiredResource
~~~~~ 



[^equality_syntax]: PML doesn't support predicates that compare two
resources; this would have to be added in order for the proposed
solution to work.
[^assert]: As of this writing the enactment engine does not support
this qualifier.  It should be straightforward to add, at least for the
simple case discussed here.

[^TCL]: (Tool Command Language)[tcltk.com], an embeddable scripting
language created by John Ousterhout at UC Berkeley.
