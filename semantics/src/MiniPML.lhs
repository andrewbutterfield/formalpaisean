\section{A minimal version of PML}
\begin{code}
module MiniPML where
import APTypes
import Data.List
\end{code}

Here we define the datatypes, show instances
and basic utility functions for a stripped down version of PML

\subsection{miniPML Abstract Syntax}

\subsubsection{Resources and Names}

We first introduce the notion of resources, and (task) names
both  view as atomic and unstructured.
\begin{eqnarray*}
   r \in R &=& \mbox{Resources}
\\ nm \in Nm &=& \mbox{Names}
\end{eqnarray*}
\begin{code}
data Res = R { outR :: String } deriving (Eq,Ord)

instance Show Res where show = outR

data Name = N { outN :: String } deriving (Eq,Ord)

instance Show Name where show = outN
\end{code}

\subsubsection{Tasks}

A task has a name and two sets of resources,
the first being those required for the task to start,
the second being the resources provided by the task:
\begin{eqnarray*}
   t \in Task &::=&  nm:[ ?r^* \Rightarrow~ !r^*]
\end{eqnarray*}
\begin{code}
data Task
 = T { nm :: Name
     , required :: [Res]
     , provided :: [Res]
     } deriving (Eq, Ord)

instance Show Task where
  show (T nm rq pv)
     = show nm ++ ":[ ?" ++ lshow "," rq
               ++ " => !" ++ lshow "," pv ++ " ]"
\end{code}

\newpage
\subsubsection{Work}

A PML \emph{Work} description is either a \emph{Task},
or a composite that denotes one of:
sequencing,
concurrency,
selection
or iteration.
\begin {eqnarray*}
   w \in Work &::= & t
\\ &\alt& w~; w
\\ &\alt& \;w \parallel w
\\ &\alt& \; w\triangleleft\!\triangleright~ w
\\ &\alt & *w
\end{eqnarray*}
\begin{code}
data Work
 = Tsk Task
 | Seq Work Work
 | Par Work Work
 | Sel Work Work
 | Iter Work
 deriving (Eq,Ord)

instance Show Work where
  show (Tsk task) = show task
  show (Seq w1 w2) = show w1 ++ " ; " ++ show w2 -- ; binds tightest
  show (Par w1 w2) = "( " ++ show w1 ++ " || " ++ show w2 ++ " )"
  show (Sel w1 w2) = "( " ++ show w1 ++ " <|> " ++ show w2 ++ " )"
  show (Iter w@(Tsk _)) = "*(" ++ show w ++ ")"
  show (Iter w@(Seq _ _)) = "*(" ++ show w ++ ")"
  show (Iter w) = '*':show w
\end{code}

\newpage
\subsection{Work Invariant}

We consider a instance of work to be well-formed
if
\begin{enumerate}
  \item all the task names are unique
\end{enumerate}
\begin{code}
invWork :: Work -> Bool
invWork work
 = duplicateFree $ sort $ taskNames work -- 1.
 where
   duplicateFree xs = xs == nub xs
\end{code}

\begin{code}
taskNames :: Work -> [Name]
-- we do an inorder traverse.
taskNames (Tsk task)  = [nm task]
taskNames (Seq w1 w2) = taskNames w1 ++ taskNames w2
taskNames (Par w1 w2) = taskNames w1 ++ taskNames w2
taskNames (Sel w1 w2) = taskNames w1 ++ taskNames w2
taskNames (Iter w)    = taskNames w
\end{code}
