\section{Abstract Action Syntax}
\begin{code}
module SynAction where
import Utilities
import AbsPML
import SynResources
\end{code}

Here we define an optimised abstract syntax for PML actions and specifications
---
the parts that actually do the work!

\newpage
\subsection{PML Specifications}

We support an equivalent of the \texttt{SPEC} type,
but as it is only used in a list,
we also define a list-of-\texttt{SPEC} type that has the five varieties
in separate lists.

BNFC Types:
\begin{verbatim}
data SPEC = SpecProv EXPR  | SpecReqs EXPR
          | SpecAgent EXPR | SpecScript STRING | SpecTool STRING
data OPTYP = OptNull | OptMan | OptExec
data PRIM = ... | PrimAct ID OPTYP [SPEC]
\end{verbatim}

Simplified Types:
\begin{code}
data PMLSpec = Requires ResExpr
             | Provides ResExpr
             | Agent ResExpr
             | Script String
             | Tool String

data PMLSpecs
 = Spec{ requires :: [ResExpr]
       , provides :: [ResExpr]
       , agents   :: [ResExpr]
       , scripts  :: [String]
       , tools    :: [String]
       }

data OpType = Manual | Executable

type PMLAction = ( Maybe OpType, PMLSpecs )
\end{code}

Display:
\begin{code}
instance Show PMLSpec where
  show (Requires res) = showKeyThing "requires" (show res)
  show (Provides res) = showKeyThing "provides" (show res)
  show (Agent res)    = showKeyThing "agent"    (show res)
  show (Script str)   = showKeyThing "script"   str
  show (Tool str)     = showKeyThing "tool"     str

instance Show PMLSpecs where
 show specs
  = unlines'
    (   map (showKeyThing "requires" . show) (requires specs)
     ++ map (showKeyThing "provides" . show) (provides specs)
     ++ map (showKeyThing "agent" . show)    (agents   specs)
     ++ map (showKeyThing "script")          (scripts  specs)
     ++ map (showKeyThing "tool")            (tools    specs)
    )

showKeyThing key thing = key ++ '{' : thing ++ "}"

instance Show OpType where
  show Manual = "manual"
  show Executable = "executable"

showAction nm (mop, specs)
  = unlines' [ "action "++nm++mshow mop++" {"
             , indshow specs
             , "}"
             ]
\end{code}

\newpage
Conversion:
\begin{code}
transSPEC :: SPEC -> PMLSpec
transSPEC (SpecProv  expr) = Provides $ expr2Res expr
transSPEC (SpecReqs  expr) = Requires $ expr2Res expr
transSPEC (SpecAgent expr) = Agent    $ expr2Res expr
transSPEC (SpecScript (STRING str)) = Script str
transSPEC (SpecTool   (STRING str)) = Tool str

addSPEC :: SPEC -> PMLSpecs -> PMLSpecs
addSPEC (SpecProv  expr) specs
         = specs{provides = provides specs++[expr2Res expr]}
addSPEC (SpecReqs  expr) specs
         = specs{requires = requires specs++[expr2Res expr]}
addSPEC (SpecAgent expr) specs
        = specs{agents = agents specs++[expr2Res expr]}
addSPEC (SpecScript (STRING str)) specs
        = specs{scripts = scripts specs++[str]}
addSPEC (SpecTool   (STRING str)) specs
        = specs{tools = tools specs++[str]}

transOPTYP :: OPTYP -> Maybe OpType
transOPTYP OptNull = Nothing
transOPTYP OptMan  = Just Manual
transOPTYP OptExec = Just Executable

transACT optyp specs = ( transOPTYP optyp, foldr addSPEC noSpecs specs )
\end{code}

Handling:
\begin{code}
noSpecs :: PMLSpecs
noSpecs = Spec [] [] [] [] []

specResources :: PMLSpecs -> [ResExpr]
specResources specs = requires specs ++ provides specs ++ agents specs
\end{code}
