\section{Abstract PML Syntax}
\begin{code}
module SynPML where
import Utilities
import AbsPML
import SynResources
import SynAction
\end{code}

Here we define an optimised abstract syntax for PML that
removes some redundant aspects (e.g. \texttt{newtype STRING = STRING String}),
and simplifies the type hierarchy a little.

\newpage
\subsection{PML Primitives}

BNFC Types:
\begin{verbatim}
data OPTNM = OpNmNull | OpNmId ID

data PRIM = PrimBr OPTNM [PRIM]   | PrimSeln OPTNM [PRIM]
          | PrimIter OPTNM [PRIM] | PrimSeq OPTNM [PRIM]
          | PrimTask OPTNM [PRIM] | PrimAct ID OPTYP [SPEC]
\end{verbatim}

We consider \texttt{PrimSeq} and \texttt{PrimTask} as the same.

Simplified Types:
\begin{code}
data OpType = Manual | Executable
data PMLPrim = Branch    (Maybe String) [PMLPrim]
             | Selection (Maybe String) [PMLPrim]
             | Iteration (Maybe String) PMLPrim
             | Sequence  (Maybe String) [PMLPrim]
             | Action String PMLAction

mkPrim comp mstr [prim] = prim
mkPrim comp mstr prims  = comp mstr prims
\end{code}

\newpage
Display:
\begin{code}
instance Show PMLPrim where
 show (Branch    mstr prims) = showComp "branch" mstr prims
 show (Selection mstr prims) = showComp "selection" mstr prims
 show (Iteration mstr prim) = showComp "iteration" mstr [prim]
 show (Sequence  mstr prims) = showComp "sequence" mstr prims
 show (Action nm action)        = showAction nm action

showComp key mstr prims
 = unlines' (
     (key ++ ' ':mshow mstr ++ " {")
   : (map indshow prims)
   ++ ["}"]
   )
 where
   mshow Nothing    = ""
   mshow (Just str) = str
\end{code}

\newpage
Conversion:
\begin{code}
transOPTNM :: OPTNM -> Maybe String
transOPTNM OpNmNull          = Nothing
transOPTNM (OpNmId (ID str)) = Just str

transPRIM :: PRIM -> PMLPrim
transPRIM (PrimBr optnm prims)   = primBuild Branch    optnm prims
transPRIM (PrimSeln optnm prims) = primBuild Selection optnm prims
transPRIM (PrimIter optnm prims) = primBuild iteration optnm prims
 where
   iteration mstr [prim] = Iteration mstr prim
   iteration mstr prims  = Iteration mstr $ Sequence Nothing prims
transPRIM (PrimSeq optnm prims)  = primBuild Sequence  optnm prims
transPRIM (PrimTask optnm prims) = primBuild Sequence  optnm prims
transPRIM (PrimAct (ID str) optyp specs) = Action str $ transACT optyp specs

primBuild :: (Maybe String -> [PMLPrim] -> PMLPrim)
          -> OPTNM -> [PRIM] -> PMLPrim
primBuild prim optnm prims
 = mkPrim prim (transOPTNM optnm) (map transPRIM prims)
\end{code}

\newpage
\subsection{PML Process}

BNFC Types:
\begin{verbatim}
data PROCESS = Process ID [PRIM]
\end{verbatim}

If we have a list of primitives at the top level,
these are interpreted as a sequence.
We prefer to indicate this explicitly.

Simplified Type:
\begin{code}
data PML = PML String PMLPrim
\end{code}

Display:
\begin{code}
instance Show PML where
  show (PML nm prim) = showComp "process" (Just nm) [prim]
\end{code}

Conversion:
\begin{code}
transPROCESS :: PROCESS -> PML
transPROCESS (Process (ID nm) [prim]) = PML nm $ transPRIM prim
transPROCESS (Process (ID nm) prims) = PML nm $ Sequence Nothing $ map transPRIM prims
\end{code}
