\section{Standard PML Resources}
\begin{code}
module PMLResources where
import qualified Data.Map.Strict as M
import ResHandling
import SynPML
import SynResources
import SynAction
\end{code}

\begin{code}
instance GetResources PML where
  listResources = listPMLResources
  listActions = listPMLActions
\end{code}

\subsection{Listing PML Resources}

\begin{code}
listPMLResources :: PML -> [ResExpr]
listPMLResources (PML _ prim) =  listPrimResources prim

listPrimResources :: PMLPrim -> [ResExpr]
listPrimResources (Branch _ prims)      = concat $ map listPrimResources prims
listPrimResources (Selection _ prims)   = concat $ map listPrimResources prims
listPrimResources (Iteration _ prim)    = listPrimResources prim
listPrimResources (Sequence _ prims)    = concat $ map listPrimResources prims
listPrimResources (Action _ (_, specs)) = specResources specs
\end{code}

\subsection{Listing PML Actions}

\begin{code}
listPMLActions :: PML -> [(String,PMLAction)]
listPMLActions (PML _ prim) =  listPrimActions prim

listPrimActions :: PMLPrim -> [(String,PMLAction)]
listPrimActions (Branch _ prims)      = concat $ map listPrimActions prims
listPrimActions (Selection _ prims)   = concat $ map listPrimActions prims
listPrimActions (Iteration _ prim)    = listPrimActions prim
listPrimActions (Sequence _ prims)    = concat $ map listPrimActions prims
listPrimActions (Action nm action)    = [(nm,action)]
\end{code}
