\section{Labelled Actions Systems Main}

\begin{code}
module Main where
import APTypes
import APUtils
import APFuns
import APEval
import LASTypes
import LASFuns
import LASExec

import LexPML
import AbsPML
import ParPML

import qualified Data.Map as M
\end{code}

\subsection{Mainline}

\begin{code}
main = do putStrLn "\nExample Program Action System (PAS)"
          putStrLn $ show pasMain
          let lasMain = pas2asys pasMain
          putStrLn "Converted to Labelled Action System (LAS)"
          putStrLn $ show lasMain
          lasStart lasMain
\end{code}


\subsection{Sample Program Action Systems}

\begin{code}
pasMain
   = PAS
      -- initialisers  VAR v1 := e1, v2 := e2, ...
      [(v1,Num 42),(v2,Txt "me")]
      -- initial labels
      (LS [i0,i1])
      -- final labels
      (LS [fz])
      -- disjunction of actions
      [ PA (LS [i0]) incV1 (LS [i1])
      , CC (LS [i1]) (LS [fz]) v1gt3 (LS [i0])
      ]
   where
     v1 = V "v1"
     v2 = V "v2"
     i0 = L "i0"
     i1 = L "i1"
     fy = L "fy"
     fz = L "fz"
     incV1 = pAsg v1 (plus (Var v1) (Num 1))
     v1gt3 = lt (Num 3) (Var v1)
\end{code}
