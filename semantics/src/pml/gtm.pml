process gtm {
  sequence SG_1_Define_Global_Project_Management {
    sequence SP_1_1_Global_Task_Management {
      sequence Determine_team_and_organisational_structure_between_locations {
	action create_roles {
	  script { "<p>Create roles, relationships and rules to
                facilitate coordination and control over
                geographical, temporal and cultural distance.</p>" }
	}
	action structure_global_team {
	  script { "<p>Structure global team and monitor operation to
                minimise fear and alienation in teams: Be aware of
                problems with unbalanced team sizes; e.g., smaller
                teams may be threatened and fear job loss.</p><p>Team structure should cater for possibility of
                dual reporting to management at more than one
                location, e.g team structure could be cross
                divisional or multi-organisational and management
                remote.</p><p>Ensure that the supervision, support and
                information needs of all team members are met
                regardless of location. Organisational structure
                should be documented and available to all team to
                allow a clear understanding of everyone's roles and
                responsibilities within the project</p>" }
	}
      }
    }
    sequence SP_1_2_Knowledge_and_Skills {
      sequence Identify_business_competencies_required_by_global_team_members_in_each_location {
	action document_customer_base {
	  script { "Document and define customer base and
              functions relative to the application being
              developed." }
	}
	action provide_training {
	  script { "Provide training to ensure that global team
              has required understanding of the customer base and
              the business functions to take full advantage of the
              proximity of the team to the customer base." }
	}
      }
      sequence Identify_the_cultural_requirements_of_each_local_subteam {
	action cultural_diversity {
	  script { "Each team member should be trained to
              understand the culture of the virtual team.
              Face-to-face meetings are recommended when and where
              possible, ideally at the start of the project and/or
              when a new member joins. Having individuals visit
              locations for extended periods can also be a
              successful strategy and should be fully leveraged at
              every possible opportunity." }
	}
      }
      sequence Identify_Communication_Skills_for_GSE {
	action establish_comm_prototol {
	  script { "In order to develop the right practice, a new
              communication protocol needs to be set up. Policies
              should be put in place to support these new
              requirements to the satisfaction of all virtual team
              members. For example in synchronous communication,
              ensure that link up times are shared between core
              team working hours in each location." }
	}
	sequence Establish_relevant_criteria_for_training_teams {
	  action Effective_knowledge_transfer {
	    script { "Carry out evaluation of training needs to
                include cultural and linguistic issues. Undertake
                training onsite and face-to-face so team members
                can be directly assessed and training provision
                tailored to their specific requirements." }
	  }
	}
      }
    }
    sequence SP_1_3_Global_Project_Management {
      sequence Identify_GSE_project_management_tasks {
	action define_team_ability {
	  script { "Define ability and potential productivity of
              team: Global project manager should allocate tasks
              and timescales that are realistic. Where possible,
              the project manager should be actively involved in
              the recruitment and selection of team members.
              Failing this, they should gather all information
              relating to the technical and professional experience
              of potential and existing team members. When teams
              are in place and project details reported project
              managers should understand and document how
              individuals contribute to that project along with
              their skills and knowledge." }
	}
      }
      selection Assign_tasks_to_appropriate_team_members {
	action define_modularization {
	  script { "Modularisation: partition work into modules
              which have a well defined functional whole" }
	}
	action define_phased_approach {
	  script { "Phase-based approach: Use when phases of the
              development cycle are relatively independent. Ensure
              that the team members developing a specific phase
              have a good understanding of what is required at each
              specific stage." }
	}
	action define_integrated_approach {
	  script { "Integrated approach: Set up a protocol to
              allow handover from one geographic location to
              another to ensure a successful follow the sun
              development." }
	}
      }
      sequence Ensure_Awareness_of_cultural_profiles {
	action identify_cultural_differences {
	  script { "National cultural differences should be
              identified and communicated to the management and
              team members." }
	}
	action communicate_cultural_training {
	  script { "<p>Cultural training can be communicated in
                following way:</p><p>Provide training to give all team members an
                opportunity to learn and understand about each
                other's culture.</p><p>Address national, religious and relevant ethnic
                issues, all team members should understand
                acceptable and unacceptable forms of behaviour.</p><p>Training should be tailored to team member's
                specific needs and location.</p>" }
	}
	action establish_cultural_profiles {
	  script { "Project managers should ensure that cultural
              profiles for teams are established. E.g., Management
              and staff should show respect for gender-related
              cultural values of all colleagues. All employees'
              legal rights must be upheld." }
	}
      }
      sequence Establish_cooperation_and_coordination_procedures_between_locations {
	action create_infrastrucure {
	  script { "Ensure that a suitable infrastructure,
              process and management procedures are in place to
              help establish cooperation and coordination between
              locations. Achievable milestones should be planned
              and agreed. Projects should be monitored with
              reference to costs, time, productivity, quality and
              risk." }
	}
      }
      sequence Establish_reporting_procedures_between_locations {
	action submit_formal_report {
	  script { "Regular formal reporting will help the
              project manager to remain aware of how project is
              progressing. Procedure should include and encourage
              team members to report whether or not they can take
              on that task in the given time and report any
              problems before it is too late." }
	}
      }
      sequence Establish_a_Risk_Management_Strategy {
	action identify_risks {
	  script { "All potential risks should be identified and
              addressed to include: risks in misunderstanding
              cultural differences, misunderstanding requirements,
              feature volatility, schedules, budgets, personnel. In
              addition, risk associated with outsourcing activities
              to politically unstable locations needs to be
              identified." }
	}
      }
    }
  }
  sequence SG_2_Define_Management_Between_Locations {
    sequence SP_2_1_Operating_Procedures {
      sequence Define_how_conflicts_and_differences_of_opinion_between_locations_are_addressed_and_resolved {
	action implement_conflict_strategy {
	  script { "Set up a strategy to handle, monitor and
              anticipate where conflict between remote locations
              may occur. The strategy should include how conflict
              will be resolved and how a person responsible for
              that resolution is selected." }
	}
	action account_for_conflict {
	  script { "When defining the global strategy for dealing
              with conflict, different types of conflict have to be
              taken into account, for example conflict due to fear
              as well as cultural differences." }
	}
      }
      sequence Implement_a_communication_strategy_for_the_team {
	action plan_interteam_communication {
	  script { "Plan, facilitate, encourage and monitor
              communication between teams." }
	}
	action provide_communication_training {
	  script { "Provide training on how best to communicate
              with remote colleagues, including the effective
              operation of communication tools and
              procedures." }
	}
	action consider_linguistic_distance {
	  script { "Consider linguistic and cultural implications
              inherent when communicating remotely." }
	}
      }
      sequence Establish_communication_interface_points_between_the_team_members {
	action encourage_reporting {
	  script { "Strategies need to be put in place which
              encourages both formal and informal
              reporting." }
	}
	action communicate_deadlines {
	  script { "Ensure that relevant team members are made
              aware of how and when they will receive inputs to
              products, needs to distribute outputs from and when
              complete work products are required." }
	}
	action communicate_constraints {
	  script { "Ensure teams are aware of potential
              constraints such as legal restrictions and holidays
              in countries within which they are developing the
              product." }
	}
	action distribute_team_member_info {
	  script { "Ensure that Information about each team
              member is easily accessible by colleagues.
              Information of an individual's role within the team
              and their specific areas of responsibility should be
              combined with a photograph, their first name,
              surname, friendly name (if appropriate) and their
              preferred form of address." }
	}
	action use_wikis {
	  script { "Intranets and wikis can be invaluable for
              this form of communication." }
	}
      }
      sequence Implement_strategy_for_conducting_meetings_between_locations {
	action identify_mtg_technology {
	  script { "Identify appropriate virtual meeting
              technology is used" }
	}
	action encourage_participation {
	  script { "Try to ensure all participants are
              comfortable with virtual meeting and are given
              opportunity to agree or disagree with points raised,
              and offer new ideas." }
	}
	action circualte_mtg_agenda {
	  script { "Circulate agenda prior to meeting, and
              clearly minute actions agreed a meeting" }
	}
	action circulate_minutes_promptly {
	  script { "Ensure that no delay occurs between the
              meeting and the circulation of minutes as people may
              be waiting for the minutes before implementing the
              actions." }
	}
      }
      sequence SP_2_2_Collaboration_between_locations {
	sequence Identify_common_goals_objectives_and_rewards_for_the_global_team {
	  action set_project_goals {
	    script { "Global Project manager sets project goals
                and objectives." }
	  }
	  action create_common_goals {
	    script { "Goals at project level are common to all
                locations." }
	  }
	  action communicate_goals {
	    script { "Project goals and objectives communicated,
                understood and agreed across all team members
                regardless of location." }
	  }
	  action judge_whole_team {
	    script { "The global team is viewed as an entity in
                its own right, regardless of the location of its
                team members and its performance should be judged
                and rewarded accordingly." }
	  }
	  action acknowledge_team_success {
	    script { "Acknowledging team success may require
                tailoring rewards to the needs of different
                cultures." }
	  }
	  action understand_cultural_motivation {
	    script { "Project Managers need to understand the
                cultural motivation of the different team members
                and identify and apply appropriate rewards in each
                situation when and where relevant." }
	  }
	  action consider_cultural_issues {
	    script { "Consideration should be given to cultural
                issues, economic situation and income tax laws when
                planning rewards." }
	  }
	}
	sequence Collaboratively_establish_and_maintain_work_product_ownership_boundaries {
	  action define_product_ownership {
	    script { "Define product ownership boundaries through
                partitioning of work across GSE teams." }
	  }
	  action understand_role {
	    script { "Each location should understand their role
                within the life cycle of the product" }
	  }
	  action understand_change_impact {
	    script { "Each location should understand how their
                modifications to the product unit can affect the
                other locations." }
	  }
	}
	sequence Collaboratively_establish_and_maintain_interfaces_and_processes {
	  action define_common_goals {
	    script { "Define common process goals across all
                locations." }
	  }
	  action define_process_ownership {
	    script { "Define process ownership - placing
                ownership with those closest to process where
                possible." }
	  }
	  action encourage_input {
	    script { "Seek and encourage input from team members
                at all locations" }
	  }
	  action communicate_value {
	    script { "Let team members know their input to
                process development and ownership is
                valued." }
	  }
	  action address_gse_challenges {
	    script { "Processes should address specific
                challenges associated with GSE." }
	  }
	  action include_all_sites_in_process {
	    script { "Processes should take into account the
                relevant structures and procedures from all
                sites." }
	  }
	}
	sequence Collaboratively_develop_communicate_and_distribute_work_plans {
	  action establish_achievable_milestones {
	    script { "Achievable milestones should be planned and
                agreed" }
	  }
	  action define_communication_plans {
	    script { "Within the commitments made, team members
                must explicitly include communication plans to
                include use synchronous and asynchronous
                communication tools" }
	  }
	  action create_contingency_plans {
	    script { "Contingency plans should be in place to
                address potential risks" }
	  }
	  action establish_contingency_procedures {
	    script { "Establish procedures to coordinate
                implementation of contingencies when and if
                required." }
	  }
	}
      }
    }
  }
}
