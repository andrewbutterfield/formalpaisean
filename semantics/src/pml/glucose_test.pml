process glucose_test {
  action retrieve_record {
    requires { blood_sample }
    provides { test_record }
    agent { technician }
  }
  action extract_test_sample {
    requires { blood_sample }
    provides { test_sample }
    agent { technician }
  }
  action test_sample {
    requires { test_sample }
    provides { test_results }
    agent { technician }
  }
  action log_results {
    requires { test_results && test_record}
    provides { test_record.results }
    agent { technician }
  }
  action notify_requester {
    requires { test_record.results }
    provides { test_record.notified }
    agent { clerk }

  }
}
