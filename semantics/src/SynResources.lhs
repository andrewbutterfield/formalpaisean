\section{Resource Abstract Syntax}
\begin{code}
module SynResources where
import Data.List

import Utilities

import AbsPML
\end{code}

Common to all our semantic theories is the key concept
of \emph{resources},
as characterised by predicates that they satisfy.
Here we map the BNFC abstract syntax
to a simplified internal one,
starting with the ``leaf'' type and working up to full resource expressions.

\newpage
\subsection{Resource Variable Expressions}

BNFC type:
\begin{verbatim}
data VAREXPR  =  VarId ID | VarPar ID | VarMore ID VAREXPR
\end{verbatim}
\begin{code}
data ResVar
 = ResId String           -- VarId
 | ResPar String          -- VarPar
 | ResMore String ResVar  -- VarMore
 deriving(Eq, Ord)
\end{code}
Display:
\begin{code}
instance Show ResVar where
  show (ResId str)       =  str
  show (ResPar str)      =  '(':str++")"
  show (ResMore str rv)  =  '(':str++") "++show rv
\end{code}
Conversion:
\begin{code}
varExpr2Res :: VAREXPR -> ResVar
varExpr2Res (VarId (ID str)) = ResId str
varExpr2Res (VarPar (ID str)) = ResPar str
varExpr2Res (VarMore (ID str) varexpr)
 = ResMore str $ varExpr2Res varexpr
\end{code}

\newpage
\subsection{Resource Value/Attribute Expressions}

BNFC types:
\begin{verbatim}
data ATTREXPR =  Attr VAREXPR ID
data VALEXPR  =  ValAttr ATTREXPR | ValString STRING | ValNum NUMBER
\end{verbatim}
Simplified type:
\begin{code}
data ResVal
 = RVNum Double     -- ValNum
 | RVStr String     -- ValString
 | RVAttr ResVar String  -- ValAttr (Attr)
 deriving(Eq, Ord)
\end{code}
Display:
\begin{code}
instance Show ResVal where
  show (RVNum dbl)  =  show dbl
  show (RVStr str)  =  str
  show (RVAttr rv str) = show rv ++ '.':str
\end{code}
Conversion:
\begin{code}
attrExpr2Res :: ATTREXPR -> ResVal
attrExpr2Res (Attr vexpr (ID str)) = RVAttr (varExpr2Res vexpr) str

valExpr2Res :: VALEXPR -> ResVal
valExpr2Res (ValNum (NUMBER number)) = RVNum $ read number
valExpr2Res (ValString (STRING string)) = RVStr string
valExpr2Res (ValAttr aexpr) = attrExpr2Res aexpr
\end{code}

\newpage
\subsection{Resource Expressions}

BNFC Type:
\begin{verbatim}
data EXPR =
   DisjExpr EXPR EXPR | ConjExpr EXPR EXPR | Str STRING
 | RelEq VALEXPR VALEXPR | RelNe VALEXPR VALEXPR | RelLt VALEXPR VALEXPR
 | RelGt VALEXPR VALEXPR | RelLe VALEXPR VALEXPR | RelGe VALEXPR VALEXPR
 | RelVeq VAREXPR VAREXPR | RelVne VAREXPR VAREXPR
 | PrimVar VAREXPR | PrimAttr ATTREXPR | PrimNot EXPR
\end{verbatim}
Simplified Type:
\begin{code}
data ResExpr
 = RStr String              -- Str
 | RVar ResVar              -- PrimVar
 | RAttr ResVar String      -- PrimAttr (Attr)
 | RExpApp String [ResExpr] -- PrimNot, DisjExpr, ConjExpr
 | RVarApp String [ResVar]  -- RelVeq, RelVne
 | RValApp String [ResVal]  -- RelEq, ... , RelGe
 deriving(Eq, Ord)
\end{code}
Display:
\begin{code}
instance Show ResExpr where
  show (RStr str)  =  str
  show (RVar rv)   =  show rv
  show (RAttr rv str) = show rv ++ '.':str
  show (RExpApp str res)  =  showApp str res
  show (RVarApp str rvs)  =  showApp str rvs
  show (RValApp str rvs)  =  showApp str rvs

showApp op [x,y] = '(':show x ++ ' ':op ++ ' ':show y++")"
showApp op xs
  = op ++ '(':concat (intersperse "," (map show xs))++")"
\end{code}
\newpage
Conversion:
\begin{code}
expr2Res :: EXPR -> ResExpr

expr2Res (Str (STRING str))  =  RStr str
expr2Res (PrimVar var)       =  RVar $ varExpr2Res var
expr2Res (PrimAttr (Attr vexpr (ID str)))   =  RAttr (varExpr2Res vexpr) str
expr2Res (PrimNot e)         =  RExpApp "!" [expr2Res e]

expr2Res (DisjExpr e1 e2)
  =  RExpApp "||" [expr2Res e1, expr2Res e2]
expr2Res (ConjExpr e1 e2)
  =  RExpApp "&&" [expr2Res e1, expr2Res e2]
expr2Res (RelEq val1 val2)
  =  RValApp "==" [valExpr2Res val1, valExpr2Res val2]
expr2Res (RelNe val1 val2)
  =  RValApp "!=" [valExpr2Res val1, valExpr2Res val2]
expr2Res (RelLt val1 val2)
  =  RValApp "<" [valExpr2Res val1, valExpr2Res val2]
expr2Res (RelGt val1 val2)
  =  RValApp ">" [valExpr2Res val1, valExpr2Res val2]
expr2Res (RelLe val1 val2)
  =  RValApp "<=" [valExpr2Res val1, valExpr2Res val2]
expr2Res (RelGe val1 val2)
  =  RValApp ">=" [valExpr2Res val1, valExpr2Res val2]
expr2Res (RelVeq var1 var2)
  =  RVarApp "==" [varExpr2Res var1, varExpr2Res var2]
expr2Res (RelVne var1 var2)
  =  RVarApp "!=" [varExpr2Res var1, varExpr2Res var2]
\end{code}
