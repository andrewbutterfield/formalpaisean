\section{Resource Handling}
\begin{code}
module ResHandling where
import Data.Maybe
import Data.List
import qualified Data.Map.Strict as M
import Utilities
import SynResources
import SynAction
\end{code}

\subsection{Introduction}

This module provides way to access resources in a PML description.
This can range from simple listing to various graphs over resources.
We also need to support a means of identifying resources that can both
distinguish different resources and track a single resource as it evolves.
We also note that the resource expression can refer to many resources,
and also to ``naked'' values.
In effect we need a way to type a resource expression.

\subsection{Resource Attributes}

Given a resource expression we want to extract all the resource attributes,
with their associated resource.

\begin{code}
type RAMap = M.Map String [String]

ramConcat = foldl (M.unionWith (++)) M.empty

rVarAttrs :: Maybe String -> ResVar -> RAMap
rVarAttrs mattr (ResId str) = M.singleton str $ maybeToList mattr
rVarAttrs mattr (ResMore _ rv) = rVarAttrs mattr rv
rVarAttrs _ _                  = M.empty

rValAttrs :: ResVal -> RAMap
rValAttrs (RVAttr rv attr)  =  rVarAttrs (Just attr) rv
rValAttrs _                =  M.empty

rExprAttrs :: ResExpr -> RAMap
rExprAttrs (RVar rv)        =  rVarAttrs Nothing rv
rExprAttrs (RAttr rv attr)  =  rVarAttrs (Just attr) rv
rExprAttrs (RExpApp _ res)  =  ramConcat $ map rExprAttrs res
rExprAttrs (RVarApp _ rvs)  =  ramConcat $ map (rVarAttrs Nothing) rvs
rExprAttrs (RValApp _ rvs)  =  ramConcat $ map rValAttrs rvs
rExprAttrs _                =  M.empty

rExprNames :: ResExpr -> [String]
rExprNames = M.keys . rExprAttrs

rExprsNames :: [ResExpr] -> [String]
rExprsNames = concat . map rExprNames
\end{code}


\subsection{Resource Expression Typing}

We have boolean expressions, numeric relations, resource names,
resource attributes, modifiers and values.
We want to associate types with resources (their known attributes)
and with attributes.
Resource types are obtained by using \texttt{rExprAttrs} as described above.
We introduce types for attributes, and map resource/attribute pairs to those
types.
\begin{code}
data RType
 = RTarb  -- arbitrary, as yet undetermined type
 | RTnum  -- Numeric type
 | RTstr  -- String type
 | RTname -- Name (Identifier) Type
 | RTbool -- Boolean type, for R...App
 | RTerr String [RType]  -- type errors, caused by type conflicts
 deriving (Eq, Ord)
 
instance Show RType where
  show RTarb   =  "?"
  show RTnum   =  "Numeric"
  show RTstr   =  "String"
  show RTname  =  "Name"
  show RTbool  =  "Boolean"
  show (RTerr msg rts)  =  msg ++ '(':showCause rts++")"

showCause = concat . intersperse "," . map show
\end{code}
We can fuse types together, generating errors if they don't match
\begin{code}
rtFuse :: RType -> RType -> RType
rtFuse RTarb rt       =  rt
rtFuse rt RTarb       =  rt
rtFuse RTnum RTnum    =  RTnum
rtFuse RTstr RTstr    =  RTstr
rtFuse RTname RTname  =  RTname
-- names can act as booleans, asserting their presence
rtFuse RTname RTbool  =  RTbool
rtFuse RTbool RTname  =  RTbool
rtFuse RTbool RTbool  =  RTbool
rtFuse rt1 rt2        =  RTerr "CannotFuse" [rt1,rt2]

isRTErr (RTerr _ _) = True
isRTerr _           = False
\end{code}

We record resource attribute types as a map from resource/attribute pairs
to types:
\begin{code}
type RATMap = M.Map (String,String) RType

insRAT res attr rt = M.insertWith rtFuse (res, attr) rt

mrgRAT :: RATMap -> RATMap -> RATMap
mrgRAT = M.unionWith rtFuse
\end{code}

We type-check a resources expression,
returning the type of the expression as a whole
and the determined resource attribute types:
\begin{code}
typeCheckResVar :: Maybe String -> ResVar -> (RType, RATMap)
typeCheckResVar Nothing     (ResId str) = (RTname, M.empty)
typeCheckResVar (Just attr) (ResId str) = (RTarb, M.singleton (str, attr) RTarb)
typeCheckResVar mattr (ResMore _ rv)    = typeCheckResVar mattr rv
typeCheckResVar _ _                     = (RTarb, M.empty)

typeCheckResVal :: ResVal -> (RType, RATMap)
typeCheckResVal (RVNum _)    = (RTnum, M.empty)
typeCheckResVal (RVStr _)    = (RTstr, M.empty)
typeCheckResVal (RVAttr rv attr) = typeCheckResVar (Just attr) rv

typeCheckResExpr :: ResExpr -> (RType, RATMap)
typeCheckResExpr (RStr _) = (RTstr, M.empty)
typeCheckResExpr (RVar rv) = typeCheckResVar Nothing rv
typeCheckResExpr (RAttr rv attr) = typeCheckResVar (Just attr) rv
typeCheckResExpr (RExpApp _ res) = typeCheckSameExprs (RTbool,M.empty) res
typeCheckResExpr (RVarApp _ rvs) = (RTbool, M.empty)
typeCheckResExpr (RValApp _ rvs)
 = let (rt', rtmap') = typeCheckSameVals (RTarb,M.empty) rvs
   in case rt' of
     (RTerr _ _)  ->  (rt', rtmap')
     _            ->  (RTbool, updateRTypes rt' rtmap' rvs)

typeCheckResExprs :: [ResExpr] -> ([(ResExpr,RType)], RATMap)
typeCheckResExprs res
 = let (rtyps,ratmaps) = unzip $ map typeCheckResExpr res
   in (zip res rtyps, foldl mrgRAT M.empty ratmaps)
\end{code}

Helper code:
\begin{code}
typeCheckSameExprs :: (RType, RATMap) -> [ResExpr] -> (RType, RATMap)
typeCheckSameExprs (rt, ratm) [] = (rt, ratm)
typeCheckSameExprs (rt, ratm) (re:res)
 = let (rt', ratm') = typeCheckResExpr re
   in typeCheckSameExprs (rt `rtFuse` rt', ratm `mrgRAT` ratm') res

typeCheckSameVals :: (RType, RATMap) -> [ResVal] -> (RType, RATMap)
typeCheckSameVals (rt, ratm) [] = (rt, ratm)
typeCheckSameVals (rt, ratm) (rv:rvs)
 = let (rt', ratm') = typeCheckResVal rv
   in typeCheckSameVals (rt `rtFuse` rt', ratm `mrgRAT` ratm') rvs

updateRTypes :: RType ->  RATMap -> [ResVal] -> RATMap
updateRTypes rt rtmap [] = rtmap
updateRTypes rt rtmap (rv:rvs) = updateRTypes rt (updateRType rt rtmap rv) rvs

updateRType :: RType ->  RATMap -> ResVal -> RATMap
updateRType rt rtmap (RVAttr rv attr)
 = upd rv
 where
   upd (ResId r)       =  insRAT r attr rt rtmap
   upd (ResMore _ rv)  =  upd rv
   upd _               =  rtmap
updateRType rt rtmap _ =  rtmap
\end{code}


\subsection{Resource Graphs}

We can define two kinds of dependencies between resources:
\begin{description}
  \item[Data Flow]
     An action with requires and provides resources induces a ``depends-on'' relation
     between each provided resource and all the required resources.
     This relation can be extended as well to attributes.
  \item[Control Flow]
     The control constructs define a sequencing of actions,
     creating a (temporal) dependency between the provided resources of any given action
     and the required resources of any subsequent action.
\end{description}

\subsubsection{Graph Datatype}

\begin{code}
newtype ResGraph = RG (M.Map String [String])

instance Show ResGraph where
  show (RG rgmap)
   = unlines' $ map showNode $ M.toList rgmap
   where
     showNode (src,tgts) = src ++ " |-> " ++ show tgts
     
resEdge :: (String, String) -> ResGraph
resEdge (r1, r2) = RG $ M.singleton r1 [r2]

mrgRG :: ResGraph -> ResGraph -> ResGraph
(RG rg1) `mrgRG` (RG rg2) = RG $ M.unionWith rgFuse rg1 rg2

rgFuse :: Ord a => [a] -> [a] ->[a]
xs `rgFuse` ys = nub $ sort (xs++ys)

resEdges :: [(String, String)] -> ResGraph
resEdges = foldr mrgRG (RG M.empty) . map resEdge
\end{code}

\subsubsection{Data Flow Dependencies}

Given an action we can derive a partial sub-graph:
\begin{code}
actionGraph :: PMLAction -> ResGraph
actionGraph (_, specs)
  = resEdges $ bldedges [] 
                   (rExprsNames $ provides specs) (rExprsNames $ requires specs)
  where
    bldedges es []     _  = es
    bldedges es (p:ps) rs = bldedges (bldedges' es p rs) ps rs
    bldedges' es p []     = es
    bldedges' es p (r:rs) = bldedges' ((p,r):es) p rs
    
actionsGraph :: [PMLAction] -> ResGraph
actionsGraph = foldr mrgRG (RG M.empty) . map actionGraph
\end{code}

\subsubsection{Control Flow Dependencies}


\newpage
\subsection{Resource Handling Class}

We define a class that supports the following operations:
\begin{code}
class GetResources pml where
\end{code}
\begin{itemize}
  \item listing all the resources
\begin{code}
  listResources :: pml -> [ResExpr]
\end{code}
  \item type checking the resources (with an obvious default implementation)
\begin{code}
  typeCheck :: pml -> ([(ResExpr, RType)], RATMap)
  typeCheck = typeCheckResExprs . listResources
\end{code}
  \item listing all the basic actions.
\begin{code}
  listActions :: pml -> [(String,PMLAction)]
\end{code}
  \item obtaining the resource data-flow graph
     induced by task \texttt{require} and \texttt{provide} clauses
     (once more, with an obvious default implementations).
\begin{code}
  dataFlow :: pml -> ResGraph
  dataFlow = actionsGraph . map snd . listActions
\end{code}
  \item obtaining the resource data-flow graph
     induced by the PML control flow.
\end{itemize}


