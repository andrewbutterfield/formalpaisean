\section{Alphabetised Predicate Types}
\begin{code}
module APTypes where
import Data.Char
import Utilities
\end{code}

\subsection{Alphabetised Predicates}

We shall define a simple alphabetised predicate language.
In this module we define datatypes and show instances only.

\subsubsection{Variables}

First, variables.
\begin{eqnarray*}
   v \in  Var &&
\end{eqnarray*}
\begin{code}
data Var = V {outV :: String} deriving (Eq,Ord)

instance Show Var where show (V s) = s
\end{code}

\newpage
\subsubsection{Alphabets}

An alphabet is a set of variables:
\begin{eqnarray*}
   \alpha \in Alf &=& \power Var
\end{eqnarray*}
\begin{code}
data Alf = A { outA :: [Var] } deriving (Eq,Ord)

instance Show Alf where show (A vs) = sshow vs
\end{code}

\subsubsection{Expressions}

We now define a simple expression language, including sets,
 over integers, names%
 \footnote{
 A name is a value that denotes itself. We shall use them here to represent
 the semantics of labels as introduced later.
 }%
 , and variables.
\begin{eqnarray*}
   n \in \Int
\\ e,b \in Expr
   & ::= &  n
\\ & \alt & v
\\ & \alt & f(e,\ldots,e)
\end{eqnarray*}
\begin{code}
data Expr
  = Num Integer
  | Txt String
  | Var Var
  | App String [Expr]
  | Set [Expr]
  deriving (Eq,Ord)

instance Show Expr where
  show (Num i)  =  show i
  show (Txt s)  =  s
  show (Var (V v)) = v
  show (App n [e1,e2])
   | all isSymbol n = "("++show e1++" "++n++" "++show e2++")"
  show (App n es) = n ++ "(" ++ lshow "," es ++ ")"
  show (Set es) = sshow es
\end{code}

\newpage
\subsubsection{Predicates}

We then define predicates over expressions as:
\begin{eqnarray*}
   P \in Pred
   & ::=  & \textbf{true}
\\ & \alt & e
\\ & \alt & \lnot P
\\ & \alt & \bigvee(P,\ldots,P)
\\ & \alt & \exists v,\ldots,v @ P
\\ & \alt & P[e_1,\ldots,e_n/v_1,\ldots,v_n]
\\ & \alt & X
\\ & \alt & \mu X @ P
\\ & \alt & \mbox{ways to define new syntactic sugar}
\end{eqnarray*}
\begin{code}
data Pred =
 -- primitives
   TRUE
 | Atm Expr
 | Not Pred
 | Or [Pred]
 | EX [Var] Pred
 -- explicit substitution
 | Sub Pred [(Var,Expr)]
 -- recursion support
 | PH String
 | Rec String Pred
 -- derived
 | K String  -- keyword
 | DRV String [Pred] String -- delimiters and pred/keyword list
 deriving (Eq,Ord)

instance Show Pred where
  show TRUE = "TRUE"
  show (Atm e) = show e
  show (Not TRUE) = "~TRUE"
  show (Not pr) = "~(" ++ show pr ++ ")"
  show (Or prs) = "\\/(" ++ lshow "," prs ++ ")"
  show (EX vs pr)
            = "(EX " ++ lshow "," vs ++ " @ " ++ show pr ++ ")"

  show (Sub pr ves) = show pr ++ "[" ++ showSubs ves ++ "]"
  show (PH x) = x
  show (Rec x pr) = "mu "++x++" @ "++show pr

  show (K s) = s
  show (DRV left prs right) = left ++ showDerive prs ++ right

showSubs [] = ""
showSubs ves
 = lshow "," es ++ "/" ++ lshow "," vs
 where (vs,es) = unzip ves

showDerive [] = ""
showDerive [pr] = show pr
showDerive (K s:prs) = s ++ ' ':showDerive prs
showDerive (pr:K s:prs) = show pr ++ ' ':s ++ ' ':showDerive prs
showDerive (pr:prs) = show pr ++ ',':showDerive prs
\end{code}

\newpage
\subsubsection{Alphabetised Predicates}

\[
  (\alf P, P)
\]
\begin{code}
data APred
 = AP { apAlf :: Alf
      , apPred :: Pred
      } deriving (Eq,Ord,Show)
\end{code}
