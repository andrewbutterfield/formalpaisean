run((A(XA) || A(XB)) ;; (A(XC) || A(XD)))

 = "defn. of run.3"

   ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "defn. of ;;"

   ((A(XA) || A(XB))[g:1,lg/g,out] \/ (A(XC) || A(XD))[g:2,lg/g,in])
   [g::,lg,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "simplify"

       (A(XA) || A(XB))[g:::1,lg,lg::,lg/g,in,out,ps]
    \/ (A(XC) || A(XD))[g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "defn. of ||"

       (   ps(in) /\ s' = s /\ ps' = ps (-/+) (in,{lg1,lg2})
        \/ A(XA)[g1::,lg1,lg1:/g,in,out]
        \/ A(XB)[g2::,lg2,lg2:/g,in,out]
        \/ ps(lg1:,lg2:) /\ s' = s /\ ps' = ps (-/+) ({lg1:,lg2:},out))
       [g:::1,lg,lg::,lg/g,in,out,ps]
    \/ (A(XC) || A(XD))[g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "simplify"

       lg(lg) /\ s' = s /\ ps' = lg (-/+) (lg,{lg:::11,lg:::12})
    \/ A(XA)[g:::11::,lg:::11,lg:::11:,lg/g,in,out,ps]
    \/ A(XB)[g:::12::,lg:::12,lg:::12:,lg/g,in,out,ps]
    \/     lg(lg:::11:,lg:::12:)
        /\ s' = s
        /\ ps' = lg (-/+) ({lg:::11:,lg:::12:},lg::)
    \/ (A(XC) || A(XD))[g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "simplify"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/ A(XA)[g:::11::,lg:::11,lg:::11:,lg/g,in,out,ps]
    \/ A(XB)[g:::12::,lg:::12,lg:::12:,lg/g,in,out,ps]
    \/ (A(XC) || A(XD))[g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "defn. of A"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/ (ps(in) /\ XA /\ ps' = ps (-/+) (in,out))
       [g:::11::,lg:::11,lg:::11:,lg/g,in,out,ps]
    \/ A(XB)[g:::12::,lg:::12,lg:::12:,lg/g,in,out,ps]
    \/ (A(XC) || A(XD))[g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "simplify"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/     lg(lg:::11)
        /\ XA[g:::11::,lg:::11,lg:::11:,lg/g,in,out,ps]
        /\ ps' = lg (-/+) (lg:::11,lg:::11:)
    \/ A(XB)[g:::12::,lg:::12,lg:::12:,lg/g,in,out,ps]
    \/ (A(XC) || A(XD))[g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "simplify"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/ A(XB)[g:::12::,lg:::12,lg:::12:,lg/g,in,out,ps]
    \/ (A(XC) || A(XD))[g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "defn. of A"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/ (ps(in) /\ XB /\ ps' = ps (-/+) (in,out))
       [g:::12::,lg:::12,lg:::12:,lg/g,in,out,ps]
    \/ (A(XC) || A(XD))[g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "simplify"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/     lg(lg:::12)
        /\ XB[g:::12::,lg:::12,lg:::12:,lg/g,in,out,ps]
        /\ ps' = lg (-/+) (lg:::12,lg:::12:)
    \/ (A(XC) || A(XD))[g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "simplify"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/ (A(XC) || A(XD))[g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "defn. of ||"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/ (   ps(in) /\ s' = s /\ ps' = ps (-/+) (in,{lg1,lg2})
        \/ A(XC)[g1::,lg1,lg1:/g,in,out]
        \/ A(XD)[g2::,lg2,lg2:/g,in,out]
        \/ ps(lg1:,lg2:) /\ s' = s /\ ps' = ps (-/+) ({lg1:,lg2:},out))
       [g:::2,lg::,lg:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "simplify"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/ lg(lg::) /\ s' = s /\ ps' = lg (-/+) (lg::,{lg:::21,lg:::22})
    \/ A(XC)[g:::21::,lg:::21,lg:::21:,lg/g,in,out,ps]
    \/ A(XD)[g:::22::,lg:::22,lg:::22:,lg/g,in,out,ps]
    \/     lg(lg:::21:,lg:::22:)
        /\ s' = s
        /\ ps' = lg (-/+) ({lg:::21:,lg:::22:},lg:)
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "defn. of A"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/ lg(lg::) /\ s' = s /\ ps' = lg (-/+) (lg::,{lg:::21,lg:::22})
    \/ (ps(in) /\ XC /\ ps' = ps (-/+) (in,out))
       [g:::21::,lg:::21,lg:::21:,lg/g,in,out,ps]
    \/ A(XD)[g:::22::,lg:::22,lg:::22:,lg/g,in,out,ps]
    \/     lg(lg:::21:,lg:::22:)
        /\ s' = s
        /\ ps' = lg (-/+) ({lg:::21:,lg:::22:},lg:)
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "simplify"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/     lg(lg:::21)
        /\ XC[g:::21::,lg:::21,lg:::21:,lg/g,in,out,ps]
        /\ ps' = lg (-/+) (lg:::21,lg:::21:)
    \/ A(XD)[g:::22::,lg:::22,lg:::22:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "defn. of A"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/     lg(lg:::21)
        /\ XC[g:::21::,lg:::21,lg:::21:,lg/g,in,out,ps]
        /\ ps' = lg (-/+) (lg:::21,lg:::21:)
    \/ (ps(in) /\ XD /\ ps' = ps (-/+) (in,out))
       [g:::22::,lg:::22,lg:::22:,lg/g,in,out,ps]
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "simplify"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/     lg(lg:::22)
        /\ XD[g:::22::,lg:::22,lg:::22:,lg/g,in,out,ps]
        /\ ps' = lg (-/+) (lg:::22,lg:::22:)
 ; ~ps(lg:) * ((A(XA) || A(XB)) ;; (A(XC) || A(XD)))[g::,lg,lg:/g,in,out]

 = "defn. of ;;"

       s' = s /\ ps' = {lg:::11,lg:::12}
    \/     lg(lg:::22)
        /\ XD[g:::22::,lg:::22,lg:::22:,lg/g,in,out,ps]
        /\ ps' = lg (-/+) (lg:::22,lg:::22:)
 ;    ~ps(lg:)
    * ((A(XA) || A(XB))[g:1,lg/g,out] \/ (A(XC) || A(XD))[g:2,lg/g,in])
      [g::,lg,lg:/g,in,out]

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;    ~ps(lg:)
    * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "obs'-;-*-prop, given ps = {lg:::11,lg:::12} => ~ps(lg:)"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;    (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])
      [{lg:::11,lg:::12}/ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;        (A(XA) || A(XB))[g:::1,lg,lg::,{lg:::11,lg:::12}/g,in,out,ps]
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "defn. of ||"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;        (   ps(in) /\ s' = s /\ ps' = ps (-/+) (in,{lg1,lg2})
           \/ A(XA)[g1::,lg1,lg1:/g,in,out]
           \/ A(XB)[g2::,lg2,lg2:/g,in,out]
           \/ ps(lg1:,lg2:) /\ s' = s /\ ps' = ps (-/+) ({lg1:,lg2:},out))
          [g:::1,lg,lg::,{lg:::11,lg:::12}/g,in,out,ps]
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            {lg:::11,lg:::12}(lg)
           /\ s' = s
           /\ ps' = {lg:::11,lg:::12} (-/+) (lg,{lg:::11,lg:::12})
       \/ A(XA)[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ A(XB)[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
       \/     {lg:::11,lg:::12}(lg:::11:,lg:::12:)
           /\ s' = s
           /\ ps' = {lg:::11,lg:::12} (-/+) ({lg:::11:,lg:::12:},lg::)
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;        A(XA)[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ A(XB)[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "defn. of A"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;        (ps(in) /\ XA /\ ps' = ps (-/+) (in,out))
          [g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ A(XB)[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            {lg:::11,lg:::12}(lg:::11)
           /\ XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11,lg:::12} (-/+) (lg:::11,lg:::11:)
       \/ A(XB)[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/ A(XB)[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "defn. of A"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/ (ps(in) /\ XB /\ ps' = ps (-/+) (in,out))
          [g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     {lg:::11,lg:::12}(lg:::12)
           /\ XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11,lg:::12} (-/+) (lg:::12,lg:::12:)
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::12:,lg:::11}
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "defn. of ||"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::12:,lg:::11}
       \/ (   ps(in) /\ s' = s /\ ps' = ps (-/+) (in,{lg1,lg2})
           \/ A(XC)[g1::,lg1,lg1:/g,in,out]
           \/ A(XD)[g2::,lg2,lg2:/g,in,out]
           \/ ps(lg1:,lg2:) /\ s' = s /\ ps' = ps (-/+) ({lg1:,lg2:},out))
          [g:::2,lg::,lg:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::12:,lg:::11}
       \/     {lg:::11,lg:::12}(lg::)
           /\ s' = s
           /\ ps' = {lg:::11,lg:::12} (-/+) (lg::,{lg:::21,lg:::22})
       \/ A(XC)[g:::21::,lg:::21,lg:::21:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ A(XD)[g:::22::,lg:::22,lg:::22:,{lg:::11,lg:::12}/g,in,out,ps]
       \/     {lg:::11,lg:::12}(lg:::21:,lg:::22:)
           /\ s' = s
           /\ ps' = {lg:::11,lg:::12} (-/+) ({lg:::21:,lg:::22:},lg:)
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::12:,lg:::11}
       \/ A(XC)[g:::21::,lg:::21,lg:::21:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ A(XD)[g:::22::,lg:::22,lg:::22:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "defn. of A"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::12:,lg:::11}
       \/ (ps(in) /\ XC /\ ps' = ps (-/+) (in,out))
          [g:::21::,lg:::21,lg:::21:,{lg:::11,lg:::12}/g,in,out,ps]
       \/ A(XD)[g:::22::,lg:::22,lg:::22:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::12:,lg:::11}
       \/     {lg:::11,lg:::12}(lg:::21)
           /\ XC[g:::21::,lg:::21,lg:::21:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11,lg:::12} (-/+) (lg:::21,lg:::21:)
       \/ A(XD)[g:::22::,lg:::22,lg:::22:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::12:,lg:::11}
       \/ A(XD)[g:::22::,lg:::22,lg:::22:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "defn. of A"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::12:,lg:::11}
       \/ (ps(in) /\ XD /\ ps' = ps (-/+) (in,out))
          [g:::22::,lg:::22,lg:::22:,{lg:::11,lg:::12}/g,in,out,ps]
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::12:,lg:::11}
       \/     {lg:::11,lg:::12}(lg:::22)
           /\ XD[g:::22::,lg:::22,lg:::22:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11,lg:::12} (-/+) (lg:::22,lg:::22:)
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "simplify"

   s' = s /\ ps' = {lg:::11,lg:::12}
 ;            XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::11:,lg:::12}
       \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
           /\ ps' = {lg:::12:,lg:::11}
    ;    ~ps(lg:)
       * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
          \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])

 = "s'ps'-;-prop"

(          XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
        /\ ps' = {lg:::11:,lg:::12}
    \/     XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
        /\ ps' = {lg:::12:,lg:::11}
 ;    ~ps(lg:)
    * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
       \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out]))
[s,{lg:::11,lg:::12}/s',ps']

 = "\\/-;-distr"

(   (      XA[g:::11::,lg:::11,lg:::11:,{lg:::11,lg:::12}/g,in,out,ps]
        /\ ps' = {lg:::11:,lg:::12}
     ;    ~ps(lg:)
        * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
           \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out]))
 \/ (      XB[g:::12::,lg:::12,lg:::12:,{lg:::11,lg:::12}/g,in,out,ps]
        /\ ps' = {lg:::12:,lg:::11}
     ;    ~ps(lg:)
        * (   (A(XA) || A(XB))[g:::1,lg,lg::/g,in,out]
           \/ (A(XC) || A(XD))[g:::2,lg::,lg:/g,in,out])))
[{lg:::11,lg:::12},s/ps',s']
