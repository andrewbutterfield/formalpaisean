\section{Labelled Action System Execution}
\begin{code}
module LASExec where
import APTypes
import APFuns
import APEval
import LASTypes

import Data.Char
import qualified Data.Map as M

import Dbg
\end{code}

We now define a means to simulate a LAS,
be repeatedly executing single steps, each of which
evaluates an enabled action, until nothing more is possible.

We do this by directly interpreting derived constructs
such as assignment or sequential composition in an operational
fashion.

\subsection{Evaluating Propositions}

We need to evaluate the propositional subset of predicates,
namely $\true$, $\false$, atomic predicates, and logic operators:
\[ \lnot \quad \lor \quad \land \quad \implies \]
\begin{code}
evalPred :: State -> Pred -> Val
evalPred _  TRUE = vTrue
evalPred st (Atm e) = evalExpr st e
evalPred st (Not pr) = boolV $ not $ vBool $ evalPred st pr
evalPred st (Or prs) = boolV $ or $ map (vBool . evalPred st) prs
evalPred st pr
 | isFalse pr    =  vFalse
 | isAnd pr      =  evalPred st $ transAnd pr
 | isImplies pr  =  evalPred st $ transImplies pr
 | otherwise
     =  error ("evalPred, expected a proposition, got: "++show pr)
\end{code}

\newpage
\subsection{``Executing'' Predicates}

We define a function that takes a state and some derived (deterministic)
predicates
and simulates their execution.
The derived predicates that produce state change for us are:
\[ \cond\_ \quad ; \quad := \quad\Skip \]
\begin{code}
execPred :: Pred -> State -> State
execPred pr st
 | isCond pr  =  execCond pr st
 | isSeq  pr  =  execSeq  pr st
 | isAsg  pr  =  execAsg  pr st
 | isSkip pr  =  execSkip pr st
 | otherwise  =  error ("execPred - not-executable : "++show pr)
\end{code}

\subsubsection{``Executing'' $\cond\_$}

\begin{code}
execCond (DRV _ [p,_,b,_,q] _) st
 = if vBool $ evalPred st b then execPred p st else execPred q st
\end{code}

\subsubsection{``Executing'' $;$}

\begin{code}
execSeq (DRV _ [p,_,q] _) st
  = let stm = execPred p st
    in        execPred q stm
\end{code}

\subsubsection{``Executing'' $:=$}

\begin{code}
execAsg (DRV _ [Atm v@(Var x),_,Atm e] _) state@(State st)
 = let ve = evalExpr state e
   in  State $ M.insert x ve st
\end{code}

\subsubsection{``Executing'' $\Skip$}

\begin{code}
execSkip _ st = st -- all it does, really
\end{code}

\newpage
\subsection{LAS Execution}

Once we initialise the state,
we proceed to execute by:
checking the termination condition;
then evaluating all the enabled guards
to provide a menu of options;
choosing one;
and executing its body predicate.
We then rinse and repeat,
until the termination condition is satisfied
or no guards are enabled.

\subsubsection{Starting State}

We simply evaluate all initialisation expressions w.r.t. to
the empty state, and then construct the initial state with the
corresponding bindings.
\begin{code}
setupLASState :: LASys -> State

setupLASState lasys
 = mkState $map eval0 $ asInit lasys
 where
   eval0 (v,e) = (v,evalExpr state0 e) -- es should not depend on state
\end{code}

\subsubsection{Enabled Guards}

Here we work through the guarded commands,
returning a list of those enabled:
\begin{code}
evalGuards :: LASys -> State -> [GCmd]
evalGuards lasys st
 = filter (enabled st) $ asGCmds lasys
 where
   enabled st gcmd = vBool $ evalPred st $ guard gcmd
\end{code}

\subsubsection{Running a LAS}

\begin{code}
lasStart lasys
  = do putStrLn "\nRunning..."
       let state0 = setupLASState lasys
       putStrLn ("Initial State = " ++ show state0)
       lasRun lasys state0

lasRun lasys state
 = if cont
   then lasStep lasys state
   else do putStrLn "LAS Terminated"
           putStrLn ("Final State = " ++ show state)
 where cont = vBool $ evalPred state $ asCont lasys

lasStep lasys state
 = do let enabledC = evalGuards lasys state
      mcmd <- lasSelect enabledC
      case mcmd of
        Nothing   ->  putStrLn "Halt"
        Just cmd  ->  lasDo lasys state cmd

lasSelect []
 = do putStrLn "No Guard Enabled"
      return Nothing
lasSelect gcmds
 = do putStrLn $ unlines $ map ngcShow $ zip [1..] gcmds
      putStr "Enter Cmd Number, 0 to exit > "
      utxt <- fmap trim $ getLine
      if not (null utxt) && all isDigit utxt
       then do let n = read utxt
               if 1 <= n && n <= count
                then return $ Just $ gcmds!!(n-1)
                else if n == 0
                      then return Nothing
                      else err "Number not in range!"
       else err "Not a Number!"
 where
   count = length gcmds
   err s = do { putStrLn s ; lasSelect gcmds }

ngcShow (i,gcmd)
  = lpad 3 (show i)
  ++ " : "
  ++ rpad 15 (show $ guard gcmd)
  ++ "  ->  "
  ++ show (gcPred gcmd)
lpad w s
  | len < w    =  replicate (w-len) ' ' ++ s
  | otherwise  =  s
  where len = length s
rpad w s
  | len < w    =  s ++ replicate (w-len) ' '
  | otherwise  =  s
  where len = length s

trim = reverse . ltrim . reverse . ltrim
ltrim "" = ""
ltrim str@(c:cs)
 | isSpace c = ltrim cs
 | otherwise = str


lasDo lasys state cmd
 = do let state' = execPred (gcPred cmd) state
      putStrLn ("Next  State = " ++ show state')
      lasRun lasys state'
\end{code}
