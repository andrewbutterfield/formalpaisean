\section{Alphabetised Predicate Utilities}
\begin{code}
module APUtils where
import APTypes
\end{code}

\subsection{General Utilities}

General utilities:
\begin{code}
isK (K _) = True
isK _     = False
\end{code}

\subsection{Substitutions}

\subsubsection{\texttt{Expr} for \texttt{Var} in Predicates}
\begin{code}
psub :: [(Var,Expr)] -- target/replacement
      -> Pred -> Pred

psub [] pr = pr

psub subs (Atm e)     =  Atm $ esub subs e
psub subs (Not pr)    =  Not $ psub subs pr
psub subs (Or prs)    =  Or $ map (psub subs) prs
psub subs (EX bvs pr) =  EX bvs $ psub (bvs `strip` subs) pr

psub subs (Sub pr ves)
 =  Sub (psub (tvs `strip` subs) pr) $ map (repsub subs) ves
 where
   tvs = map fst ves
   repsub subs (v,e) = (v,esub subs e)

psub subs (Rec x pr)  =  Rec x $ psub subs pr

psub subs pr@(DRV _ _ _) =  Sub pr subs

psub subs pr  =  pr
\end{code}

Stripping out target variables and their replacements.
\begin{code}
strip :: [Var] -> [(Var,Expr)] -> [(Var,Expr)]
strip _   []                 =  []
strip bvs (sub@(tv,_):subs)
 | tv `elem` bvs  =  strip bvs subs
 | otherwise      =  sub : strip bvs subs
\end{code}

\subsubsection{\texttt{Expr} for \texttt{Var} in Expressions}
\begin{code}
esub :: [(Var,Expr)] -- target/replacement
      -> Expr -> Expr

esub subs v@(Var x)
  =  sapply subs x
  where
    sapply [] x  =  v
    sapply ((tv,rv):subs) x
     | tv == x    =  rv
     | otherwise  =  sapply subs x

esub subs (App f es)  =  App f $ map (esub subs) es
esub subs e           =  e
\end{code}


\subsubsection{\texttt{Pred} for \texttt{PH} in Predicates}

We need code for handling recursions that substitutes
predicates for recursion place-holder variables:
\begin{code}
rPsub :: Pred -> String -> Pred -> Pred

rPsub rpr x pr@(PH y)
 | x == y     =  rpr
 | otherwise  =  pr

rPsub rpr x pr@(Rec y bpr)
 | x == y     =  pr
 | otherwise  = Rec y $ rPsub rpr x bpr

rPsub rpr x (Not pr) = Not $ rPsub rpr x pr
rPsub rpr x (Or prs) = Or $ map (rPsub rpr x) prs
rPsub rpr x (EX vs pr) = EX vs $ rPsub rpr x pr
rPsub rpr x (Sub pr ves) = Sub (rPsub rpr x pr) ves
rPsub rpr x (DRV l prs r) = DRV l (map (rPsub rpr x) prs) r

rPsub rpr x pr = pr
\end{code}
