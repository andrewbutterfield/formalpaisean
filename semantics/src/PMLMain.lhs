\section{PML Main}

\begin{code}
module Main where

import System.Directory
import System.FilePath
import System.Console.Haskeline
import Control.Monad.Trans.Class
import qualified Data.Map.Strict as M

import Utilities

import ErrM
import LexPML
import AbsPML
import ParPML
import PrintPML

import SynResources
import SynAction
import ResHandling
import SynPML
import PMLResources
import SynWeak
import WeakResources
import PrettyPrint 
import CalcPredicates 
import CalcSteps 
import CalcRun
\end{code}


\subsection{Tool State}

\begin{code}
data PMLState
 = PMLS { pmlVersion  :: String
        , pmlBNFC     :: Maybe PROCESS
        , pmlWeak     :: Maybe WkPML
        , pmlStd      :: Maybe PML
        , pmlTypedRes :: ([(ResExpr,RType)], RATMap)
        , pmlDataFlow :: ResGraph
        , pmlActions  :: [(String,PMLAction)]
        }

initPMLState
 = PMLS { pmlVersion  = "0.2"
        , pmlBNFC     = Nothing
        , pmlWeak     = Nothing
        , pmlStd      = Nothing
        , pmlTypedRes = ([], M.empty)
        , pmlDataFlow = RG M.empty
        , pmlActions  = []
        }

type REPLAction = [String] -> PMLState -> InputT IO PMLState

actFail :: PMLState -> String -> InputT IO PMLState
actFail pmsl what
 = do outputStrLn $ "Action Failed: " ++ what
      return pmsl
\end{code}

\subsection{Tool Actions}

We define an action specification as having four parts:
\begin{enumerate}
  \item the string to invoke the command
  \item a short help string for the command
  \item a long help text for the command
  \item the code for the action of type \texttt{REPLAction}.
\end{enumerate}
\begin{code}
type ActSpec
 = ( String  -- command string
   , String  -- short help
   , [String] -- long help (as lines)
   , REPLAction -- the code
   )
\end{code}
The ``?'' command displays all the short help strings
The ``? cmd'' command displays the long help for ``cmd''.

\subsubsection{State Display}

\begin{code}
displayState :: REPLAction
displayState [] pmls
 = do summariseState pmls
      return pmls

displayState (comp:_) pmls
 | comp == "v"  =  displayComp pmls  $ "Version " ++ pmlVersion pmls
 | comp == "b"  =  displayComp pmls  $ "BNFC:" ++ lmshow (pmlBNFC pmls)
 | comp == "w"  =  displayComp pmls  $ "Weak:" ++ mshow (pmlWeak pmls)
 | comp == "p"  =  displayComp pmls  $ "PML:" ++ mshow (pmlStd pmls)
 | comp == "r"  =  displayComp pmls
                      (    "\nAttribute Types:\n"
                        ++ (unlines' $ map ratshow (M.toList $ snd $ pmlTypedRes pmls))
                        ++ "\n\nResource Exprs:\n"
                        ++ (unlines' $ map rtshow (fst $ pmlTypedRes pmls)))
 | comp == "d"  =  displayComp pmls  $ "DataFlow:\n" ++ show (pmlDataFlow pmls)
 | comp == "a"  =  displayComp pmls  $ "Actions:" ++ nlshow (pmlActions pmls)
 | otherwise    =  displayState [] pmls

summariseState pmls
 = do outputStrLn $ "Version " ++ pmlVersion pmls
      outputStrLn $ mshowWith sumPROCESS (pmlBNFC pmls)
      outputStrLn $ mshowWith sumWkPML (pmlWeak pmls)
      outputStrLn $ mshowWith sumPML (pmlStd pmls)
      outputStrLn $ "ResExprs: " ++ (show $ length $ fst $ pmlTypedRes pmls)
      outputStrLn $ "DataFlow: " ++ (sumDFLOW $ pmlDataFlow pmls)
      outputStrLn $ "Actions: " ++ (show $ length $ pmlActions pmls)

lmshow Nothing = " Nothing"
lmshow (Just x) = '\n':show x

nlshow [] = ""
nlshow (x:xs) = '\n':show x ++ nlshow xs

rtshow (r,t) = show r ++ " : " ++ show t

ratshow ((r,a),t) = r ++ '.':a ++ " : " ++ show t

displayComp pmls txt
 = do outputStrLn txt
      return pmls


msshow sh Nothing = " Nothing"
msshow sh (Just x) = '\n':sh x

sumPROCESS (Process (ID name) _) = "BNFC: "++name
sumWkPML (WPML nm wmap) = "Weak PML: "++nm++", "++show n++" Task(s)"
 where
   n = M.size wmap
sumPML (PML nm prims) = "PML: "++nm
sumDFLOW (RG rgmap) = show (M.size rgmap) ++ " item(s)"

displaySpec :: ActSpec
displaySpec
 = ( "s"
   , " <opt. state comp>, displays program state"
   , [ "with no argument, displays summary of all state"
     , "With state-comp argument, displays component in full"
     , "v : program version"
     , "b : BNFC syntax"
     , "w : weak PML form"
     , "p : standard PML form"
     , "r : typed resource expressions"
     , "d : data-flow induced resource graph"
     , "a : all basic actions"
     ]
   , displayState
   )
\end{code}



\subsubsection{File Handling}

\begin{code}
loadPMLFile :: REPLAction
loadPMLFile [] pmls
 = do dc <- lift $ getDirectoryContents "pml"
      let fps = filter isPMLfile dc
      outputStrLn ("Current Directory:\n"++unlines (map show (zip [1..] fps)))
      resp <- getInputLine "Enter file number :- "
      case resp of
        Nothing -> actFail pmls "no filenumber"
        Just txt
          -> do let fnum  = (read txt :: Int)
                loadPMLFile [fps!!(fnum-1)] pmls

loadPMLFile (fp:_) pmls
 = do outputStrLn ("File "++fp++" chosen.")
      txt <- lift $ readFile $ "pml/" ++ fp
      let toks = tokens txt
      case pPROCESS toks of
        Bad msg  ->  actFail pmls $ "Bad process - "++msg
        Ok proc  ->  do let weak = transPROCESS2WPML proc
                        return pmls{ pmlBNFC = Just proc
                                   , pmlWeak = Just weak
                                   , pmlStd = Just $ transPROCESS proc
                                   , pmlTypedRes = typeCheck weak
                                   , pmlDataFlow = dataFlow weak
                                   , pmlActions = listActions weak
                                   }

isPMLfile fp = takeExtension fp == ".pml"

loadSpec :: ActSpec
loadSpec
 = ( "l"
   , " <opt. filename>, loads PML file into memory"
   , [ "with no argument, looks up PML files and offers a choice"
     , "With argument, tries to load specified file"
     ]
   , loadPMLFile
   )
\end{code}

\subsubsection{Help Action}

\begin{code}
helpAct :: REPLAction

helpAct [] pmls
  = shelp dispatchTable
  where
    shelp [] = return pmls
    shelp ((cmd,hlp,_,_):rest)
     = do outputStrLn (cmd ++ " : " ++ hlp)
          shelp rest

helpAct (cmd:_) pmls
 = case cmdLookup cmd dispatchTable of
    Nothing  ->  do outputStrLn $ "Unrecognised command - "++cmd
                    helpAct [] pmls
    Just (_,_,lhelp,_)
     -> do outputLines lhelp
           return pmls

outputLines [] = return ()
outputLines (ln:lns)
 = do outputStrLn ln
      outputLines lns

helpSpec :: ActSpec
helpSpec
 = ( "?"
   , " <opt. cmd>, displays help info"
   , [ "With no arguments, displays all command short helps"
     , "With command argument, displays its long help"
     ]
   , helpAct
    )

quitSpec :: ActSpec
quitSpec
 = ( "q"
   , "Quits program"
   , [ "Quits program immediately"
     , "No saving of state"
     , "No Warning !"
     ]
   , undefined
   )
\end{code}



\subsection{Dispatch Table}

\begin{code}
dispatchTable :: [ActSpec]
dispatchTable
 = [ quitSpec
   , loadSpec
   , displaySpec
   , helpSpec
   ]

cmdLookup cmd [] = Nothing
cmdLookup cmd (spec@(cmd',_,_,_):rest)
 | cmd == cmd'  =  Just spec
 | otherwise    =  cmdLookup cmd rest
\end{code}


\subsection{Tool Mainline}

\begin{code}
main :: IO ()
main = runInputT defaultSettings  $ launch initPMLState

launch pmls
 = do outputStrLn $ "\n\tWelcome to PML " ++ pmlVersion pmls ++ "\n"
      helpAct [] pmls
      loop pmls

loop :: PMLState -> InputT IO ()
loop pmls = do

    minput <- getInputLine "\n> "
    case minput of
        Nothing    -> loop pmls   -- dig heels in...
        Just input -> dispatch (trim input) pmls

dispatch "" pmls = loop pmls
dispatch input pmls

 | input == "q" = do outputStrLn "\n\tGoodbye!\n"
                     return ()

 | otherwise
    = case words input of
        [] -> loop pmls
        (cmd:args)
           ->  case cmdLookup cmd dispatchTable of
                 Nothing -> do outputStrLn $ "Unknown command - "++cmd
                               loop pmls
                 Just (_,_,_,action)
                   -> do pmls' <- action args pmls
                         loop pmls'
\end{code}


