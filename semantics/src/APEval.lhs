\section{Action System Evaluator}
\begin{code}
module APEval where
import Utilities
import APTypes
import APUtils
import APFuns

import Data.List
import qualified Data.Map as M

import Dbg
\end{code}

Here we develop an evaluator that enables us to animate or simulate
action systems.

\subsection{Values}

We define a value as being a number, string or sets of values:
\begin{code}
data Val = N Integer | S String | P [Val] deriving (Eq,Ord)

instance Show Val where
  show (N i)   =  show i
  show (S s)   =  s
  show (P vs)  =  sshow vs
\end{code}


\subsection{Defining Functions}

We shall define some basic builtin functions/operators on values,
nothing that
Evaluation is strict:
\begin{eqnarray*}
   E\sem{e} &:& Expr \fun Val
\\ F\sem{f} &:& String \fun (Val^* \fun Val)
\\ E\sem{f(a_1,\ldots,a_n)} &=& F\sem{f}(E\sem{a_1},\ldots,E\sem{a_n})
\end{eqnarray*}
For each such operator we shall define a constructor,
and its implementation on values.

Given a function called \texttt{fun}, we shall define the following:
\begin{verbatim}
funName :: String
fun     :: Expr -> ... -> Expr -> Expr
funEval :: [Val] -> Val
\end{verbatim}

We define a generic error message:
\begin{code}
ferror nm expected es
  = error ("Function '"++nm++"' "++expected++", given: "++show es)
\end{code}

\newpage
\subsubsection{Arithmetic Operators}

\begin{code}
plusName = "+"
plus e1 e2 = App plusName [e1,e2]
plusEval [N i1, N i2] = N (i1 + i2)
plusEval es = ferror plusName "expects two numbers" es

minusName = "-"
minus e1 e2 = App minusName [e1,e2]
minusEval [N i1, N i2] = N (i1 - i2)
minusEval es = ferror minusName "expects two numbers" es

timesName = "*"
times e1 e2 = App timesName [e1,e2]
timesEval [N i1, N i2] = N (i1 * i2)
timesEval es = ferror timesName "expects two numbers" es

divdName = "/"
divd e1 e2 = App divdName [e1,e2]
divdEval es@[N i1, N i2]
 | i2 == 0  = ferror divdName "has zero divisor" es
 | otherwise = N (i1 `div` i2)
divdEval es = ferror divdName "expects two numbers" es
\end{code}

\newpage
\subsubsection{Set Operators}

\begin{code}
unionName = "U"
union e1 e2 = App unionName [e1,e2]
unionEval [P vs1, P vs2] = P $ mkset (vs1 ++ vs2)
unionEval es = ferror unionName "expects two sets" es

intsctName = "^"
intsct e1 e2 = App intsctName [e1,e2]
intsctEval [P vs1, P vs2] = P $ mkset (vs1 ++ vs2)
intsctEval es = ferror intsctName "expects two sets" es

diffName = "\\"
diff e1 e2 = App diffName [e1,e2]
diffEval [P vs1, P vs2] = P $ mkset (vs1 ++ vs2)
diffEval es = ferror diffName "expects two sets" es

oslashName = "(/)"
oslash e1 e2 e3 = App oslashName [e1,e2,e3]
oslashEval [P ell, P em, P en] = P $ mkset ((ell \\ em)++en)
oslashEval es = ferror oslashName "expects three sets" es
\end{code}

\newpage
\subsubsection{Relational Operators}

We shall model $False$ by $0$ and $True$ by any non-zero value:
\begin{code}
vFalse       =  N    0
vTrue        =  N    1 -- canonical True
vBool (N i)  =  i /= 0
vBool v      =  True
boolV False  =  vFalse
boolV True   =  vTrue
\end{code}

Now, our relations (valid \texttt{Atm} predicates)
\begin{code}
eqName = "="
eq e1 e2 = App eqName [e1,e2]
eqEval [v1, v2] = boolV (v1 == v2)
eqEval es = ferror eqName "expects two values" es

ltName = "<"
lt e1 e2 = App ltName [e1,e2]
ltEval [v1, v2] = boolV (v1 < v2)
ltEval es = ferror ltName "expects two values" es

mofName = "in"
mof e1 e2 = App mofName [e1,e2]
mofEval [v, P vs] = boolV (v `elem` vs)
mofEval es = ferror mofName "expects value and set" es

subsetName = "subset"
subset e1 e2 = App subsetName [e1,e2]
subsetEval [P vs1, P vs2] = boolV $ null $ (vs1 \\ vs2)
subsetEval es = ferror subsetName "expects two sets" es
\end{code}

\subsubsection{Conditional Operator}
\begin{code}
condName = "cond"
cond e1 e2 e3 = App condName [e1,e2,e3]
condEval [v1, v2, v3] = if vBool v2 then v1 else v3
condEval es = ferror condName "expects three values" es
\end{code}

\newpage
\subsection{Builtin Function Semantics}

The following function is a one-stop shop that dispatches based
on the function string in an \texttt{App}:

\begin{code}
builtinFuns
 = M.fromList
    [ (plusName,plusEval)
    , (minusName,minusEval)
    , (timesName,timesEval)
    , (divdName,divdEval)
    , (unionName,unionEval)
    , (intsctName,intsctEval)
    , (diffName,diffEval)
    , (oslashName,oslashEval)
    , (eqName,eqEval)
    , (ltName,ltEval)
    , (mofName,mofEval)
    , (subsetName,subsetEval)
    , (condName,condEval)
    ]

lsBuiltins = M.keys builtinFuns

evalFun :: String -> [Val] -> Val
evalFun funName vs
 = case M.lookup funName builtinFuns of
     Nothing -> error ("evalFun '"++funName++"' does not exist!")
     Just fun  ->  fun vs
\end{code}


\newpage
\subsection{States}

A state is a mapping from a variable to a value
\begin{code}
newtype State = State (M.Map Var Val)

instance Show State where
  show (State state) = mapshow $ M.assocs state

mapshow xs  =  "{" ++ concat (intersperse "," $ map pshow xs) ++ " }"

pshow (k,a) = ' ' : show k ++ " >-> " ++ show a
\end{code}

\subsubsection{The Empty State}

We define an uninitialised state as an empty map:
\begin{code}
state0 :: State
state0 = State M.empty
\end{code}

\subsubsection{Building States}

First, we can build a state based on all the variables mentioned
in an expression or predicate,
all mapped to a value indicating their un-initialised status:
\begin{code}
uninitV = S "?"

newVar v = (v, uninitV)

exprState :: Expr -> State
exprState e = State $ M.fromList $ map newVar $ eFV e

predState :: Pred -> State
predState pr = State $ M.fromList $ map newVar $ pFV pr
\end{code}

We also allow explicit state building from var/val lists:
\begin{code}
mkState :: [(Var,Val)] -> State
mkState = State . M.fromList
\end{code}

\newpage
\subsection{Expression Evaluation}

We do expression evaluation w.r.t. a state in the usual way:
\begin{code}
evalExpr :: State -> Expr -> Val
evalExpr _  (Num i)  = N i
evalExpr _  (Txt s)  = S s
evalExpr st (Set es) = P $ map (evalExpr st) es

evalExpr (State st) (Var var)
 = case M.lookup var st of
    Nothing   ->  error ("Variable '"++show var++"' not defined")
    Just val  ->  val

evalExpr st (App f es) = evalFun f $ map (evalExpr st) es
\end{code}
