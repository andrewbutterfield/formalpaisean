\section{Debugger}
\begin{code}
module Dbg where

import System.IO
import System.IO.Unsafe
import Debug.Trace

dbgsh sh msg x = trace (msg++sh x) x

dbg msg x = dbgsh show msg x

cdbg p msg x
 | p x        =  dbg msg x
 | otherwise  =  x

class Dshow t where dshow :: t -> String
ddbg msg x = dbgsh dshow msg x

debug s e = unsafePerformIO $ mdebug s e
mdebug s e
  = do hPutStrLn stderr s
       return e
shdebug s e = debug (s++show e) e
\end{code}
