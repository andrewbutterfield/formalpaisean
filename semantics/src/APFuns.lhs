\section{Alphabetised Predicate Functions}
\begin{code}
module APFuns where
import Data.List
import APTypes
import APUtils
\end{code}

\subsection{Alphabetised Predicates}


\[
  (\alf P, P)
\]
We require that $FV(P) \subseteq \alf P$.
\begin{code}
invPA :: APred -> Bool
invPA (AP (A vs) pr) = pFV pr `subsetof` vs
\end{code}

Subset is easily defined:
\begin{code}
small `subsetof` large = null (small \\ large)
\end{code}

Free variables are computed by a straightforward recursive descent:
\begin{code}
pFV :: Pred -> [Var]
pFV (Atm e) = eFV e
pFV (Not pr) = pFV pr
pFV (Or prs) = mkSet $ map pFV prs
pFV (EX vs pr) = pFV pr \\ vs
pFV (Sub pr subs)
 = mkSet $ (pFV pr \\ vs):map eFV es
 where (vs,es) = unzip subs
pFV (Rec x pr) = pFV pr
pFV (DRV left prs right) = mkSet $ map pFV prs
pFV _ = []

eFV :: Expr -> [Var]
eFV (Var v) = [v]
eFV (App f es) = mkSet $ map eFV es
eFV (Set es) = mkSet $ map eFV es
eFV _ = []
\end{code}

We represent sets as sorted duplicate-free lists:
\begin{code}
mkset :: Ord a => [a] -> [a]
mkset = nub . sort

mkSet :: Ord a => [[a]] -> [a]
mkSet = mkset . concat
\end{code}

\newpage

We now provide a mechanism to `alphabetise' a predicate,
by adding its free variables as an alphabet:
\begin{eqnarray*}
   P &\mapsto& (FV(P),P)
\end{eqnarray*}
\begin{code}
alphabetise :: Pred -> APred
alphabetise pr = AP (A $ pFV pr) pr

alfv :: APred -> [Var]
alfv (AP (A vs) _) = vs
\end{code}

The only operator we supply at present to modify the alphabet
is one that extends it:
\[
  (\alf P,P) \mapsto (\alf P \cup V, P)
\]
\begin{code}
alfExtend :: [Var] -> APred -> APred
alfExtend vs (AP (A alfs) pr)  =  AP (A $ mkset (vs++alfs)) pr
\end{code}

\newpage
\subsubsection{Input \& Output Alphabets}

In an alphabet, an un-dashed variable denotes a starting or input value,
while a dashed variable denotes a final or output variable.
So we can partition an alphabet into these two portions:
\[
  \alf P = in\alf P \cup out\alf P
\]
\begin{code}
isIn (V "")  =  error "isIn applied to null variable"
isIn (V v)   =  last v /= '\''

ioPartition :: APred -> ([Var],[Var])
ioPartition (AP (A vs) _)  =  partition isIn vs

inA, outA :: APred -> [Var]
inA   =  fst . ioPartition
outA  =  snd . ioPartition
\end{code}

\[
  A' = \{ v' | v \in A \}
\]
\begin{code}
adash :: [Var] -> [Var]
adash = adecor "'"

adecor :: String -> [Var] -> [Var]
adecor d = map (vdecor d)

vdecor :: String -> Var -> Var
vdecor d (V v) = V (v++d)

vdash :: Var -> Var
vdash = vdecor "'"
\end{code}

Sometimes we want to undo dashes:
\begin{code}
aundash :: [Var] -> [Var]
aundash = map vundash

vundash :: Var -> Var
vundash (V "") = error ""
vundash (V v) = V $ init v
\end{code}

Homogeneous alphabets
\[
 A = \{x,y,\ldots,z,x',y',\ldots,z'\}
\]
\begin{code}
isHomogeneous vs
 = let (ins,outs) = partition isIn vs
   in adash ins == outs

homogenise vs
 = let (ivs,ovs) = partition isIn vs
   in mkSet [ ivs++aundash ovs, ovs++adash ivs ]
\end{code}

\newpage
\subsection{Derived Predicates}

The following derived predicates appear in UTPP.
We provide code to define these using the derived predicate constructors
of \texttt{Pred},
namely \texttt{K} and \texttt{DRV}.
Some are just predicate syntactical sugar
with names of the form \texttt{pXxxx},
whilst others define alphabetised predicates ,
along with various restrictions on the alphabets of the components.
We first define a strict constructor for these,
names of the form \texttt{apXxxx},
that checks compliance on the components as given
and then define a more liberal constructor,
name of the form \texttt{aplXxxx},
that will attempt to make them fit by extending alphabets,
and then calling the strict constructor.
We also provide a translation function \texttt{transXxxx} that replaces
the derived predicate syntax by its definition.

We have the following derived predicates:
\[
  \bigwedge
  \qquad
  \false
  \qquad
  \implies
\]
and the following derived alphabetised predicates:
\[
  \cond\_
  \qquad
  ;
  \qquad
  :=
  \qquad
  \Skip
  \qquad
  \sqcap
  \qquad
  \bot
  \qquad
  \top
  \qquad
  \vdash
  \qquad
  \top_{\mathbf{D}}
  \qquad
  \bot_{\mathbf{D}}
\]
It should be noted that what is done here is purely syntactical, but does
allow a description using derived predicates to be converted to one in terms
of the base predicates.

\newpage
\subsubsection{Logical-And}

\begin{eqnarray*}
   \bigwedge(P,\ldots,P) &\defs& \lnot\bigvee(\lnot P,\ldots,\lnot P)
\end{eqnarray*}
\begin{code}
leftAnd = "/\\("
rightAnd = ")"

pAnd prs  =  DRV leftAnd prs rightAnd

isAnd (DRV left _ right)  =  left == leftAnd && right == rightAnd
isAnd _                   =  False

transAnd pr@(DRV _ prs _)
 | isAnd pr  =  Not $ Or $ map Not prs
transAnd _   =  error "transAnd: not a pAnd"

recTransAnd pr@(DRV _ prs _)
    | isAnd pr  =  Not $ Or $ map (Not . recTransAnd) prs
recTransAnd pr  =  pr

-- simplify version
sAnd []    =  TRUE
sAnd [pr]  =  pr
sAnd prs   =  pAnd prs
\end{code}

\newpage
\subsubsection{False}

\begin{eqnarray*}
   \false
    &\defs& \lnot\true
\end{eqnarray*}
\begin{code}
kFalse = K "FALSE"

pFalse = kFalse

isFalse pr@(K _)  =  pr == kFalse
isFalse _         =  False

transFalse pr@(K _)
 | isFalse pr  =  Not TRUE
transFalse _   =  error "transFalse: not a pFalse"
\end{code}

\newpage
\subsubsection{Implication}

\begin{eqnarray*}
   P \implies Q &\defs& \lnot P \lor Q
\end{eqnarray*}
\begin{code}
leftImplies = "("
rightImplies = ")"
kImplies = K "=>"

pImplies p q  =  DRV leftImplies [p,kImplies,q] rightImplies

isImplies (DRV left [_,k,_] right)
             =  (left,k,right) == (leftImplies,kImplies,rightImplies)
isImplies _  =  False

transImplies pr@(DRV _ [p,_,q] _)
 | isImplies pr  =  Or [Not p,q]
transImplies _   =  error "transImplies: not a pImplies"
\end{code}

\newpage
\subsubsection{Conditional}

\begin{eqnarray*}
   \textbf{provided} && \alf b \subseteq \alf P = \alf Q \ldots
\\ P \cond{b} Q &\defs& (b \land P) \lor (\lnot b \land Q)
\\  \alf{(P \cond b Q)} &\defs& \alf P
\end{eqnarray*}
\begin{code}
apCond p b q
 | ab `subsetof` ap && ap == aq
      = AP (A ap) $ pCond (apPred p) (apPred b) (apPred q)
 | otherwise  =  error "apCond: alphabet mismatch"
 where
   ab = alfv b
   ap = alfv p
   aq = alfv q

aplCond p b q
 = apCond (alfExtend (ab++aq) p) b (alfExtend (ab++ap) q)
 where
   ab = alfv b
   ap = alfv p
   aq = alfv q

transCond (AP a pr@(DRV _ [p,_,b,_,q] _))
 | isCond pr  =  AP a $ Or [pAnd [b,p],pAnd [Not b,q]]
transCond _   =  error "transCond: not a pCond"
\end{code}

Now, the predicate bit:
\begin{code}
leftCond = "("
rightCond = ")"
k1Cond = K "<|"
k2Cond = K "|>"

pCond p b q =  DRV leftCond [p,k1Cond,b,k2Cond,q] rightCond

isCond (DRV left [_,k1,_,k2,_] right)
             =  [left,right] == [leftCond,rightCond]
                  && [k1,k2] == [k1Cond,k2Cond]
isCond _     =  False
\end{code}

\newpage
\subsubsection{Sequential Composition}

\begin{eqnarray*}
   \textbf{provided} && out\alf P = in\alf P' = \{v'\}
\\ P(v');Q(v) &\defs& \exists v_0 @ P(v_0) \land Q(v_0)
\\ in\alf(P(v');Q(v)) &\defs& in\alf P
\\ out\alf(P(v');Q(v)) &\defs& out\alf P
\end{eqnarray*}
\begin{code}
apSeq p q
 | adash iaq == oap
              =  AP (A $ mkSet [iap,oaq]) $ pSeq (apPred p) (apPred q)
 | otherwise  =  error "apSeq: alphabet mismatch"
 where
   (iap,oap) = ioPartition p
   (iaq,oaq) = ioPartition q

aplSeq p q = apSeq (alfExtend (adash iaq) p)
                   (alfExtend (aundash oap) q)
 where
   (iap,oap) = ioPartition p
   (iaq,oaq) = ioPartition q


transSeq m (AP a pr@(DRV _ [p,_,q] _))
 | isSeq pr   =  AP a $ pTransSeq midvs m p q
 | otherwise  =  error "transSeq: not a pSeq"
 where -- here is why we really should merge alphabets
       -- into sub-components
   midvs = mkSet [ filter isIn $ pFV q
                 , aundash $ filter (not . isIn) $ pFV p ]
\end{code}

Predicate part:
\begin{code}
leftSeq = "("
rightSeq = ")"
kSeq = K ";"
pSeq p q  =  DRV leftSeq [p,kSeq,q] rightSeq

isSeq (DRV left [_,k,_] right)
         =  k == kSeq && [left,right] == [leftSeq,rightSeq]
isSeq _  =  False

pTransSeq vs d p q
 = EX vd $ pAnd [ psub (zip (adash vs) (map Var vd)) p
                , psub (zip vs         (map Var vd)) q ]
 where vd = adecor d vs
\end{code}

\newpage
\subsubsection{Assignment}

\begin{eqnarray*}
   \textbf{provided} && A = \{x,y,\ldots,z,x',y',\ldots,z'\} \land \alf e \subseteq A
\\ x :=_A e &\defs& x'=e \land y'=y \land \ldots \land z' = z
\\ \alf(x:=_A e) &\defs& A
\end{eqnarray*}
\begin{code}
apAsg a x e
 | ([x,vdash x]++eFV e) `subsetof` a && isHomogeneous a
              = AP (A $ mkset a) $ pAsg x e
 | otherwise  =  error "apAsg: alphabet mismatch"

aplAsg a x e
 = apAsg alf x e
 where
   inxev = x : eFV e
   alf = homogenise $ mkSet [a, inxev, adash inxev]

transAsg (AP a@(A vs) pr@(DRV _ [Atm (Var x),_,Atm e] _))
 | isAsg pr  =  AP a $ pTransAsg (filter isIn vs) x e
 | otherwise  =  error "transAsg: not a pAsg"
\end{code}

Predicates:
\begin{code}
leftAsg = ""
rightAsg = ""
kAsg = K ":="

pAsg x e
 = DRV leftAsg [Atm $ Var $ x, kAsg, Atm e] rightAsg

isAsg (DRV left [_,k,_] right)
         =  k == kAsg && [left,right] == [leftAsg,rightAsg]
isAsg _  =  False

pTransAsg ina x e
 = pAnd (eeq (Var $ vdash x) e : map vv'eq (ina \\ [x]) )

eeq e1 e2 = Atm $ App "==" [e1,e2]

vv'eq v = eeq (Var $ vdash v) (Var v)
\end{code}

\newpage
\subsubsection{Skip}

\begin{eqnarray*}
   \textbf{provided} && A = \{x,y,\ldots,z,x',y',\ldots,z'\}
\\ \Skip_A  &\defs& x'=x \land y'=y \land \ldots \land z' = z
\\ \alf(\Skip) &\defs& A
\end{eqnarray*}
\begin{code}
apSkip a
 | isHomogeneous a
              = AP (A $ mkset a) $ pSkip
 | otherwise  =  error "apSkip: alphabet mismatch"

aplSkip a = apSkip $ homogenise a

transSkip (AP a@(A vs) pr)
 | isSkip pr  =  AP a $ pTransSkip (filter isIn vs)
 | otherwise  =  error "transSkip: not a pSkip"
\end{code}

Predicates:
\begin{code}
kSkip = K "II"

pSkip = kSkip

isSkip pr@(K _)  =  pr == kSkip
isSkip _         =  False

pTransSkip ina = pAnd $ map vv'eq ina
\end{code}

\newpage
\subsubsection{Demonic Non-Determinism}

\begin{eqnarray*}
   \textbf{provided} && \alf P = \alf Q \ldots
\\ P \sqcap Q &\defs& P \lor Q
\\  \alf{(P \sqcap Q)} &\defs& \alf P
\end{eqnarray*}
\begin{code}
apDND p q
 | ap == aq
      = AP (A ap) $ pDND (apPred p) (apPred q)
 | otherwise  =  error "apDND: alphabet mismatch"
 where
   ap = alfv p
   aq = alfv q

aplDND p q
 = apDND (alfExtend aq p) (alfExtend ap q)
 where
   ap = alfv p
   aq = alfv q

transDND (AP a pr@(DRV _ [p,_,q] _))
 | isDND pr  =  AP a $ Or [p,q]
transDND _   =  error "transDND: not a pDND"
\end{code}

Now, the predicate bit:
\begin{code}
leftDND = "("
rightDND = ")"
kDND = K "|~|"

pDND p q =  DRV leftDND [p,kDND,q] rightDND

isDND (DRV left [_,k,_] right)
         =  k  == kDND && [left,right] == [leftDND,rightDND]
isDND _  =  False
\end{code}


\newpage
\subsubsection{Abort}

\begin{eqnarray*}
   \bot_A &\defs& \true
\\ \alf(\bot_A) &\defs& A
\end{eqnarray*}
\begin{code}
apAbort a = AP (A a) $ pAbort

transAbort (AP a pr@(K _))
 | isAbort pr  =  AP a TRUE
transAbort _     =  error "transAbort: not a pAbort"
\end{code}

Now, the predicate bit:
\begin{code}
kAbort = K "_|_"

pAbort = kAbort

isAbort pr@(K _)  =  pr == kAbort
isAbort _         =  False
\end{code}

\newpage
\subsubsection{Miracle}

\begin{eqnarray*}
   \top_A &\defs& \false
\\ \alf(\top_A) &\defs& A
\end{eqnarray*}
\begin{code}
apMiracle a = AP (A a) $ pMiracle

transMiracle (AP a pr@(K _))
 | isMiracle pr  =  AP a pFalse
transMiracle _     =  error "transMiracle: not a pMiracle"
\end{code}

Now, the predicate bit:
\begin{code}
kMiracle = K "T"

pMiracle = kMiracle

isMiracle pr@(K _)  =  pr == kMiracle
isMiracle _         =  False
\end{code}

\newpage
\subsubsection{Iteration}

\begin{eqnarray*}
   \textbf{provided} && \alf b \subseteq \alf P = \alf \Skip \ldots
\\ b * P &\defs& \mu X @ ( P;X \cond b \Skip)
\\  \alf{(b * P)} &\defs& \alf P
\end{eqnarray*}
\begin{code}
apIter b p
 | ab `subsetof` ap
      = AP (A ap) $ pIter (apPred b) (apPred p)
 | otherwise  =  error "apIter: alphabet mismatch"
 where
   ab = alfv b
   ap = alfv p

aplIter b p
 = apIter (alfExtend ap b) (alfExtend ab p)
 where
   ab = alfv b
   ap = alfv p

transIter x (AP a pr@(DRV _ [b,_,p] _))
 | isIter pr   =  AP a $ Rec x $ pCond (pSeq p (PH x)) b pSkip
transIter _ _  =  error "transIter: not a pIter"
\end{code}

Now, the predicate bit:
\begin{code}
leftIter = "("
rightIter = ")"
kIter = K "*"

pIter b p =  DRV leftIter [b,kIter,p] rightIter

isIter (DRV left [_,k,_] right)
          =  k == kIter && [left,right] == [leftIter,rightIter]
isIter _  =  False
\end{code}

\newpage
\subsubsection{Assumption}

\begin{eqnarray*}
   \textbf{provided} && \alf c \subseteq A = \alf \Skip = \alf \top
\\ c^\top &\defs& \Skip \cond c \top
\\ \alf(c^\top) &\defs& A
\end{eqnarray*}
\begin{code}
apAssume a c
 | a `subsetof` ac && isHomogeneous a
      = AP (A a) $ pAssume $ apPred c
 | otherwise  =  error "apAssume: alphabet mismatch"
 where
   ac = alfv c

aplAssume a c
 = apAssume ha (alfExtend ha c)
 where
   ac = alfv c
   ha = homogenise a

transAssume (AP a pr@(DRV _ [c,_] _))
 | isAssume pr   =  AP a $ pCond pSkip c pMiracle
transAssume _  =  error "transAssume: not a pAssume"
\end{code}

Now, the predicate bit:
\begin{code}
leftAssume = ""
rightAssume = ""
kAssume = K "^T"

pAssume c =  DRV leftAssume [c,kAssume] rightAssume

isAssume (DRV left [_,k] right)
            =  k == kAssume && [left,right] == [leftAssume,rightAssume]
isAssume _  =  False
\end{code}

\newpage
\subsubsection{Assertion}

\begin{eqnarray*}
   \textbf{provided} && \alf c \subseteq A = \alf \Skip = \alf \bot
\\ c_\bot &\defs& \Skip \cond c \bot
\\ \alf(c_\bot) &\defs& A
\end{eqnarray*}
\begin{code}
apAssert a c
 | a `subsetof` ac && isHomogeneous a
      = AP (A a) $ pAssert $ apPred c
 | otherwise  =  error "apAssert: alphabet mismatch"
 where
   ac = alfv c

aplAssert a c
 = apAssert ha (alfExtend ha c)
 where
   ac = alfv c
   ha = homogenise a

transAssert (AP a pr@(DRV _ [c,_] _))
 | isAssert pr   =  AP a $ pCond pSkip c pMiracle
transAssert _  =  error "transAssert: not a pAssert"
\end{code}

Now, the predicate bit:
\begin{code}
leftAssert = ""
rightAssert = ""
kAssert = K "_B"

pAssert c =  DRV leftAssert [c,kAssert] rightAssert

isAssert (DRV left [_,k] right)
            =  k == kAssert && [left,right] == [leftAssert,rightAssert]
isAssert _  =  False
\end{code}

\newpage
\subsubsection{Designs}

\begin{eqnarray*}
   \textbf{provided} && ok, ok' \notin \alf P = \alf Q \ldots
\\ P \vdash Q &\defs& ok \land P \implies ok' \land  Q
\\  \alf{(P \vdash Q)} &\defs& \alf P \cup \{ok,ok'\}
\end{eqnarray*}
\begin{code}
apDesign p q
 | ap == aq && ap == ap \\ aok
      = AP (A ap) $ pDesign (apPred p) (apPred q)
 | otherwise  =  error "apDesign: alphabet mismatch"
 where
   ap = alfv p
   aq = alfv q

vok  = V "ok"
vok' = V "ok'"
aok  = [vok,vok']

aplDesign p q
 | aok == aok \\ (ap++aq)
    = apDesign (alfExtend aq p) (alfExtend ap q)
 | otherwise  =  error "aplDesign: pre/post mention ok,ok'"
 where
   ap = alfv p
   aq = alfv q

transDesign (AP a pr@(DRV _ [p,_,q] _))
 | isDesign pr  =  AP a $ pImplies (pAnd [pok,p]) (pAnd [pok',q])
transDesign _   =  error "transDesign: not a pDesign"

pok  = Atm $ Var vok
pok' = Atm $ Var vok'
\end{code}

Now, the predicate bit:
\begin{code}
leftDesign = "("
rightDesign = ")"
kDesign = K "|-"

pDesign p q =  DRV leftDesign [p,kDesign,q] rightDesign

isDesign (DRV left [_,k,_] right)
         =  k  == kDesign && [left,right] == [leftDesign,rightDesign]
isDesign _  =  False
\end{code}


\newpage
\subsubsection{Design Miracle}

\begin{eqnarray*}
   \textbf{provided} && D = A \cup \{ok,ok'\} \ldots
\\ \top_{\mathbf{D}} &\defs& (\true \vdash \false)
\\ \alf(\top_{\mathbf{D}}) &\defs& D
\end{eqnarray*}
\begin{code}
apMiracleD d
 | aok `subsetof` d && isHomogeneous d
              =  AP (A d) $ pMiracleD
 | otherwise  =  error "apMiracleD: alphabet mismatch"

aplMiracleD d = apMiracleD $ mkSet [homogenise d,aok]

transMiracleD (AP a pr@(K _))
 | isMiracleD pr  =  AP a $ pDesign TRUE pFalse
transMiracleD _   =  error "transMiracleD: not a pMiracleD"
\end{code}

Now, the predicate bit:
\begin{code}
leftMiracleD = ""
rightMiracleD = ""
kMiracleD = K "T_D"

pMiracleD  =  kMiracleD

isMiracleD pr@(K _)  =  pr == kMiracleD
isMiracleD _         =  False
\end{code}

\newpage
\subsubsection{Design Abort}

\begin{eqnarray*}
   \textbf{provided} && D = A \cup \{ok,ok'\} \ldots
\\ \bot_{\mathbf{D}} &\defs& (\false \vdash \true)
\\ \alf(\bot_{\mathbf{D}}) &\defs& D
\end{eqnarray*}
\begin{code}
apAbortD d
 | aok `subsetof` d && isHomogeneous d
              =  AP (A d) $ pAbortD
 | otherwise  =  error "apAbortD: alphabet mismatch"

aplAbortD d = apAbortD $ mkSet [homogenise d,aok]

transAbortD (AP a pr@(K _))
 | isAbortD pr  =  AP a $ pDesign pFalse TRUE
transAbortD _   =  error "transAbortD: not a pAbortD"
\end{code}

Now, the predicate bit:
\begin{code}
leftAbortD = ""
rightAbortD = ""
kAbortD = K "B_D"

pAbortD  =  kAbortD

isAbortD pr@(K _)  =  pr == kAbortD
isAbortD _         =  False
\end{code}

\newpage
\subsection{Derived Predicate Translator}

We gather all the \texttt{transXXX} definitions above into one place
here:
\begin{code}
defTrans :: APred -> APred
defTrans ap@(AP alf pr)

 | isAnd      pr =  AP alf $ transAnd     pr
 | isFalse    pr =  AP alf $ transFalse   pr
 | isImplies  pr =  AP alf $ transImplies pr

 | isCond     pr =  transCond     ap
 | isSeq      pr =  transSeq "m"  ap
 | isAsg      pr =  transAsg      ap
 | isSkip     pr =  transSkip     ap
 | isDND      pr =  transDND      ap
 | isAbort    pr =  transAbort    ap
 | isMiracle  pr =  transMiracle  ap
 | isIter     pr =  transIter "X" ap
 | isAssume   pr =  transAssume   ap
 | isAssert   pr =  transAssert   ap
 | isDesign   pr =  transDesign   ap
 | isMiracleD pr =  transMiracleD ap
 | isAbortD   pr =  transAbortD   ap

 | otherwise     =  ap  -- no change
\end{code}
