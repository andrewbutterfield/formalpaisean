\section{Weak PML Resources}
\begin{code}
module WeakResources where
import qualified Data.Map.Strict as M
import Data.List
import SynResources
import SynAction
import SynWeak
import ResHandling
\end{code}

\begin{code}
instance GetResources WkPML where
  listResources = listWeakResources
  listActions (WPML _ wpml) = M.toList wpml
\end{code}

\subsection{Listing Weak Resources}

\begin{code}
listWeakResources :: WkPML -> [ResExpr]
listWeakResources (WPML _ wpml) =  nub . concat $ map listWkTBodyResources $ M.elems wpml

listWkTBodyResources :: PMLAction -> [ResExpr]
listWkTBodyResources (_, specs) = specResources specs
\end{code}
