\section{Weak PML Abstract Syntax}
\begin{code}
module SynWeak where
import Data.List
import qualified Data.Map as M

import Utilities

import SynResources
import SynAction

import AbsPML
import ErrM
\end{code}

\subsection{Weak Abstract Syntax}

\subsubsection{Task Bodies}

\begin{eqnarray*}
   \TbdyDecl &::=& \TbdyDefn
\end{eqnarray*}
We shall model this using the full PML Action.
\begin{code}
type WkAction = (String,PMLAction)

showWkAct (nm,act) = showAction nm act
\end{code}

\subsubsection{Weak PML Process}
\begin{eqnarray*}
   \WPMLDecl &::=& \WPMLDefn
\end{eqnarray*}
We implement this directly as a map:
\begin{code}
data WkPML = WPML String (M.Map String PMLAction)

instance Show WkPML where
  show (WPML nm wpml)
    = unlines (("Weak "++nm) : map showWkAct (M.assocs wpml))
\end{code}

\subsection{From Parser Syntax to Weak PML}

\begin{code}
transPROCESS2WPML :: PROCESS -> WkPML

transPROCESS2WPML (Process (ID str) prims)
 = WPML str $ M.fromList 
            $ concat $ map transPRIM2WPML prims
\end{code}

\begin{code}
transPRIM2WPML :: PRIM -> [WkAction]

transPRIM2WPML (PrimAct (ID id) optyp specs)
 = [(id , transACT optyp specs)]

transPRIM2WPML (PrimBr optnm prims)
  = concat $ map transPRIM2WPML prims

transPRIM2WPML (PrimSeln optnm prims)
  = concat $ map transPRIM2WPML prims

transPRIM2WPML (PrimIter optnm prims)
 = concat $ map transPRIM2WPML prims

transPRIM2WPML (PrimSeq optnm prims)
 = concat $ map transPRIM2WPML prims

transPRIM2WPML (PrimTask optnm prims)
 = concat $ map transPRIM2WPML prims
\end{code}
