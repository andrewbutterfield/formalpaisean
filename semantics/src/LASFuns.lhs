\section{Labelled Action System Functions}
\begin{code}
module LASFuns where
import Utilities
import APTypes
import APFuns
import APEval
import LASTypes
import Data.List
\end{code}

\subsection{Augmented PAS View}

Here we give code to display a program action system
as the corresponding action system UTP predicate:
\begin{code}
showPASUTP (PAS ves i f p)
 = unlines [showSimAsg ves,showCond f,showDisj 2 p]
 where

   showSimAsg [] = "VAR ls := " ++ show i
   showSimAsg ves
    = "VAR\n  "
      ++ lshow "," vs ++ ",ls := "
      ++ lshow "," es ++ "," ++ show i
    where (vs,es) = unzip ves

   showCond f = "~(subsetof("++show f++",ls) *"
\end{code}

Here we give computations relating program actions to labelled transition systems.

\subsubsection{Labels, and Sets of\ldots}

We shall interpret labels and label sets in our expression language
in a fairly obvious way:
\begin{code}
lbl2expr :: Lbl -> Expr
lbl2expr (L ell) = Txt ell

lbls2expr :: LblSet -> Expr
lbls2expr (LS ls) = Set $ map lbl2expr ls
\end{code}

\subsection{Label Updates}

The notation $L \oslash (M,N)$ denotes $L$ with
$M$ removed first, and then $N$ added.
\begin{eqnarray*}
   L \oslash (M,N) &\defs& (L \setminus M) \cup N
\end{eqnarray*}
\begin{code}
labUpdate :: LblSet -> (LblSet,LblSet) -> LblSet
labUpdate (LS ls) (LS ms,LS ns)
  =  LS $ nub $ sort ((ls \\ ms) ++ ns)
\end{code}
We lift this to the expression level:
\begin{code}
setUpdate :: Expr -> (Expr,Expr) -> Expr
setUpdate (Set ls) (Set ms,Set ns)
 = Set $ nub $ sort ((ls \\ ms) ++ ns)
setUpdate ell (em,en)
 = error ("setUpdate requires sets ! "++show [ell,em,en])
\end{code}

\newpage
\subsubsection{Program Actions}

We now show how to convert program actions
into guarded commands.
\begin{code}
pa2gcmd :: PAction -> GCmd

vls = V "ls"
\end{code}
\paragraph{Program Action}

\begin{eqnarray*}
   ( L : P | N ) &\defs& L \subseteq ls \then P ; ls := ls \oslash (L,N)
\\ ( l : P | n ) &=& (\{l\} : P | \{n\})
\end{eqnarray*}
\begin{code}
pa2gcmd (PA ls pr ns)
 = GC (Atm (subset (lbls2expr ls) (Var $ V "ls")))
      (pSeq
        pr
        (pAsg vls
              (oslash (Var vls)
                      (lbls2expr ls) (lbls2expr ns))))
\end{code}

\paragraph{Conditional Continuation}

\begin{eqnarray*}
   ( L :| M \cond b N )
   &\defs&
   L \subseteq ls \then ls := ls \oslash (L,M \cond b N)
\\ ( l :| m \cond b n ) &=& (\{l\} :| \{m\} \cond b \{n\})
\end{eqnarray*}
\begin{code}
pa2gcmd (CC ls ms c ns)
 = GC (Atm (subset (lbls2expr ls) (Var $ vls)))
      (pAsg vls
              (oslash (Var vls)
                      (lbls2expr ls)
                      (cond (lbls2expr ms) c (lbls2expr ns))))
\end{code}


\newpage
\subsubsection{Program Action System}

We now show how to convert a program action system
into a labelled action system:
\begin{code}
pas2asys :: PActionSys -> LASys
pas2asys paSys
 = LAS init cont gcmds
 where
   init = (vls, lbls2expr $ initLabs paSys) : pasInit paSys
   cont = Not $ Atm $ subset (lbls2expr $ finLabs paSys) ls
   gcmds = map pa2gcmd $ pasActions paSys
   ls = Var $ vls
\end{code}
