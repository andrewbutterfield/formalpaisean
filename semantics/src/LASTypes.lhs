\section{Labelled Action System Types}
\begin{code}
module LASTypes where
import Utilities
import APTypes
\end{code}


We shall define types for the abstract syntax of labelled action
systems.
In this module we define datatypes and show instances only.

\subsection{Action Systems}

\subsubsection{Guarded Command}

For condition $g$ and design $P$,
\begin{eqnarray*}
   g \then P &\defs& g^\top ; P
\end{eqnarray*}
\begin{code}
data GCmd
 = GC { guard :: Pred
      , gcPred :: Pred
      } deriving (Eq, Ord)

instance Show GCmd where
  show (GC g p) = show g ++ " -> " ++ show p
\end{code}

\newpage
\subsubsection{Action System}

For state variable vector $v$,
with a corresponding initialisation vector $e$,
and a collection of actions $\{ g_i \rightarrow C_i\}_{i \in 1\ldots n}$,
then the following is an action system:
\[
  \mathbf{var}~ v:=e ; (true) * \bigvee_{i \in 1\ldots n} (g_i \then C_i)
\]
\begin{code}
data LASys
 = LAS { asInit :: [(Var,Expr)]
       , asCont :: Pred
       , asGCmds :: [GCmd]
       } deriving (Eq,Ord)

instance Show LASys where
  show (LAS ves pr gcmd)
   = showSimAsg ves ++ "\n("++show pr++") *\n" ++ showDisj 2 gcmd
   where

     showSimAsg [] = "VAR"
     showSimAsg ves
      = "VAR\n  "
        ++ lshow "," vs ++ " := " ++ lshow "," es
      where (vs,es) = unzip ves

     showAsg v e  =  show v ++ " := " ++ show e
\end{code}

\newpage
\subsection{Labelled Action System}

\subsubsection{Labels}

\begin{code}
data Lbl = L { outL :: String} deriving (Eq, Ord)

instance Show Lbl where show (L s) = s
\end{code}

\subsubsection{Label Sets}

\begin{code}
data LblSet = LS { outLS :: [Lbl]} deriving (Eq, Ord)

instance Show LblSet where show (LS ls) = sshow ls
\end{code}

\subsubsection{Program Actions}

\paragraph{Program Action}

If $P$ is a design and $l$ and $n$ are labels,
then a program action is
\begin{eqnarray*}
   ( l : P | n )
\end{eqnarray*}
This generalises to sets of labels in the obvious way
\begin{eqnarray*}
   ( L : P | N )
\end{eqnarray*}

\paragraph{Conditional Continuation}

Give labels $l$, $m$ and $n$, and condition $b$,
the a conditional continuation is
\begin{eqnarray*}
   ( l :| m \cond b n )
\end{eqnarray*}
This also generalises:
\begin{eqnarray*}
   ( L :| M \cond b N )
\end{eqnarray*}

\paragraph{The datatype}~

\begin{code}
data PAction
  = PA LblSet Pred LblSet
  | CC LblSet LblSet Expr LblSet
  deriving (Eq,Ord)

instance Show PAction where
  show (PA ls p ns)
    = "( " ++ show ls ++ " : " ++ show p ++ " | " ++ show ns ++ ")"
  show (CC ls ms b ns)
    = "( " ++ show ls
      ++ " :| "
      ++ show ms ++ " <| " ++ show b ++ " |> " ++ show ns
      ++ ")"
\end{code}

\newpage
\subsubsection{Program Action System}

For state variable vector $v$,
with a corresponding initialisation vector $e$,
and $I$ and $F$ as sets of initial and final labels respectively,
with $P$ a disjunction of program actions and conditional continuations,
then the following is a program action system:
\begin{eqnarray*}
  (v,e,I,F,P)
  &\equiv&
  \mathbf{var}~  v,ls := e,I ; ( F \not\subseteq ls ) * P
\end{eqnarray*}
\begin{code}
data PActionSys
  = PAS { pasInit    :: [(Var,Expr)]
        , initLabs   :: LblSet
        , finLabs    :: LblSet
        , pasActions :: [PAction]
        }  deriving (Eq,Ord)

instance Show PActionSys where
  show (PAS ves i f p)
   = unlines (
      [ showSimAsg ves
      , "init: " ++ show i
      , "fin : " ++ show f
      ] ++ map show p )
   where

     showSimAsg [] = "no state"
     showSimAsg ves
      =    lshow "," vs
        ++ " := "
        ++ lshow "," es
      where (vs,es) = unzip ves
\end{code}
